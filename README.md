Requirements
============

Drupal 8.x (https://www.drupal.org/docs/8/install)
Commerce 2.x (https://docs.drupalcommerce.org/commerce2)

Installation
============

Install and enable the module.

If you haven't done so yet, log into Opayo to get your integration key and password.

Put in the 'generic' settings for Opayo using url '/admin/commerce/config/opayo_pi/settings'
Set the 'Opayo Vendor Name' and your live/test integaration keys and passwords.
Set the values for the 'expired' depending on how you want to keep expired database records.

Go to Commerce Configuration pages and add a new Payment Gateway using plugin 'Opayo (Pi integration)'.
Make sure 'Collect billing information' is checked and fill in the required fields.

Add a checkout flow that uses the 'Opayo checkout flow' Plugin (which supports the extra '3D Secure' step to the checkout process - other plugins won't work).
Check that the checkout flow has the '3DS (Opayo) pane in the '3D Secure' phase.
If you are shipping physical goods you will probably want to enable the 'Shipping Information' pane.

Change the 'Checkout flow' for any Commerce 'Order Types' that will use the Opayo checkout.

Opayo requires a customer telephone number in international format (starting with +).
* By default the gateway adds a text 'Telephone number' field to the payment information.
* This can be changed to a 'Phone International' style field (which helps with the international prefix and formatting) by selecting
  'Field of type 'Phone International' added to the payment information pane.' in the Opayo Pi Gateway configuration page.

Alternatively you can select 'Expect a Telephone number field added to the 'Customer' profile type....' and add a phone number field to the 'Customer' profile type:

Modify the 'Customer' Profile Type (/admin/config/people/profile-types/manage/customer/fields) and add a field for a phone number
Tested field types are 'International phone' and 'Plain text', make a note of the machine name of the new field and copy that into the
'Customer Profile Type telephone number field machine name' field of the Opayo Pi Gateway configuration screen.

If you *do* use the 'International Phone' module, follow the instructions on
https://www.drupal.org/project/phone_international for including the 3rd party 'intl-tel-input' library.

Since the code is still in 'release candidate' state it's recommended to maximise logging:
Set the 'Gateway log level' to 'Extra Verbose Debugging Info' on the payment gateway's configuration page.
Install and enable the 'File Log' module and set it to log to the following specific channels: commerce_opayo_pi, commerce_payment
Turn on 'Preserve Log' for the browser console on the client side (if testing yourself).
You should see logging into in the browser console and on the server in 'web/sites/default/files/logs/drupal.log'.


If you upgrading from a previous beta version then you may need to run update.php: there are additional entity fields that need to be installed.


Some comments about the module's behaviour:

There is substantial use of Javascript.

You can choose between using an Opayo provided 'drop-in Checkout' or a custom form provided by this module (https://developer.elavon.com/products/opayo/v1).
They operate in similar ways: using Javascript to submit the card details from the client's browser to Opayo to generate a token - this way the back-end never sees the card number.
The custom form provides more contextual info back to the back-end and is more robust against the various Opayo tokens expiring.

You can also choose between embedding the 3D secure form inside an iFrame in your Drupal or displaying it as a standalone page.

Opayo Pi uses 2 keys that are generated dynamically ('merchant session key' and 'card identifier') in the process of
paying: once generated they can only be used once. The implication is that, if there is a failure during a payment attempt, the customer
gets returned to the first checkout page (with a warning message) to try again.

If 3D Secure authentication fails then the customer gets returned to the checkout pages with a warning message set.

Javascript is used to provide a 'whatchdog timer' in case the customer leaves the browser on the 3D secure page.

A list of Opayo transaction info is available from admin/commerce/orders/opayotransactions
Opayo Transactions are Drupal entities you you are free to create your own views to make custom lists.
