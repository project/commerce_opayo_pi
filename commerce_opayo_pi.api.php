<?php

/**
 * @file
 * Hooks provided by the Commerce Opayo Pi module.
 */

use Drupal\commerce_opayo_pi\Entity\OpayoTransaction;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Allows pre-population of credit card fields
 *
 * @param array $form
 *   The form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the complete form.
 *
 * @see \Drupal\commerce_opayo_pi\PluginForm\OpayoPi\PaymentMethodAddForm::buildConfigurationForm()
 *
 * @ingroup commerce_opayo_pi_api
 */
function hook_commerce_opayo_pi_payment_method_alter(array $form, FormStateInterface $form_state) {
}

/**
 * Allows pre-population of billing information
 *
 * @param array $form
 *   The form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the complete form.
 *
 * @see \Drupal\commerce_opayo_pi\PluginForm\OpayoPi\PaymentMethodAddForm::buildConfigurationForm()
 *
 * @ingroup commerce_opayo_pi_api
 */
function hook_commerce_opayo_pi_billing_info_alter(array $form, FormStateInterface $form_state) {
}

/**
 * Allows override customer email address
 *
 * @param array $custContact
 *  Array containing customer's email (key 'customerEmail') and phone (key 'customerPhone') details
 * @param OrderInterface $order
 *   Order details
 * @param stdClass extraInfo
 *   The extra info to be stored in the session for the 'createPayment' method to pick up
 *
 * @ingroup commerce_opayo_pi_api
 */
function hook_commerce_opayo_pi_email_and_phone_alter(array $custContact, OrderInterface $order, stdClass $extraInfo) {
}

/**
 * Allows extra info to be passed along with the payment method
 *
 * @param array $form
 *   The form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the complete form.
 * @param stdClass extraInfo
 *   The extra info to be stored in the session for the 'createPayment' method to pick up
 *
 * @ingroup commerce_opayo_pi_api
 */
function hook_commerce_opayo_pi_payment_method_extra_info_alter(array $form, FormStateInterface $form_state, stdClass $extraInfo) {
}

/**
 * Allows custom actions when a 3D secure process is successful and will lead to an order completion
 *
 * @param OpayoTransaction $opayoTransaction
 *  Opayo transaction
 * @param OrderInterface $order
 *   Order details
 *
 * @ingroup commerce_opayo_pi_api
 */
function hook_commerce_opayo_pi_3d_order_complete_alter(OpayoTransaction $opayoTransaction, OrderInterface $order) {
}
