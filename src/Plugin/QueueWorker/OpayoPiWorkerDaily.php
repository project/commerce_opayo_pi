<?php

namespace Drupal\commerce_opayo_pi\Plugin\QueueWorker;

use DateTime;
use Drupal\commerce_opayo_pi\OpayoPi;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Exception;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Length;

/**
 * Queue job for getting bank settlement and fraud scoring info.
 *
 * Need to add the following to 'settings.php' so that the 'unique queue' gets picked up during cron processing
 * $settings['queue_service_commerce_opayo_pi_daily'] = 'queue_unique.database';
 *
 * @QueueWorker(
 *  id = "commerce_opayo_pi_daily",
 *  title = @Translation("Opayo Pi"),
 *  cron = {"time" = 30}
 * )
 */
class OpayoPiWorkerDaily extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

    /**
   * The module's service
   *
   * @var \Drupal\commerce_opayo_pi\OpayoPi
   */
  protected $opayoPiService;

  /**
   * Constructs a new OpayoPiWorkerDaily object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, LoggerChannelInterface $logger, OpayoPi $opayoPi) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->opayoPiService = $opayoPi;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('commerce_opayo_pi.logger.channel.commerce_opayo_pi'),
      $container->get('commerce_opayo_pi.opayo_pi'),
    );
  }

  public function processItem($data) {
    if (property_exists($data, 'opayoBatchDateToProcess')) {
      // Fetch transaction batch info
      $this->processOpayoBatchDate($data->opayoBatchDateToProcess);
    }
  }

  /**
   * Retrieve bank settlement batch information for a given date and update the 'batch ID' field on OpayoTransaction entities
   *
   * @param string $opayoBatchDateToProcess
   *   The date for which to get batch IDs
   *
   */
  protected function processOpayoBatchDate(string $opayoBatchDateToProcess) {

    $opayo_config = $this->opayoPiService->getSettings();

    $url = $opayo_config == null ? null : $opayo_config['liveaccess'];    // Batch IDs are only created for 'live' payments, not test
    $account_user = $opayo_config == null ? null : $opayo_config['live_account_user'];
    $account_password = $opayo_config == null ? null : $opayo_config['live_account_password'];

    if (isset($account_user) && isset($account_password) && isset($url)) {
      try {

        // First, get the 'batch ID' associated with the date by using the 'getBatchList' command
        $batch_ids = array();
        $batch_completed = array();

        // Use Opayo's 'Reporting & Admin API', see https://developer.elavon.com/products/opayo-reporting-api/v1

        // Batch date (Opayo's transactions get processed once a day from the acquiring provider to your bank account) - this is often 1 day after the transaction but can be several, depending on public holidays
        $parse_result = date_parse($opayoBatchDateToProcess);
        $start_date = DateTime::createFromFormat('Y-n-j H:i:s', strval($parse_result['year']) . '-' . strval($parse_result['month']) . '-' . strval($parse_result['day']) . ' 00:00:00');
        $end_date = clone $start_date;
        // Allow for bank holidays
        $end_date->modify('+ 4 days');
        if ($end_date->format('w') == '0')
          // Sunday
          $end_date->modify('+ 1 day');
        else if ($end_date->format('w') == '6')
          // Saturday
          $end_date->modify('+ 2 days');
        $start_date_xml = '<startdate>' . $start_date->format('d/m/Y H:i:s') . '</startdate>';
        $end_date_xml = '<enddate>' . $end_date->format('d/m/Y H:i:s') . '</enddate>';
        $request_contents = $this->requestXML($opayo_config['vendor'], $account_user, $account_password, 'getBatchList', $start_date_xml . $end_date_xml);

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => http_build_query(["XML" => $request_contents]),
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded"
          ),
        ));

        $this->logger->debug('OpayoPiWorker::processOpayoBatchDate: about to request getBatchList for dates: {startDate} - {endDate}', ['startDate' => $start_date->format('d/m/Y'), 'endDate' => $end_date->format('d/m/Y')]);
        $raw_response = curl_exec($curl);
        $http_code = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
        $err = curl_error($curl);

        if (!empty($err)) {
          $this->logger->warning('OpayoPiWorker::processOpayoBatchDate: Unsuccessful getBatchList request date: {batchDate}, error: {error} ', ['batchDate' => $start_date->format('d/m/Y'), 'error' => $err]);
        } else {
          $this->logger->debug('OpayoPiWorker::processOpayoBatchDate: Successful getBatchList request date: {startDate} - {endDate}', ['startDate' => $start_date->format('d/m/Y'), 'endDate' => $end_date->format('d/m/Y')]);
        }

        $contents = json_decode(json_encode(simplexml_load_string($raw_response)), true);

        curl_close($curl);

        if (array_key_exists('errorcode', $contents)) {
          $error_code = $contents['errorcode'];

          if ($error_code == '0000') {
            // success
            $batches = $contents['batches'];

            $this->parseBatchInfo($batches, $batch_ids, $batch_completed);
          }
          else {
            $this->logger->warning('OpayoPiWorker::processOpayoBatchDate: getBatchList returns error for request date: {batchDate}, code: {error} ', ['batchDate' => $start_date->format('d/m/Y'), 'error' => $error_code]);
          }
        }
        else {
          $this->logger->warning('OpayoPiWorker::processOpayoBatchDate: getBatchList unexpected response for request date: {batchDate}, payload: {payload} ', ['batchDate' => $start_date->format('d/m/Y'), 'payload' => $raw_response]);
        }

        if (count($batch_ids) == 0) {
          $this->logger->warning('OpayoPiWorker::processOpayoBatchDate: getBatchList returns no batch request date: {batchDate}', ['batchDate' => $start_date->format('d/m/Y')]);
        }
        else if (count($batch_ids) > 0) {
          // Transactions matching the batch
          $opayo_transactions = array();

          foreach(array_keys($batch_ids) as $batch_id) {

            $auth_processor = $batch_ids[$batch_id];
            $batch_id_xml = '<batchid>' . $batch_id . '</batchid>';
            $auth_processor_xml = '<authprocessor>' . $auth_processor . '</authprocessor>';
            $request_contents = $this->requestXML($opayo_config['vendor'], $account_user, $account_password, 'getBatchDetail', $batch_id_xml . $auth_processor_xml);

            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => http_build_query(["XML" => $request_contents]),
              CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
              ),
            ));

            $this->logger->debug('OpayoPiWorker::processOpayoBatchDate: about to request getBatchDetail for ID: {batchId} ', ['batchId' => $batch_id]);
            $raw_response = curl_exec($curl);
            $http_code = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
            $err = curl_error($curl);

            if (!empty($err)) {
              $this->logger->warning('OpayoPiWorker::processOpayoBatchDate: Unsuccessful getBatchDetail ID: {batchId} , error: {error} ', ['batchId' => $batch_id, 'error' => $err]);
            } else {
              $this->logger->debug('OpayoPiWorker::processOpayoBatchDate: Successful getBatchDetail ID: {batchId} ', ['batchId' => $batch_id]);
            }

            $response_array = json_decode(json_encode(simplexml_load_string($raw_response)), true);

            curl_close($curl);

            $contents = null;
            if (array_key_exists('vspaccess', $response_array)) {
              $contents = $response_array['vspaccess'];
            }
            else if (array_key_exists('errorcode', $response_array)) {
              $contents = $response_array;
            }
            if ($contents != null) {
              $error_code = $contents['errorcode'];

              if ($error_code == '0000') {
                // success
                $transactions = $contents['transactions'];
                $total_rows = $contents["totalrows"];

                if ($total_rows > 1 && array_key_exists('transaction', $transactions)) {
                  $transactions = $transactions['transaction'];
                }

                foreach ($transactions as $transaction) {
                  $opayo_transaction_id = $transaction['vpstxid'];
                  $opayo_transaction_started = $transaction['started'];
                  $opayo_transaction_completed = null;
                  $opayo_transaction_amount = $transaction['amount'];

                  // Collect additional 3rd man and fraud scoring info
                  $additional_info = array();
                  if (array_key_exists('t3mscore', $transaction))
                    $additional_info['3rd man score'] = $transaction['t3mscore'];
                  if (array_key_exists('t3maction', $transaction))
                    $additional_info['3rd man score'] = $transaction['t3maction'];
                  if (array_key_exists('fraudscreenrecommendation', $transaction))
                    $additional_info['3rd man score'] = $transaction['fraudscreenrecommendation'];
                  if (array_key_exists('fraudcode', $transaction))
                    $additional_info['3rd man score'] = $transaction['fraudcode'];
                  if (array_key_exists('fraudcodedetail', $transaction))
                    $additional_info['3rd man score'] = $transaction['fraudcodedetail'];
                  $additional_info['started'] = $opayo_transaction_started;

                  // The 'completed' timestamp is not part of the retrieved info
                  // It can be fetched using the Transaction Details query
                  try {
                    $request_contents = $this->requestXML($opayo_config['vendor'], $account_user, $account_password, 'getTransactionDetail', '<vpstxid>' . $opayo_transaction_id . '</vpstxid>');
                    $curli = curl_init();
                    curl_setopt_array($curli, array(
                      CURLOPT_URL => $url,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_CUSTOMREQUEST => "POST",
                      CURLOPT_POSTFIELDS => http_build_query(["XML" => $request_contents]),
                      CURLOPT_HTTPHEADER => array(
                        "Cache-Control: no-cache",
                        "Content-Type: application/x-www-form-urlencoded"
                      ),
                    ));
                    $this->logger->debug('OpayoPiWorker::processOpayoBatchDate: about to call \'getTransactionDetail\' for Transaction ID: {transactionId}', ['transactionId' => $opayo_transaction_id]);
                    $raw_response = curl_exec($curli);
                    $http_code = curl_getinfo($curli, CURLINFO_RESPONSE_CODE);
                    $err = curl_error($curli);
                    if (!empty($err)) {
                      $this->logger->warning('OpayoPiWorker::processOpayoBatchDate: Unsuccessful getTransactionDetail Transaction ID: {transactionId}, error: {error} ', ['transactionId' => $opayo_transaction_id, 'error' => $err]);
                    } else {
                      $this->logger->debug('OpayoPiWorker::processOpayoBatchDate: Successful getTransactionDetail Transaction ID: {transactionId}', ['transactionId' => $opayo_transaction_id]);
                    }
                    $contents = json_decode(json_encode(simplexml_load_string($raw_response)), true);
                    curl_close($curli);
                    if (array_key_exists('errorcode', $contents)) {
                      $error_code = $contents['errorcode'];

                      if ($error_code == '0000') {
                        // success
                        if (array_key_exists('completed', $contents)) {
                          $additional_info['completed'] = $contents['completed'];
                        }
                      }
                      else {
                        $error = '';
                        if (array_key_exists('error', $contents))
                          $error = $contents['error'];
                        $this->logger->warning('OpayoPiWorker::processOpayoBatchDate: getTransactionDetail returns error forTransaction ID: {transactionId}, code: {errorCode}, error {error}', ['transactionId' => $opayo_transaction_id, 'errorCode' => $error_code, 'error' => $error]);
                      }
                    }
                    else {
                      $this->logger->warning('OpayoPiWorker::processOpayoBatchDate: getTransactionDetail unexpected response for Transaction ID: {transactionId}, payload: {payload} ', ['transactionId' => $opayo_transaction_id, 'payload' => $raw_response]);
                    }
                  }
                  catch (Exception $e1) {
                    $this->logger->error('OpayoPiWorker::processOpayoBatchDate: exception {exception}', ['exception' => ($e1->getMessage() . ' ' . $e1->getTraceAsString())]);
                  }

                  // Save additional info with the transaction ID
                  $opayo_transactions[$opayo_transaction_id] = $additional_info;

                  $this->logger->info('OpayoPiWorker::processOpayoBatchDate: Transaction {transactionId} dated {transTime} for amount {amount} part of batch: {batchId} ', ['batchId' => $batch_id, 'batchId' => $batch_id, 'transactionId' => $opayo_transaction_id, 'transTime' => $opayo_transaction_started, 'amount' => $opayo_transaction_amount]);
                }
              } else {
                $this->logger->warning('OpayoPiWorker::processOpayoBatchDate: getBatchDetail returns error for ID: {batchId}, code: {error} ', ['batchId' => $batch_id, 'error' => $error_code]);
              }
            } else {
              $this->logger->warning('OpayoPiWorker::processOpayoBatchDate: getBatchDetail unexpected response for ID: {batchId}, payload: {payload} ', ['batchId' => $batch_id, 'payload' => $raw_response]);
            }
          }

          foreach($opayo_transactions as $opayo_transaction_id => $additional_info) {
            // Match
            $entity = $this->entityTypeManager->getStorage('opayo_transaction')->load($opayo_transaction_id);
            if ($entity != null) {
              /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction */
              $opayo_transaction = $entity;

              $cur_batch_id = $opayo_transaction->get('batch_id')->value;
              if ($cur_batch_id == null || $cur_batch_id == '') {
                // Set the batch ID, batch completed timestamp and optionally info about fraud scoring
                $opayo_transaction->set('batch_id', $batch_id);
                $opayo_transaction->set('batch_completed', $batch_completed[$batch_id]->getTimestamp());
                if (count($opayo_transactions[$opayo_transaction_id]) > 0) {
                  $fraud_score_info = json_encode($additional_info);
                  $opayo_transaction->set('fraud_score_info', $fraud_score_info);
                }
                $opayo_transaction->save();
                $this->logger->info('OpayoPiWorker::processOpayoBatchDate: Transaction entity {transactionId} set batch ID TO: {batchId} ', ['batchId' => $batch_id, 'batchId' => $batch_id, 'transactionId' => $opayo_transaction_id]);
              }
              $cur_started = $opayo_transaction->get('started')->value;
              $cur_completed = $opayo_transaction->get('completed')->value;
              if ($cur_started == null || $cur_started == '' || $cur_completed == null || $cur_completed == '') {
                if (array_key_exists('started', $additional_info)) {
                  $transaction_started = DateTime::createFromFormat('d/m/Y H:i:s', substr($additional_info['started'], 0, 19));
                  $opayo_transaction->set('started', $transaction_started->getTimestamp());
                }
                if (array_key_exists('completed', $additional_info)) {
                  $transaction_completed = DateTime::createFromFormat('d/m/Y H:i:s', substr($additional_info['completed'], 0, 19));
                  $opayo_transaction->set('completed', $transaction_completed->getTimestamp());
                }
                $opayo_transaction->save();
              }
            }
          }
        }
      }
      catch (Exception $e) {
        $this->logger->error('OpayoPiWorker::processOpayoBatchDate: exception {exception}', ['exception' => ($e->getMessage() . ' ' . $e->getTraceAsString())]);
      }
    }
  }


  protected function parseBatchInfo(array $batchOrBatches, array &$batch_ids, array &$batch_completed) {
    if (array_key_exists('batchid', $batchOrBatches)) {
      // The array contains info about a batch
      $batch_id = $batchOrBatches['batchid'];
      $auth_processor = $batchOrBatches["authprocessorname"];
      $completed = $batchOrBatches['completed'];
      if (strlen($completed) > 19)
        $completed = substr($completed, 0, 19);
      // Add to the list of batch IDs for which we need to get details
      $batch_ids[$batch_id] = $auth_processor;
      $batch_completed[$batch_id] = DateTime::createFromFormat('d/m/Y H:i:s', $completed);
      $this->logger->info('OpayoPiWorker::processOpayoBatchDate: Batch ID {batchId} completed date: {completedDate} ', ['completedDate' => $completed, 'batchId' => $batch_id]);
      return;
    }
    else {
      foreach(array_keys($batchOrBatches) as $key) {
        if (is_array($batchOrBatches[$key]))
          $this->parseBatchInfo($batchOrBatches[$key], $batch_ids, $batch_completed);
      }
    }

  }


  /**
   * Creates an XML request message
   *
   * @param array $vendor
   *   Opayo 'vendor'
   * @param string $user
   *   Opayo account username
   * @param string $password
   *   Opayo account password
   * @param string $command
   *   Command to submit - see API
   * @param string $other
   *   Additional XML fields to support the command
   *
   * @return string
   *   request message in XML format
   */
  protected function requestXML($vendor, $user, $password, $command, $other = null) {
    $xml_string_contents = '<command>' . $command . '</command>' .
                   '<vendor>' . $vendor . '</vendor>' .
                   '<user>' . $user . '</user>' .
                   (isset($other) ? $other : '');

    // MD5 Hash the contents with the password included
    $xml_string_contents_hash = md5($xml_string_contents . '<password>'. $password . '</password>');

    // Return contents and the MD5 hash signature
    return '<vspaccess>' . $xml_string_contents . '<signature>' . $xml_string_contents_hash . '</signature></vspaccess>';
  }
}
