<?php

namespace Drupal\commerce_opayo_pi\Plugin\QueueWorker;

use DateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Exception;

/**
 * Various jobs deleting expired information
 *
 * @QueueWorker(
 *  id = "commerce_opayo_pi",
 *  title = @Translation("Opayo Pi"),
 *  cron = {"time" = 30}
 * )
 */
class OpayoPiWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new OpayoPiWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, LoggerChannelInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('commerce_opayo_pi.logger.channel.commerce_opayo_pi'),
    );
  }

  public function processItem($data) {
    if (property_exists($data, 'opayoTransactionToDelete'))
    {
      // Process expired Opayo transactions
      $this->deleteExpiredOpayoTransaction($data->opayoTransactionToDelete);
    }
    elseif (property_exists($data, 'commercePaymentMethodToDelete'))
    {
      // Process expired Opayo transactions
      $this->deleteExpiredCommercePaymentMethod($data->commercePaymentMethodToDelete);
    }

  }


  protected function deleteExpiredOpayoTransaction(string $transactionId) {
    $entity = $this->entityTypeManager->getStorage('opayo_transaction')->load($transactionId);
      if ($entity != null) {
      /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction */
      $opayo_transaction = $entity;
      $transaction_time = DateTime::createFromFormat('U', $opayo_transaction->getReceived());
      $threed_response_time = null;
      if ($opayo_transaction->get('3d_response_received')->value != null)
        $threed_response_time = DateTime::createFromFormat('U', $opayo_transaction->get('3d_response_received')->value);

      $this->logger->info('OpayoPiWorker::deleteExpiredOpayoTransaction: deleting expired Opayo transaction entity {transactionId}, status: {status}, status code: {statusCode}, status detail: {statusDetail}, request URL: {requestUrl}, order: {ordeId}, transaction time {transactionTime}, 3D response recieved {threeDResponseTime}',
        ['transactionId' => $opayo_transaction->getTransactionId(), 'status' => $opayo_transaction->getStatus(), 'statusCode' => $opayo_transaction->getStatusCode(), 'statusDetail' => $opayo_transaction->get('status_detail')->value,
        'requestUrl' => $opayo_transaction->get('request_url')->value, 'ordeId' => $opayo_transaction->get('order')->value, 'transactionTime' => $transaction_time->format('d-m-Y H:i:s'), 'threeDResponseTime' => ($threed_response_time == null ? '' : $threed_response_time->format('d-m-Y H:i:s'))]);
      $opayo_transaction->delete();
    }
  }


  protected function deleteExpiredCommercePaymentMethod(string $methodId) {
    $entity = $this->entityTypeManager->getStorage('commerce_payment_method')->load($methodId);
    if ($entity != null) {
      /** @var \Drupal\commerce_payment\Entity\PaymentMethod $payment_method */
      $payment_method = $entity;
      $expires_time = DateTime::createFromFormat('U', $payment_method->getExpiresTime());

      // Any orders that still link to the payment method?
      $order_ids = array();
      try {
        $query = $this->entityTypeManager->getStorage('commerce_order')->getQuery();
        $query->accessCheck(FALSE);
        $query->condition('payment_method', $payment_method->id());
        $order_ids = $query->execute();
      } catch (Exception $e) {
        $this->logger->error('OpayoPiWorker::deleteExpiredCommercePaymentMethods: exception {exception}', ['exception' => ($e->getMessage() . ' ' . $e->getTraceAsString())]);
        throw $e;
      }
      $commerce_orders = $this->entityTypeManager->getStorage('commerce_order')->loadMultiple($order_ids);
      foreach($commerce_orders as $co) {
        // Unset the payment method and gateway on any orders
        /** @var \Drupal\commerce_order\Entity\Order $commerce_order */
        $commerce_order = $co;
        $commerce_order->set('payment_gateway', null);
        $commerce_order->set('payment_method', null);
        $commerce_order->save();
        $this->logger->info('OpayoPiWorker::deleteExpiredCommercePaymentMethods: unset payment_method ({methodId} and payment_gateway on order {orderId}', ['methodId' => $payment_method->id(), 'orderId' => $commerce_order->id()]);
      }

      $this->logger->info('OpayoPiWorker::deleteExpiredCommercePaymentMethods: deleting expired Commerce Payment Method entity {methodId}, type: {type}, user Id: {uid}, mode: {mode}, remote ID: {remoteId}, expired {expiryTime}',
        ['methodId' => $payment_method->id(), 'type' => $payment_method->getType(), 'uid' => $payment_method->get('uid')->value, 'mode' => $payment_method->getPaymentGatewayMode(), 'remoteId' => $payment_method->getRemoteId(), 'expiryTime' => $expires_time->format('d-m-Y H:i:s')]);
      $payment_method->delete();
    }
  }

}
