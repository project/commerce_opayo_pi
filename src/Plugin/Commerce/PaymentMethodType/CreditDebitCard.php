<?php

namespace Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\CreditCard as CreditCardHelper;

/**
 * Custom credit card payment type for Opayo Pi
 *
 * @CommercePaymentMethodType(
 *   id = "credit_card_opayo",
 *   label = @Translation("new Credit/Debit card"),
 * )
 */
class CreditDebitCard extends \Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\CreditCard {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $remote_id = $payment_method->getRemoteId();
    if (!isset($remote_id))
      return $this->t('new Credit/Debit card');
    else {
      // Previously entered payment method, Opayo card identifier token is only valid for 400 seconds
      $expiry_suffix = $payment_method->isExpired() ? ' (verification expired)' : '';

      // 'Own Form' javascript provides 'card type' and 'last 4 digits', 'Dropin Form' method doesn't
      if ($payment_method->card_type != null && isset($payment_method->card_type->value) && $payment_method->card_number != null && isset($payment_method->card_number->value)) {
        $card_type = CreditCardHelper::getType($payment_method->card_type->value);
        $args = [
          '@card_type' => $card_type->getLabel(),
          '@card_number' => $payment_method->card_number->value,
        ];
        return $this->t('@card_type ending in @card_number' . $expiry_suffix, $args);
      }
      else {
        return $this->t('your Credit/Debit card' . $expiry_suffix);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();
    return $fields;
  }

}
