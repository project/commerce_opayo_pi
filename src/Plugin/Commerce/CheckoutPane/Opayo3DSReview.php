<?php

namespace Drupal\commerce_opayo_pi\Plugin\Commerce\CheckoutPane;

use DateTime;
use Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_opayo_pi\Entity\OpayoTransaction;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds 3DS authentication for Opayo Pi payment methods.
 *
 * This checkout pane is required for 3DS functionality. It ensures that the
 * last step in the checkout performs authentication. If the
 * customer's card is not enrolled in 3DS then the form will submit as normal.
 * Otherwise a modal will appear for the customer to authenticate.
 *
 * @CommerceCheckoutPane(
 *   id = "opayo_pi_3ds",
 *   label = @Translation("3DS (Opayo)"),
 *   default_step = "threedsecure",
 *   wrapper_element = "container",
 * )
 */
class Opayo3DSReview extends CheckoutPaneBase {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $payment_gateway_plugin
   */
  protected $payment_gateway_plugin;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $checkout_flow);
    $instance->setLogger($container->get('logger.channel.commerce_payment'));
    $instance->setModuleHandler($container->get('module_handler'));

    return $instance;


  }

  /**
   * Sets the logger.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The new logger.
   *
   * @return $this
   */
  public function setLogger(LoggerInterface $logger) {
    $this->logger = $logger;
    return $this;
  }

    public function setModuleHandler(ModuleHandlerInterface $moduleHandler) {
    $this->moduleHandler = $moduleHandler;
  }


  /**
   * {@inheritdoc}
   */
  public function isVisible() {

    if ($this->order->get('payment_method')->isEmpty() || $this->order->get('payment_gateway')->isEmpty() || !$this->order->get('payment_gateway')->entity) {
      return FALSE;
    }
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $this->order->get('payment_gateway')->entity;
    if (!$payment_gateway->getPlugin() instanceof OpayoPiPaymentGatewayInterface) {
      return FALSE;
    }
    $configuration = $payment_gateway->getPlugin()->getConfiguration();
    if (empty($configuration['3d_secure'])) {
      return FALSE;
    }
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->order->get('payment_method')->entity;

    if (!$payment_method || $payment_method->getType()->getPluginId() !== 'credit_card_opayo') {
      return FALSE;
    }

    if (!$this->order->getTotalPrice() || $this->order->getTotalPrice()->isZero()) {
      // No payment is needed if the order is free.
      $this->logVerboseDebug('Opayo3DSReview::isVisible - not visible, order is free');
      return FALSE;
    }

    return true;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $this->order->payment_gateway->entity;
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->order->get('payment_method')->entity;
    /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment_gateway->getPlugin();

    $this->logVerboseDebug('Opayo3DSReview::buildPaneForm, order id: ' . $this->order->id());

    // Find the matching Opayo transaction that has a 3DS challenge request
    /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction */
    $opayo_transaction = null;
    $opayo_transaction_id = $payment_method->getRemoteId();

    try {
      $query = $this->entityTypeManager->getStorage('opayo_transaction')->getQuery();
      $query->condition('transaction_id', $opayo_transaction_id);
      $query->accessCheck(FALSE);
      $query->sort('received', 'DESC');
      $ids = $query->execute();
      if (count($ids) >= 1) {
        /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction */
        $opayo_transaction = $this->entityTypeManager->getStorage('opayo_transaction')->load(array_values($ids)[0]);
      }
    }
    catch (Exception $exception) {
      $this->logVerboseDebug('Opayo3DSReview::buildPaneForm, PaymentGatewayException: ' . $exception->getMessage() . ', order id: ' . $this->order->id());
      throw new PaymentGatewayException($exception->getMessage());
    }

    if ($opayo_transaction != null) {

      $this->logVerboseDebug('Opayo3DSReview::buildPaneForm, Opayo transaction: ' . $opayo_transaction_id . ', order id: ' . $this->order->id());

      /** @var int $received_time */
      $received_time = $opayo_transaction->getReceived();
      $seconds_since_received = (new DateTime('now'))->getTimestamp() - $received_time;

      if ($opayo_transaction->getStatus() == 'Ok' && $opayo_transaction->getStatusCode() == '0000') {
        // Already authenticated, can skip 'threedsecure' checkout flow step and continue to 'complete'
        $next_step_id = $this->checkoutFlow->getNextStepId($this->getStepId());
        $this->logVerboseDebug('Opayo3DSReview::buildPaneForm, Opayo transaction success - checkout flow next step: ' . $next_step_id . ', Opayo transaction: ' . $opayo_transaction_id . ', order id: ' . $this->order->id());

        if ($payment_gateway_plugin instanceof SupportsStoredPaymentMethodsInterface && !$this->order->get('payment_method')->isEmpty()) {
          if ($next_step_id == 'complete') {
            // Allow changes or interventions between 3D authentication success and order completion
            $this->moduleHandler->alter('commerce_opayo_pi_3d_order_complete', $opayo_transaction, $this->order);
          }
          $this->checkoutFlow->redirectToStep($next_step_id);
        }
      }
      elseif (strlen($opayo_transaction->getChallengeRequest()) > 0 && $seconds_since_received < 1200) {

        $this->logVerboseDebug('Opayo3DSReview::buildPaneForm, Opayo transaction success - 3D step needed, Opayo transaction: ' . $opayo_transaction_id . ', order id: ' . $this->order->id());

        // Display 3DS secure challenge

        // The Opayo API documentation is unclear about what to use as the 'threeDSSessionData' (this identifies between potentially multiple pending 3D Secure challenges)
        // It has been observed that the obvious approach (using the returned 'transactionId') will cause an automatic and unexplained rejection of the 3D secure challenge.
        // Instead we use the 'vendorTxCode' from the original request
        $threeds_session_data = $opayo_transaction->getVendorTxCode();

        // Encode the threeDSSessionData  to BASE64
        $threeds_session_data_base64 = base64_encode($threeds_session_data);

        try {
          // Log the conversion of the transaction ID to BASE64 to help tracing 3DS flow
          $this->logger->info('Opayo3DSReview::buildPaneForm: convert threeDSSessionData {threeDsSessionData} to BASE64: {threeDsSessionDatadBase64} for 3DS challenge (order {orderId}, uid: {orderUID})', ['threeDsSessionData' => $threeds_session_data, 'threeDsSessionDatadBase64' => $threeds_session_data_base64, 'orderId' => $this->order->id(), 'orderUID' => $this->order->uuid()]);
        } catch (Exception $le) {
        }

        $configuration = $payment_gateway_plugin->getConfiguration();
        if ($configuration['3d_iframe'] == true) {

          // Link CSS from within iframe
          $module_path = \Drupal::service('extension.list.module')->getPath('commerce_opayo_pi');
          $iframe_css = '/' . $module_path . "/css/commerce_opayo_pi-3dsecure.css";

          $url = $opayo_transaction->getAcsUrl();
          $challenge_request_base64 = $opayo_transaction->getChallengeRequest();

          // HTML contents of the form requesting the user to continue to 3DS authentication
          $opayo_3ds_submit_html = "<link type=\"text/css\" rel=\"Stylesheet\" href=\"{$iframe_css}\" />" .
            "<form action=\"{$url}\" method=\"post\">" .
            "<input type=\"hidden\" name=\"creq\" value=\"{$challenge_request_base64}\" />" .
            "<input type=\"hidden\" id=\"threeDSSessionData\" name=\"threeDSSessionData\" value=\"{$threeds_session_data_base64}\" />" .
            "<p>Please click the button below to proceed to 3D secure.</p>" .
            "<input type=\"submit\" value=\"Proceed to 3D Secure\" class=\"button\"/>" .
            "</form>";

          // Display an iFrame with a message and a button that takes the customer to 3D secure when pressed

          $pane_form['3Diframe'] = [
            '#attached' => [
              'library' => ['commerce_opayo_pi/opayo_pi_3dsecure'],
            ],
            '#type' => 'html_tag',
            '#tag' => 'iframe',
            '#attributes' => [
              'srcdoc' => $opayo_3ds_submit_html,
              'allow' => 'fullscreen',
              'width' => 400,
              'height' => 390,
              'loading' => 'eager',
              'id' => ['commerce-opayo-3d-iframe'],
              'class' => ['commerce-opayo-3d-iframe'],
            ],
          ];

          // Settings for javascript
          $computed_settings = [
              'proceedButtonSelector' => $this->getProceedButtonQuerySelector(),
              'formQuerySelector' => $this->getFormQuerySelector(),
              'paymentGateway' => $payment_gateway->id(),
              'opayoTransactionId' => $opayo_transaction_id,
              'threeDSSessionData' => $threeds_session_data_base64,
              'logLevel' => $configuration['gateway_log_level'],
            ];
          if ($this->order != null
          ) {
            $computed_settings['orderId'] = $this->order->id();
          }
          // Pass the settings to the Javascript
          $pane_form['3Diframe']['#attached']['drupalSettings']['commerceopayopi']['opayo_3dsecure'] = $computed_settings;

          // Opayo transaction ID
          $pane_form['commerce-opayo-transaction-id'] = [
            '#type' => 'hidden',
            '#value' => $opayo_transaction_id,
            '#attributes' => [
              'id' => ['commerce-opayo-transaction-id'],
            ],
          ];
          // Opayo Order ID
          $pane_form['commerce-opayo-order-id'] = [
            '#type' => 'hidden',
            '#value' => $this->order->id(),
            '#attributes' => [
              'id' => ['commerce-opayo-order-id'],
            ],
          ];
          // result
          $pane_form['commerce-opayo-3d-result'] = [
            '#type' => 'hidden',
            '#value' => '',
            '#attributes' => [
              'id' => ['commerce-opayo-3d-result'],
            ],
          ];
          // May need to pass a message to the user from within the iFrame
          $pane_form['commerce-opayo-3d-user-message'] = [
            '#type' => 'hidden',
            '#value' => '',
            '#attributes' => [
              'id' => ['commerce-opayo-3d-user-message'],
            ],
          ];

          $this->logVerboseDebug('Opayo3DSReview::buildPaneForm, iFrame render array for: ' . $opayo_transaction_id . ', order id: ' . $this->order->id() . ', URL: ' . $url . 'creq: ' . $challenge_request_base64 . 'threeDSSessionData: ' . $threeds_session_data_base64);
        }
        else {
          // Don't use iFrame - present a form where the customer gets taken to the supplied URL when pressed

          $url = $opayo_transaction->getAcsUrl();
          $challenge_request_base64 = $opayo_transaction->getChallengeRequest();

          $pane_form['creq'] = [
            '#type' => 'hidden',
            '#value' => $challenge_request_base64,
            '#name' => 'creq',
          ];

          $pane_form['threeDSSessionData'] = [
            '#type' => 'hidden',
            '#value' => $threeds_session_data_base64,
            '#name' => 'threeDSSessionData',
          ];

          $pane_form['message'] = [
            '#attached' => [
              'library' => ['commerce_opayo_pi/opayo_pi_3dsecure_noiframe'],
            ],
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => 'Please click the button below to proceed to 3D secure.',
          ];

          // Settings for javascript
          $computed_settings = [
            'proceedButtonSelector' => $this->getProceedButtonQuerySelector(),
            'formQuerySelector' => $this->getFormQuerySelector(),
            'acsUrl' => $url,
          ];
          // Pass the settings to the Javascript
          $pane_form['message']['#attached']['drupalSettings']['commerceopayopi']['opayo_3dsecure_noiframe'] = $computed_settings;

          $this->logVerboseDebug('Opayo3DSReview::buildPaneForm, non-iFrame render array for: ' . $opayo_transaction_id . ', order id: ' . $this->order->id() . ', URL: ' . $url . 'creq: ' . $challenge_request_base64 . 'threeDSSessionData: ' . $threeds_session_data_base64);
        }
      }
      elseif (strlen($opayo_transaction->getChallengeRequest()) > 0 && $seconds_since_received >= 1200) {
        // 3DS challenge token has expired

        try {
          $this->logger->info('Opayo3DSReview::buildPaneForm: 3DS challenge token expired: {secondsSinceReceived} seconds, transaction ID: {transactionId} (order {orderId} (uid: {orderUID})', ['secondsSinceReceived' => $seconds_since_received, 'transactionId' => $opayo_transaction_id, 'orderId' => $this->order->id(), 'orderUID' => $this->order->uuid()]);
        } catch (Exception $le) {
        }

        // Feedback about the expiry
        $this->messenger()->addWarning('3D secure request has expired. You need to re-enter your card details');

        // Revert the checkout flow back tot the 'order_information' step
        $next_step_id = 'order_information';
        $this->checkoutFlow->redirectToStep($next_step_id);
      }
      else {
        // Unexpected
        try {
          $this->logger->error('Opayo3DSReview::buildPaneForm: technical issue: transaction ID: {transactionId} (order {orderId} (uid: {orderUID})', ['transactionId' => $opayo_transaction_id, 'orderId' => $this->order->id(), 'orderUID' => $this->order->uuid()]);
        } catch (Exception $le) {
        }
        $payment_gateway_plugin->logVerboseDebug('Opayo3DSReview::buildPaneForm, PaymentGatewayException: technical issue, Opayo transaction: ' . $opayo_transaction_id . ', order id: ' . $this->order->id());
        throw new PaymentGatewayException('Opayo3DSReview::buildPaneForm: technical issue.');
      }
    }
    else {
      // Something unexpected must have happened
      try {
        $this->logger->info('Opayo3DSReview::buildPaneForm: cannot get transaction information: transaction ID: {transactionId} (order {orderId} (uid: {orderUID})', ['transactionId' => $opayo_transaction_id, 'orderId' => $this->order->id(), 'orderUID' => $this->order->uuid()]);
      } catch (Exception $le) {
      }
      $payment_gateway_plugin->logVerboseDebug('Opayo3DSReview::buildPaneForm, PaymentGatewayException: 3DS review checkout pane cannot get transaction information, order id: ' . $this->order->id());
      throw new PaymentGatewayException('Opayo3DSReview::buildPaneForm: 3DS review checkout pane cannot get transaction information');
    }

    $cacheability = new CacheableMetadata();
    $cacheability->addCacheableDependency($this->order);
    $cacheability->setCacheMaxAge(0);
    $cacheability->applyTo($pane_form);

    return $pane_form;
  }

  /**
   * Normally the ID for the button that submits the card details form is 'edit-actions-next'
   *
   * @return string
   */
  protected function getProceedButtonQuerySelector() {
    return '[data-drupal-selector="edit-actions-next"]';
  }

  /**
   * Normally the query selector to identify the checkout form is form[id^="commerce-checkout"]
   *
   * @return string
   */
  protected function getFormQuerySelector() {
    return 'form[id^="commerce-checkout"]';
  }


  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    // Information about the results of the 3D Secure authentication is filled into hidden form fields by the javascript
    $userInput = $form_state->getUserInput();
    $message = '';
    $opayo_transaction_id = null;
    /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction */
    $opayo_transaction = null;
    $opayo_3ds_feedback = null;
    $checkout_flow_redirect_step = null;

    // Get the transaction ID from the form fields
    if ($userInput != null && array_key_exists('opayo_pi_3ds', $userInput)) {
      $opayo_3ds_feedback = $userInput['opayo_pi_3ds'];
      if (array_key_exists('commerce-opayo-transaction-id', $opayo_3ds_feedback)) {
        $opayo_transaction_id = $opayo_3ds_feedback['commerce-opayo-transaction-id'];
      }
    }

    if ($opayo_transaction_id != null) {
      $opayo_transaction = OpayoTransaction::load($opayo_transaction_id);
    }
    else {
      // Transaction ID not available via form fields, search the transaction entities for a matching one via the order
      try {
        $query = $this->entityTypeManager->getStorage('opayo_transaction')->getQuery();
        $query->accessCheck(FALSE);
        $query->condition('order', $this->order->id());
        $query->sort('received', 'DESC');
        $ids = $query->execute();
        if (count($ids) >= 1) {
          // The most recent one if there are several
          /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction */
          $opayo_transaction = $this->entityTypeManager->getStorage('opayo_transaction')->load(array_values($ids)[0]);
        }
      } catch (Exception $exception) {
      }
    }

    // Any message to display to the user?
    if (array_key_exists('commerce-opayo-3d-user-message', $opayo_3ds_feedback)) {
      // Any message to be presented to the user
      $message = \Drupal\Component\Utility\Html::escape($opayo_3ds_feedback['commerce-opayo-3d-user-message']);
      $this->messenger()->addMessage($message, \Drupal\Core\Messenger\MessengerInterface::TYPE_WARNING);
    }

    // Have the form fields supplied a result?
    if (array_key_exists('commerce-opayo-3d-result', $opayo_3ds_feedback)) {
      $result = $opayo_3ds_feedback['commerce-opayo-3d-result'];

      if (strlen($result) > 0 && $result != 'success') {
        $form_state->setErrorByName('3Diframe', '3D Authentication failure ' . $message);
        // Redirect checkout to 'order_information' state
        $checkout_flow_redirect_step = 'order_information';
      }
      // For 'Proceed' do nothing here
    }

    if ($opayo_transaction == null)
    {
      // Unexpected - log, set message and redirect to an earlier state
      $this->logger->error('Opayo3DSReview::validatePaneForm: could not match successful 3DS response for transaction ID: {transactionId} (order {orderId} (uid: {orderUID})', ['transactionId' => $opayo_transaction_id, 'orderId' => $this->order->id(), 'orderUID' => $this->order->uuid()]);
      $form_state->setErrorByName('3Diframe', '3D Authentication failure ' . $message);
      // Redirect checkout to 'order_information' state
      $this->checkoutFlow->redirectToStep('order_information');
    }

    if ($checkout_flow_redirect_step != null) {
      if ($checkout_flow_redirect_step == 'order_information')
      {
        // If the reason for the 3DS failure was a decline then the 'saved' payment details should not be reused
        if ($opayo_transaction != null && ($opayo_transaction->getStatus() == 'NotAuthed' || $opayo_transaction->getStatusCode() =='2000' || $opayo_transaction->getStatusCode() == '4026')) {
          $this->order->set('payment_gateway', null);
          $this->order->set('payment_method', null);
          $this->order->save();
          $this->logger->info('Opayo3DSReview::validatePaneForm: unset payment method and payment gateway for order {orderId} after 3DS decline.', ['orderId' => $this->order->id()]);
        }
      }
      $this->checkoutFlow->redirectToStep($checkout_flow_redirect_step);
    }

    // Default is success - let the checkout flow dictate the logic
    // (in case of success we have already 'moved on' the order's checkout flow state without involving the regular
    //  checkout flow processes so these methods of 'Opayo3CSReview' will not be called on a successful submit - because the state has moved on to 'complete')
  }

  protected function logVerboseDebug(string|\Stringable $message, array $context = []) {
    if ($this->payment_gateway_plugin == null && $this->order != null && $this->order->get('payment_gateway') != null && $this->order->get('payment_gateway')->entity != null && $this->order->get('payment_gateway')->entity->getPlugin() != null)
    $this->payment_gateway_plugin = $this->order->get('payment_gateway')->entity->getPlugin();

    if ($this->payment_gateway_plugin != null)
      $this->payment_gateway_plugin->logVerboseDebug($message, $context);
    else
      $this->logger->debug($message, $context);
  }
}
