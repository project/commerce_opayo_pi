<?php

namespace Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_opayo_pi\Entity\OpayoTransaction;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsCreatingPaymentMethodsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use stdClass;

/**
 * Interface for the Opayo Pi Payment Gateway
 *
 * SupportsCreatingPaymentMethodsInterface is where any 'tokenisation' (which is part of the Opayo Pi process) happens according to Drupal Commerce documentation
 */
interface OpayoPiPaymentGatewayInterface extends OnsitePaymentGatewayInterface, SupportsRefundsInterface, SupportsCreatingPaymentMethodsInterface {

  /**
   * Get the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface:null
   *   The order.
   */
  public function getOrder();

  /**
   * Set the order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  public function setOrder(OrderInterface $order);

  /**
   * Get the payment gateway
   *
   * @return \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   *   The payment gateway
   */
  public function getPaymentGateway();

  /**
   * Returns an Opayo merchant session key
   *
   * @param string $orderId
   *  Identification for the order, normally 'uuid'
   * @return stdClass
   */
  public function getNewMerchantSessionKey($orderId);

  /**
   * Returns the last requested Opayo merchant session key
   *
   * @param string $order_id
   *  Identification for the order, normally 'uuid'
   *
   * @return stdClass
   */
  public function getCurrentMerchantSessionKey($order_id);

  /**
   * Invalidates the merchant session key
   *
   * A new Merchant Session Key must be created each time you load the payment page, or when an existing Merchant Session Key expires
   * (expiry after 400 seconds or after 3 failed attempts to create a Card Identifier)
   *
   * @param string $order_id
   *  Identification for the order, normally 'uuid'
   *
   */
  public function invalidateCurrentMerchantSessionKey($order_id);

  /**
   * Invalidates the merchant session key
   *
   * A new Merchant Session Key must be created each time you load the payment page, or when an existing Merchant Session Key expires
   * (expiry after 400 seconds or after 3 failed attempts to create a Card Identifier)
   *
   * @param string $order_id
   *  Identification for the order, normally 'uuid'
   * @param string $merchant_session_key
   *  The merchant session key to invalidate
   *
   */
  public function invalidateMerchantSessionKey($order_id, $merchant_session_key);

  /**
   * Delete all retained merchant session keys for the order.
   *
   * Normally called after succesful order completion
   *
   * @param string $order_id
   *  Identification for the order, normally 'uuid'
   *
   */
  public function deleteAllMerchantSessionKeys($order_id);


  /**
   * Process the result of 3D Secure Authentication
   *
   * @param OrderInterface $order
   *  The order
   * @param OpayoTransaction $opayoTransaction
   *  Entity representing the Opayo transaction
   * @param string $cRes
   *  3D Secure challenge result
   * @return bool
   */
  public function process3DSAuthenticationResult(OrderInterface &$order, OpayoTransaction $opayoTransaction, string $cRes);


  /**
   * Get transaction record from Opayo by ID
   *
   * @param string $opayoTransactionId
   *    Opayo's 'Transaction Id'
   * @param stdClass $response
   *    Opayo response contents
   *
   * @return \Drupal\commerce_opayo_pi\Entity\OpayoTransaction
   *    Entity encapsulating the Opayo transaction's info
   */
  public function getOpayoPiTransaction(string$opayoTransactionId, ?stdClass &$response, ?OpayoTransaction &$opayoTransaction);


  /**
   * Get transaction status from Opayo by 'Vendor Tx Code'
   *
   * @param string $vendorTxCode
   *   'Vendor Tx Code' supplied by us
   *
   * @return string
   *    Numeric code representing the status of the Transaction on Opayo
   */
  public function getOpayoPiTransactionStatusByVendorTxCode(string $vendorTxCode, string &$opayo_transaction_id);

  /**
   * Log a message at the payment gateway's 'extra verbose debug' level
   *
   * @param string|\Stringable $message
   *   Log message
   * @param array $context
   *   Additional info
   */
  public function logVerboseDebug(string|\Stringable $message, array $context = []);

  /**
   * Log a message via the standard logger(s)
   *
   * @param $level
   *   Log level
   * @param string|\Stringable $message
   *   Log message
   * @param array $context
   *   Additional info
   */
  public function log($level, string|\Stringable $message, array $context = []): void;



}
