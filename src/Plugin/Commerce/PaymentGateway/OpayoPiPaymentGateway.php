<?php

namespace Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway;

use DateTime;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_price\Calculator;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Exception;
use stdClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\commerce_opayo_pi\Entity\OpayoTransaction;
use Drupal\commerce_opayo_pi\Entity\OpayoTransactionInterface;
use Drupal\commerce_opayo_pi\OpayoPi;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Logger\RfcLogLevel;
use Psr\Log\LogLevel;

/**
 * Provides an On-site payment gateway supporing Opayo Pi integration.
 *
 * It uses an Opayo provided 'sagepay.js' script for the browser to tokenise the card details into a 'card identifier'.
 * The 'card identifier' is posted back during checkout and can then be used to submit the payment transaction to Opayo from the commerce server.
 * Card details themselves are only exchanged between the client's browser and Opayo, thereby simplifying PCI compliance.
 *
 * @CommercePaymentGateway(
 *   id = "opayo_pi",
 *   label = "Opayo (Pi integration)",
 *   display_label = "Opayo (Pi integration)",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_opayo_pi\PluginForm\OpayoPi\PaymentMethodAddForm",
 *   },
 *   payment_method_types = {"credit_card_opayo"},
 *   credit_card_types = {
 *     "amex", "maestro", "mastercard", "visa",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class OpayoPiPaymentGateway extends OnsitePaymentGatewayBase implements OpayoPiPaymentGatewayInterface {

  const LOGLEVEL_VERBOSEDEBUG = 10;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The current store.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected $currentStore;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Private storage.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore $privateTempStore
   */
  protected $privateTempStore;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * Logger
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The request stack
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The module's service
   *
   * @var \Drupal\commerce_opayo_pi\OpayoPi
   */
  protected $opayoPiService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('event_dispatcher'),
      $container->get('commerce_store.current_store'),
      $container->get('current_route_match'),
      $container->get('tempstore.private'),
      $container->get('commerce_opayo_pi.logger.channel.commerce_opayo_pi'),
      $container->get('request_stack'),
      $container->get('commerce_opayo_pi.opayo_pi'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, EventDispatcherInterface $event_dispatcher, CurrentStoreInterface $current_store, RouteMatchInterface $route_match, PrivateTempStoreFactory $private_temp_store, LoggerChannelInterface$logger, RequestStack $requestStack, OpayoPi $opayoPi, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->eventDispatcher = $event_dispatcher;
    $this->currentStore = $current_store;
    $this->routeMatch = $route_match;
    $this->privateTempStore = $private_temp_store->get('commerce_opayo_pi');
    $this->logger = $logger;
    $this->requestStack = $requestStack;
    $this->opayoPiService = $opayoPi;
    $this->moduleHandler = $moduleHandler;
  }


  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'form_type' => 'ownform',
      'telephone_source' => 'text',
      'telephone_field_name' => '',
      'opayo_order_description' => '',
      'transaction_timeout' => '180',
      'gateway_log_level' => RfcLogLevel::INFO,
      '3d_secure' => 'UseMSPSetting',
      'opayo_apply_avs_cvc' => 'UseMSPSetting',
      '3d_iframe' => 'true',
      'test_with_deferred_payment' => 'false',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['form_type'] = [
      '#type' => 'radios',
      '#title' => t('Form Type'),
      '#description' => t('Use Commerce form or Opayo Drop-In for sensitive checkout fields'),
      '#options' => [
        'ownform' => t('Use custom Commerce form - more customer-friendly checkout experience but higher PCI compliance (SAQ-A-EP) burden. (default)'),
        'dropin' => t('Use Opayo Drop-In form - easier PCI compliance (SAQ-A) but less customer friendly checkout experience.'),
      ],
      '#default_value' => $this->configuration['form_type'],
    ];

    $form['telephone_source'] = [
      '#type' => 'radios',
      '#title' => t('Telephone number field type'),
      '#required' => TRUE,
      '#description' => t('A phone number in international format (e.g. +44) is required by Opayo. Choose the type of field used by the gateway'),
      '#options' => [
        'phone_international' => t('Field of type \'Phone International\' added to the payment information pane.'),
        'text' => t('Field of type \'text\' added to the payment information pane.'),
        'profile' => t('Expect a Telephone number field added to the \'Customer\' profile type. Copy its field name into the \'Customer Profile Type telephone number field machine name\' field below.'),
      ],
      '#default_value' => $this->configuration['telephone_source'],
    ];

    $form['telephone_field_name'] = [
      '#type' => 'textfield',
      '#title' => t('Customer Profile Type telephone number field machine name'),
      '#description' => t('If you have chosen to have the phone number field in the customer profile then put its field name here'),
      '#default_value' => $this->configuration['telephone_field_name'],
    ];

    $form['opayo_order_description'] = [
      '#type' => 'textfield',
      '#title' => t('Order Description'),
      '#description' => $this->t('The description of the order that will appear in the Opayo transaction. (For example, Your order from sitename.com)'),
      '#default_value' => $this->configuration['opayo_order_description'],
    ];

    $form['transaction_timeout'] = [
      '#type' => 'textfield',
      '#title' => t('Timeout for the transaction POST'),
      '#description' => t('Timeout in seconds for the operation that posts the payment transaction to Opayo'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['transaction_timeout'],
    ];

    $form['opayo_apply_avs_cvc'] = [
      '#type' => 'radios',
      '#title' => t('AVS/CVC Mode'),
      '#description' => t('Use this field to override your default account level AVS CVC settings'),
      '#options' => [
        'UseMSPSetting' => $this->t('UseMSPSetting - Use default MyOpayo settings'),
        'Force' => $this->t('Force - Apply authentication even if turned off'),
        'Disable' => $this->t('Disable authentication and rules'),
        'ForceIgnoringRules' => $this->t('Only use this if you intend to use an SCA Exemption'),
      ],
      '#default_value' => $this->configuration['opayo_apply_avs_cvc'],
    ];

    $form['gateway_log_level'] = [
      '#type' => 'radios',
      '#title' => t('Gateway log level'),
      '#description' => t('Used to temporarily change the logging level for the gateway'),
      '#options' => [
        self::LOGLEVEL_VERBOSEDEBUG => $this->t('Extra Verbose Debugging Info'),
        RfcLogLevel::DEBUG => $this->t('Debug'),
        RfcLogLevel::INFO => $this->t('Info'),
        RfcLogLevel::WARNING => $this->t('Warning'),
        RfcLogLevel::ERROR => $this->t('Error'),
      ],
      '#default_value' => $this->configuration['gateway_log_level'],
    ];

    $form['3d_secure'] = [
      '#type' => 'radios',
      '#title' => $this->t('3D Secure'),
      '#description' => t('Use this field to override your default account level 3D Secure settings'),
      '#options' => [
        'UseMSPSetting' => $this->t('UseMSPSetting - Use default MyOpayo settings'),
        'Force' => $this->t('Force - Apply authentication even if turned off'),
        'Disable' => $this->t('Only use this if you intend to use an SCA Exemption'),
      ],
      '#default_value' => $this->configuration['3d_secure'],
    ];

    $form['3d_iframe'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('3D Secure challenge inside iFrame'),
      '#description' => $this->t('Check to have the 3D secure challenge inside an iFrame.'),
      '#default_value' => $this->configuration['3d_iframe'],
    ];

    $form['test_with_deferred_payment'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Deferred instead of Payment transaction for testing'),
      '#description' => $this->t('To test transactions with live payment gateway: deferred payments can be aborted afterwards from Opayo merchant portal'),
      '#default_value' => $this->configuration['test_with_deferred_payment'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['form_type'] = $values['form_type'];
      $this->configuration['opayo_order_description'] = $values['opayo_order_description'];
      $this->configuration['opayo_apply_avs_cvc'] = $values['opayo_apply_avs_cvc'];
      $this->configuration['telephone_source'] = $values['telephone_source'];
      $this->configuration['telephone_field_name'] = $values['telephone_field_name'];
      $this->configuration['transaction_timeout'] = $values['transaction_timeout'];
      $this->configuration['gateway_log_level'] = $values['gateway_log_level'];
      $this->configuration['3d_secure'] = $values['3d_secure'];
      $this->configuration['3d_iframe'] = $values['3d_iframe'];
      $this->configuration['test_with_deferred_payment'] = $values['test_with_deferred_payment'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {

    // Add the module's settings to the payment gateway config
    $this->configuration = array_merge($this->configuration, $this->opayoPiService->getSettings());

    if ($this->configuration['mode'] == 'test') {
      $this->configuration['uri']['merchant_session_key'] = 'https://sandbox.opayo.eu.elavon.com/api/v1/merchant-session-keys/';
      $this->configuration['uri']['card_identifiers'] = 'https://sandbox.opayo.eu.elavon.com/api/v1/card-identifiers/';
      $this->configuration['uri']['transactions'] = 'https://sandbox.opayo.eu.elavon.com/api/v1/transactions/';
      $this->configuration['uri']['access'] = 'https://sandbox.opayo.eu.elavon.com/access/access.htm';
      if (array_key_exists('test_integration_key', $this->configuration) && array_key_exists('test_integration_password', $this->configuration))
        $this->configuration['auth'] = base64_encode($this->configuration['test_integration_key'] . ":" . $this->configuration['test_integration_password']);
      $this->configuration['account_user'] = $this->configuration['test_account_user'];
      $this->configuration['account_password'] = $this->configuration['test_account_password'];
    }
    else {
      $this->configuration['uri']['merchant_session_key'] = 'https://live.opayo.eu.elavon.com/api/v1/merchant-session-keys/';
      $this->configuration['uri']['card_identifiers'] = 'https://live.opayo.eu.elavon.com/api/v1/card-identifiers/';
      $this->configuration['uri']['transactions'] = 'https://live.opayo.eu.elavon.com/api/v1/transactions/';
      $this->configuration['uri']['access'] = 'https://live.opayo.eu.elavon.com/access/access.htm';
      if (array_key_exists('live_integration_key', $this->configuration) && array_key_exists('live_integration_password', $this->configuration))
        $this->configuration['auth'] = base64_encode($this->configuration['live_integration_key'] . ":" . $this->configuration['live_integration_password']);
      $this->configuration['account_user'] = $this->configuration['live_account_user'];
      $this->configuration['account_password'] = $this->configuration['live_account_password'];
    }
    $this->configuration['website'] = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    $this->configuration['siteFqdns'] = ['test' => $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost(), 'live' => $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost()];

    return $this->configuration;
  }



  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {

    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $order = $payment->getOrder();

    $this->logVerboseDebug('OpayoPiPaymentGateway::createPayment called for cart ' . $order->id());

    // New transaction (not a repeat payment)
    $updated_payment = $this->submitNewOpayoPiTransaction($order, $payment, $payment_method, $capture);

    if ($updated_payment != null && $updated_payment->id() > 0)
      $this->logVerboseDebug('OpayoPiPaymentGateway::createPayment completed for payment ' . $updated_payment->id() . ' and cart ' . $order->id());
    else
      $this->logVerboseDebug('OpayoPiPaymentGateway::createPayment completed for cart ' . $order->id());
  }


  /**
   * Submit the (payment) transaction request to Opayo
   *
   * @param OrderInterface $order
   *   Order details
   * @param PaymentInterface $payment
   *   Payment settings
   * @param bool $capture
   *   Whether to 'capture' the payment from the acquiring merchant
   *
   * @return PaymentInterface $payment
   *   The newly saved payment
   */
  public function submitNewOpayoPiTransaction(OrderInterface $order, PaymentInterface $payment, PaymentMethodInterface $payment_method, $capture = TRUE) {

    $configuration = $this->getConfiguration();
    $url = $configuration['uri']['transactions'];
    $curl = curl_init();
    $vendor_tx_code = null;

    $this->logVerboseDebug('OpayoPiPaymentGateway::submitNewOpayoPiTransaction called for cart ' . $order->id() . ' using URL: ' . $url);

    // Construct the contents of the body of the transaction request post, ready for conversion into JSON format
    /** @var stdClass $extra_info */
    $extra_info = null;
    $payload = $this->transactionRequestContents($order, $payment, $configuration, $extra_info);
    $payload_json = json_encode($payload);
    $vendor_tx_code = $payload->vendorTxCode;

    $this->log(LogLevel::DEBUG, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId} (uid: {orderUID}), mode: {mode}, create transaction payload: {payload}', ['mode' => $configuration['mode'], 'orderId' => $this->getOrder()->id(), 'orderUID' => $this->getOrder()->uuid(), 'payload' => $payload_json]);

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $payload_json,
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic " . $configuration['auth'],
        "Cache-Control: no-cache",
        "Content-Type: application/json"
      ),
    ));

    //Tell cURL that it should only spend 20 seconds
    //trying to connect to the URL in question.
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
    // Set a timeout for the requests, TODO currently set to 3 minutes - may need to be configurable
    $timeout = intval($configuration['transaction_timeout']);
    curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
    $raw_response = null;
    $response = null;
    $response_array = null;

    try {

      // Execute the request (presumably synchronously)
      $this->log(LogLevel::DEBUG, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId}, payment transaction request using {url}', ['orderId' => $this->getOrder()->id(), 'url' => $url]);
      $raw_response = curl_exec($curl);
      $http_code = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);

      // Check the response
      $err = curl_error($curl);
      if (!empty($err)) {
        $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId} (uid: {orderUID}), mode: {mode}, transaction request error for: {url}, error: {error}',
          ['mode' => $configuration['mode'], 'orderId' => $this->getOrder()->id(), 'orderUID' => $this->getOrder()->uuid(), 'url' => $url, 'error' => print_r($err, TRUE)]);
        $this->gatewayFailureResponse('Transport error during Opayo Pi transaction request, error: ' . print_r($err, TRUE));
      }
      else {
        $response = json_decode($raw_response);
        $response_array = json_decode($raw_response, TRUE);
      }
      $this->log(LogLevel::DEBUG, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId}, payment transaction request completed', ['orderId' => $this->getOrder()->id()]);
    }
    catch (Exception $e) {
      $this->log(LogLevel::ERROR, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId}, URL: {url}, exception during payment transaction request: {exception}', ['exception' => ($e->getMessage() . ' ' . $e->getTraceAsString()), 'orderId' => $this->getOrder()->id(), 'url' => $url]);
    }
    finally {
      curl_close($curl);

      // The merchant session key and the card identifier from the payment method have been 'used' and cannot be reused
      $payment_method->setRemoteId(null);
      $payment_method->save();
      $this->log(LogLevel::DEBUG, "OpayoPiPaymentGateway::submitNewOpayoPiTransaction: re-set payment method {paymentMethodId} Remote Id to 'null' to indicate single-use MSK and card identifier have been 'used'.", ['paymentMethodId' => $payment_method->id()]);
    }

    if ($raw_response != null && $response != null) {
      // Successfully received a response to the request

      try {
        $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId}, response payload: {payload}', ['payload' => $raw_response, 'orderId' => $this->getOrder()->id()]);
      } catch (Exception $le) {}

      if (($http_code != 201 && $http_code != 202) || property_exists($response, 'errors')) {
        // Response from Opayo but errors detected at their end
        try {
          $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId}, payment transaction failed with response http code {httpcode}', ['orderId' => $this->getOrder()->id(), 'httpcode' => $http_code]);
        } catch (Exception $le) {}

        $decline_message = '';
        $technical_message = '';

        $opayo_errors = array();

        if (property_exists($response, 'errors')) {
          // Opayo response returned one or more error
          $opayo_errors = array_merge($opayo_errors, $response->errors);
        }
        else {
          // Assume single error
          $opayo_errors[] = $response;
        }
        /** @var StdClass $opayo_error */
        foreach($opayo_errors as $opayo_error)
        {
          try {
            $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId}, transaction response error {errorCode}, property {property}, error description: ' . $opayo_error->description,
              ['orderId' => $this->getOrder()->id(), 'errorCode' => $opayo_error->code, 'property' => $opayo_error->property]);
          }
          catch (Exception $e){
            $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId}, transaction response has error, error: {error}',
              ['orderId' => $this->getOrder()->id(), 'error' => print_r($opayo_error, TRUE)]);
          }
          if ($opayo_error->code == 1011) {
            $decline_message = $decline_message . (strlen($decline_message) == 0 ? '' : ', ') . $opayo_error->description;

            // Code 1011 implies 'Merchant Session Keys or card identifier invalid', revert order back to information state
            $this->resetCheckoutFlow($order);
          }
          else {
            $technical_message = $technical_message . (strlen($technical_message) == 0 ? '' : ', ') . $opayo_error->description;
          }
        }

        $this->orderResetPaymentMethod($this->getOrder());

        if ($decline_message != "")
        {
          $this->storePaymentErrorMessage($this->getOrder()->id(), $decline_message);
          $this->paymentDeclinedReponse('Errors in Opayo Pi transaction response, HTTP code: ' . $http_code . ', ' . $decline_message);
        }
        else {
          $this->storePaymentErrorMessage($this->getOrder()->id(), $technical_message);
          $this->gatewayFailureResponse('Errors in Opayo Pi transaction response, HTTP code: ' . $http_code . ', ' . $technical_message);
        }
      }
      elseif ($http_code == 201 && property_exists($response, 'statusCode') && $response->statusCode != 0) {
        // declined/rejected
        $status_detail = $response->statusDetail;
        $status = $response->status;
        $transaction_id = $response->transactionId;

        $this->log(LogLevel::WARNING,
          'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId} payment transaction failed (ID: {transactionId}) with status: {status} ({statusCode} - {statusDetail})',
          ['orderId' => $this->getOrder()->id(), 'transactionId' => $transaction_id, 'statusCode' => $response->statusCode, 'status' => $status, 'statusDetail' => $status_detail]
        );

        $this->orderResetPaymentMethod($this->getOrder());

        if ($response->statusCode == 3336 || $response->statusCode == 3069 || $response->statusCode == 4009 || $response->statusCode == 4027 || $response->statusCode == 4035 || $response->statusCode == 5055) {
          // Declined for logical reasons
          $this->storePaymentErrorMessage($this->getOrder()->id(), $status_detail);
          $this->paymentDeclinedReponse($status_detail);
        }
        else {
          // Failure is technical
          $this->storePaymentErrorMessage($this->getOrder()->id(), $status_detail);
          $this->gatewayFailureResponse($status_detail);
        }
      }
      elseif ($http_code == 201 && property_exists($response, 'statusCode') && $response->statusCode == 0) {
        // Successful payment
        $transaction_id = '';
        if (property_exists($response, 'transactionId'))
          $transaction_id = $response->transactionId;

        // Process successfully authenticated payment
        $this->processAuthorisedPaymentTransaction($order, $payment, $payment_method, $capture, $transaction_id, $url, $response, $response_array, $configuration, $payload);
      }
      elseif ($http_code == 202 && property_exists($response, 'cReq')) {
        $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId}, payment transaction response calls for 3D Auth step', ['orderId' => $this->getOrder()->id()]);

        $transaction_id = '';
        if (property_exists($response, 'transactionId')) {
          $transaction_id = $response->transactionId;
          $payment_method->setRemoteId($transaction_id); // Set the remote ID to Opayo's transaction ID - to be matched during 3D secure
          $payment_method->save();
        }

        $this->logVerboseDebug('OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId}, transaction ID: {transactionId} saved with payment method {paymentMethod}', ['orderId' => $this->getOrder()->id(), 'transactionId' => $transaction_id, 'paymentMethod' => $payment_method->id()]);

        // Save the Opayo transaction response info, 3D Secure checkout pane behaviour will depend on its contents
        $opayo_transaction = $this->createOpayoTransactionFromResponseForwardCustomer($response, $payload, $url, $configuration);
        $opayo_transaction->set('order', $order);
        $opayo_transaction->setVendorTxCode($vendor_tx_code);
        if ($extra_info != null && property_exists($extra_info, 'cardLastFourDigits'))
          $opayo_transaction->set('card_last_four_digits',$extra_info->cardLastFourDigits); // Save the payment card last four digits logging and tracing
        $opayo_transaction->save();
        $this->logVerboseDebug('OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId}, new Opayo transaction entity {transactionId} saved', ['orderId' => $this->getOrder()->id(), 'transactionId' => $opayo_transaction->getTransactionId()]);

        // Note: checkout pane 'opayo_pi_3ds' will examine the $payment_method object and matching 'OpayoTransaction' entry for displaying a 3DS notice to the customer
      }
      else {
        $this->log(LogLevel::WARNING,
          'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: order {orderId} (uid: {orderUID}), mode: {mode}, unexpected result for: {url}, HTTP code {httpCode}',
          ['mode' => $configuration['mode'], 'orderId' => $this->getOrder()->id(), 'orderUID' => $this->getOrder()->uuid(), 'url' => $url, 'httpCode' => $http_code]
        );
        $this->gatewayFailureResponse('Unexpected response in Opayo Pi transaction response, HTTP code: ' . $http_code);
      }
    }
    else {
      // No response from the request - result of the payment request is unclear
      $url = $configuration['uri']['access'];
      $account_user = $configuration['account_user'];
      $account_password = $configuration['account_password'];
      if (isset($account_user) && isset($account_password) && isset($url)) {
        // The only way (at the moment) to query an Opayo transaction by the 'Vendor TX Code' (at this stage we don't have a transaction ID yet)
        // is to use its 'reporting' API, it cannot be done through the Opayo Pi API.
        // This need a username and password with sufficient access to query

        // Request info about the transaction from Opayo
        $success = false;
        $failed = false;
        $transaction_status = -1;
        try {
          $retry = 20;
          $transaction_id = '';
          while ($retry && $success == false && $failed != true) {
            $transaction_status = $this->getOpayoPiTransactionStatusByVendorTxCode($vendor_tx_code, $transaction_id);
            if ($transaction_status != null) {
              switch($transaction_status) {
                // Successfull authentication
                case "16":
                case "29":
                case "5":
                case "6":
                case "7":
                  $success = true;
                  break;
                case "2":
                case "3":
                case "4":
                case "14":
                case "15":
                case "21":
                case "25":
                case "28":
                case "38":
                case "39":
                  // Opayo thinks transaction is still 'in progress'
                  break;
                default:
                  // Opayo aware of payment request but failed
                  $failed = true;
                  break;
              }
            }
            if ($success == false && $failed != true) {
              sleep(5);
            }
            $retry = $retry - 5;
          }
        }
        catch (Exception $e1) {
          // Cannot get the status of the transaction, must assume failure
          $this->orderResetPaymentMethod($this->getOrder());
          $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction cannot get transaction status for \'Vendor Tx Code\'' . $vendor_tx_code);
          $this->gatewayFailureResponse('cannot get transaction status for \'Vendor Tx Code\'' . $vendor_tx_code);
        }

        if ($success == false || $transaction_id == null) {
          // After 20 seconds of trying to get the transaction's status it's still not succesful, assume failure
          $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::createPayment non-success status code '. $transaction_status . ' for \'Vendor Tx Code\'' . $vendor_tx_code);
          $this->orderResetPaymentMethod($this->getOrder());
          $this->gatewayFailureResponse('non-success status code ' . $transaction_status . ' for \'Vendor Tx Code\'' . $vendor_tx_code);
        }
        else {
          if ($transaction_id != null) {
            // Payment transaction request successful - get more details
            $opayo_transaction = $this->getOpayoPiTransaction($transaction_id, $response);

            if ($opayo_transaction != null) {

              $opayo_transaction->set('request_payload', $payload_json);
              $opayo_transaction->set('request_url', $url);

              if ($opayo_transaction->getStatusCode() == "0000") {
                // Payment succesfully authorised
                $this->processAuthorisedPaymentTransaction($order, $payment, $payment_method, $capture, $transaction_id, $url, null, null, $configuration, $payload, $opayo_transaction);
              }
              elseif ($opayo_transaction->getStatus() == '3DAuth') {
                // Need 3D secure authentication
                $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId}, payment transaction response calls for 3D Auth step', ['orderId' => $this->getOrder()->id()]);
                $payment_method->setRemoteId($transaction_id); // Set the remote ID to Opayo's transaction ID - to be matched during 3D secure
                $payment_method->save();
                $this->logVerboseDebug('OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId}, transaction ID: {transactionId} saved with payment method {paymentMethod}', ['orderId' => $this->getOrder()->id(), 'transactionId' => $transaction_id, 'paymentMethod' => $payment_method->id()]);

                // Add any fields related to the 3DS forwarding
                if (property_exists($response, 'acsTransId'))
                  $opayo_transaction->set('acs_trans_id', $response->acsTransId);
                if (property_exists($response, 'dsTransId'))
                  $opayo_transaction->set('ds_trans_id', $response->dsTransId);

                // Save the Opayo transaction response info, 3D Secure checkout pane behaviour will depend on its contents
                $opayo_transaction->set('order', $order);
                $opayo_transaction->setVendorTxCode($vendor_tx_code);
                if ($extra_info != null && property_exists($extra_info, 'cardLastFourDigits'))
                  $opayo_transaction->set('card_last_four_digits', $extra_info->cardLastFourDigits); // Save the payment card last four digits logging and tracing
                $opayo_transaction->save();
                $this->logVerboseDebug('OpayoPiPaymentGateway::submitNewOpayoPiTransaction: cart {orderId}, new Opayo transaction entity {transactionId} saved', ['orderId' => $this->getOrder()->id(), 'transactionId' => $opayo_transaction->getTransactionId()]);
              }
              else {
                $this->orderResetPaymentMethod($this->getOrder());
                $this->logVerboseDebug('OpayoPiPaymentGateway::submitNewOpayoPiTransaction: unexpected Opayo transaction status ' . $opayo_transaction->getStatus() . 'code: ' . $opayo_transaction->getStatusCode() . ' for order ' . $order->id());
                $this->gatewayFailureResponse('OpayoPiPaymentGateway::submitNewOpayoPiTransaction unexpected Opayo transaction status ' . $opayo_transaction->getStatus() . 'code: ' . $opayo_transaction->getStatusCode() . ' for order ' . $order->id());
              }
            }
            else {
              $this->orderResetPaymentMethod($this->getOrder());
              $this->logVerboseDebug('OpayoPiPaymentGateway::submitNewOpayoPiTransaction: unexpected failure to retrieve Opayo transaction for ID ' . $transaction_id . ' for order ' . $order->id());
              $this->gatewayFailureResponse('OpayoPiPaymentGateway::submitNewOpayoPiTransaction unexpected failure to retrieve Opayo transaction for ID ' . $transaction_id . ' for order ' . $order->id());
            }
          }
          else {
            $this->orderResetPaymentMethod($this->getOrder());
            $this->logVerboseDebug('OpayoPiPaymentGateway::submitNewOpayoPiTransaction: unable to get Opayo transaction for Vendor TX Code ' . $vendor_tx_code . ' for order ' . $order->id());
            $this->gatewayFailureResponse('OpayoPiPaymentGateway::submitNewOpayoPiTransaction unable to get Opayo transaction for Vendor TX Code ' . $vendor_tx_code . ' for order ' . $order->id());
          }
        }
      }
      else {
        // No means to query the status of the payment authentication - safest option is to assume authentication has failed
        $this->log(LogLevel::ERROR, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: order {orderId} (uid: {orderUID}), mode: {mode}, cannot establish success of payment authentication request', ['mode' => $configuration['mode'], 'orderId' => $this->getOrder()->id(), 'orderUID' => $this->getOrder()->uuid()]);
        $this->orderResetPaymentMethod($this->getOrder());
        $this->gatewayFailureResponse('OpayoPiPaymentGateway::submitNewOpayoPiTransaction cannot establish success of payment authentication request for order ' . $order->id());
      }
    }

    return $payment;
  }


  /**
   * Internal household tasks after a successful payment authorisation
   *
   * @param OrderInterface $order
   *    the order
   * @param PaymentInterface $payment
   *    the payment
   * @param PaymentMethodInterface $payment_method
   *    the payment method
   * @param bool $capture
   *    true if the payment is captured as well as authorised
   * @param string $transaction_id
   *    Opayo transaction ID
   * @param string $url
   *   URL used for the payment request
   * @param StdClass $response
   *   Response from Opayo as a JSON decoded StdClass
   * @param array $responseArray
   *   Response as associative array
   * @param array $configuration
   *   Gateway configuration settings
   * @param stdClass $payload
   *   The JSON payload of the original request
   * @param \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction
   *   Entity representing the Opyao transaction
   */
  protected function processAuthorisedPaymentTransaction(OrderInterface $order, PaymentInterface $payment, PaymentMethodInterface $payment_method, bool $capture, string $transaction_id, string $url, ?stdClass $response, ?array $response_array, array $configuration, ?stdClass $payload, ?OpayoTransaction $opayo_transaction = null) {

    try {
      // Log successfull transaction response
      $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::processAuthorisedPaymentTransaction: cart {orderId} payment transaction success response (HTTP code: 201, status code: 0, transaction ID: {transactionId}): {url}', ['transactionId' => $transaction_id, 'orderId' => $this->getOrder()->id(), 'url' => $url]);
    } catch (Exception $le) {
    }

    $order_state = $order->get('state')->value;
    $payment_state = $payment->get('state')->value;
    $payment_save_result = -1;

    try {
      // Log intention to update status of the order and saving the new payment
      $this->log(
        LogLevel::INFO,
        'OpayoPiPaymentGateway::processAuthorisedPaymentTransaction: cart {orderId}, about to save payment {paymentId} with current state {paymentState} changed to {newPaymentState}, transaction ID: {transactionId}',
        ['transactionId' => $transaction_id, 'orderId' => $this->getOrder()->id(), 'paymentId' => $payment->getRemoteId(), 'paymentState' => $payment_state, 'newPaymentState' => ($capture ? 'completed' : 'authorization')]
      );
    } catch (Exception $le) {
    }

    // Transaction successful - no 3DS processing needed

    // No need to save the order at this stage
    // The checkout flow will find out the next 'stage' by using 'getNextStepId', this should be the 'completed' step.
    // It will then call 'onStepChange' which will call $order->getState()->applyTransitionById('place') which sets the order to completed (if the next step is 'completed').

    try {
      // Save the commerce payment

      // Update the state of the payment and save
      $next_state = $capture ? 'completed' : 'authorization';
      $payment->setState($next_state);
      $payment->setRemoteId($transaction_id); // Set the remote ID to Opayo's transaction ID
      $payment_save_result = $payment->save();
      try {
        // Log saving of the payment
        $this->log(
          LogLevel::INFO,
          'OpayoPiPaymentGateway::processAuthorisedPaymentTransaction: cart {orderId}, successfully saved payment {paymentId}, remote ID set to: {transactionId}',
          ['transactionId' => $transaction_id, 'orderId' => $this->getOrder()->id(), 'paymentId' => $payment->id()]
        );
      } catch (Exception $le) {
      }

      // Update the payment method's remote ID to the Opayo transaction ID so that the 3D secure phase can find the Opayo transaction info
      $payment_method->setRemoteId($transaction_id);
      $payment_method->save();
    }
    catch (Exception $e) {
      // Log exception details
      $this->log(LogLevel::ERROR, 'OpayoPiPaymentGateway::processAuthorisedPaymentTransaction: cart {orderId}, payment {paymentId}, Opayo transaction ID: {transactionId}, URL: {url}, internal exception: {exception}', ['exception' => ($e->getMessage() . ' ' . $e->getTraceAsString()), 'paymentId' => $payment->getRemoteId(), 'transactionId' => $transaction_id, 'orderId' => $this->getOrder()->id(), 'url' => $url]);
      $this->gatewayFailureResponse('Internal exception', $e);
    }
    finally {
      try {
        if ($opayo_transaction == null) {
          // Create the entity capturing the Opayo response details
          $opayo_transaction = $this->processTransactionResponseSuccess($response, $response_array, $configuration, null, $payload, $url);
        }
        else {
          // Save the supplied opayo transaction
          $opayo_transaction->save();
        }
        $opayo_transaction->set('order', $order);
        $opayo_transaction->save();
        $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::processAuthorisedPaymentTransaction: Opayo transaction saved - status {status}, transaction ID: {transactionId}, cart ID: {orderId}', ['transactionId' => $opayo_transaction->getTransactionId(), 'orderId' => $order->id(), 'status' => $opayo_transaction->getStatus()]);

        if ($payment_save_result == -1) {
          // Expected updates on the order and the payment did not happen but the (remote) payment was successful
          // Must have been caused by database issue or a bug in the code above
          try {
            // Log saving of the payment
            $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::processAuthorisedPaymentTransaction: cart {orderId} (uid: {orderUID}), payment {paymentId}, mode: {mode}, problem saving order and/or payment (transaction ID: {transactionId}): {url}', ['mode' => $configuration['mode'], 'paymentId' => $payment->getRemoteId(), 'transactionId' => $transaction_id, 'orderId' => $this->getOrder()->id(), 'orderUID' => $this->getOrder()->uuid(), 'url' => $url]);
          } catch (Exception $le) {
          }

          $this->gatewayFailureResponse('Internal error after successful Opayo Pi transaction request');
        }

        if ($payment_save_result != -1) {
          $opayo_transaction->set('payment', $payment);
          $opayo_transaction->save();
          $this->logVerboseDebug('OpayoPiPaymentGateway::processAuthorisedPaymentTransaction: cart {orderId}, new Opayo transaction entity {transactionId} saved', ['orderId' => $this->getOrder()->id(), 'transactionId' => $transaction_id]);
        }
      }
      catch (Exception $ef) {
        // Nothing's working
        $context = $ef->getMessage() . ', ' . $ef->getTraceAsString();
        $this->log(LogLevel::ERROR, 'OpayoPiPaymentGateway::processAuthorisedPaymentTransaction - error during transaction success response processing, exception: ' . $context . ', ' . print_r($ef, TRUE));
        $this->gatewayFailureResponse('OpayoPiPaymentGateway::processAuthorisedPaymentTransaction: Internal error after successful Opayo Pi transaction request, error: ' . print_r($ef, TRUE));
      }
    }
  }


  protected function paymentDeclinedReponse(string $message) {
    throw new DeclineException('Payment request declined, details: ' . $message);
  }


  protected function gatewayFailureResponse(string $message, Exception $e = null) {
    if ($e == null) {
      $this->logVerboseDebug('OpayoPiPaymentGateway::gatewayFailureResponse, message: ' . $message . ' ' . print_r($e, TRUE));
      throw new PaymentGatewayException('Technical failure during payment request, details: ' . $message);
    }
    else {
      $this->logVerboseDebug('OpayoPiPaymentGateway::gatewayFailureResponse, message: ' . $message);
      throw $e;
    }
  }


  /**
   * Resets the Drupal Commerce checkout flow - normally after a failed payment attempt or other error/issue
   *
   * @param OrderInterface $order
   *    the order
   * @param string $next_step_id
   *    the next step for the checkout process
   */
  protected function resetCheckoutFlow(OrderInterface $order, string $next_step_id = 'order_information') {
    $checkout_flow = $order->get('checkout_flow')->entity;
    $checkout_flow_plugin = $checkout_flow->getPlugin();
    if ($checkout_flow->getPluginId() == 'opayo_checkout_flow') {
      $current_checkout_step = $order->get('checkout_step')->value;
      $order->set('checkout_step', $next_step_id);
      $checkout_flow_plugin->setOrder($order);
      $checkout_flow_plugin->onStepChangeCustom($next_step_id);
      $order->save();
      $this->logVerboseDebug('OpayoPiPaymentGateway::resetCheckoutFlow - cart {orderId} changed current checkout step from {currentStep} to: {nextStep}', ['orderId' => $order->id(), 'currentStep' => $current_checkout_step, 'nextStep' => $next_step_id]);
    }

  }


  /**
   * Process the response from a successful Opayo payment transaction request
   *
   * @param StdClass $response
   *   Response from Opayo as a JSON decoded StdClass
   * @param array $responseArray
   *   Response as associative array
   * @param array $configuration
   *   Gateway configuration settings
   * @param \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction
   *   Provide Opayo transaction info entity for update - if null a new one will be created
   * @param StdClass $requestPayload
   *   Payload of the transaction request
   * @param string $requestURL
   *   URL of the transaction request
   *
   * @return \Drupal\commerce_opayo_pi\Entity\OpayoTransaction
   *   OpayoTransaction entity capturing the response info
   *
   */
  public function processTransactionResponseSuccess(StdClass $response, array $responseArray, $configuration, OpayoTransaction $opayo_transaction = null, StdClass $requestPayload = null, $requestURL = null, $saveEntity = true) {

    // Save the response contents to a new 'OpayoTransaction' entity

    $this->logVerboseDebug('OpayoPiPaymentGateway::processTransactionResponseSuccess, transaction ID: {transactionId}', ['transactionId' => ((property_exists($response, 'transactionId') ? $response->transactionId : ''))]);

    $new_opayo_transaction = $opayo_transaction == null;
    if ($opayo_transaction == null) {
      $opayo_transaction = OpayoTransaction::create();
    }
    else {
      $status_code = $opayo_transaction->getStatusCode();
      $transaction_type = $opayo_transaction->getTransactionType();
      $status = $opayo_transaction->getStatus();
      // Log the status of the Opayo transaction entity before the update
      $this->logVerboseDebug('OpayoPiPaymentGateway::processTransactionResponseSuccess, existing transaction (ID: {transactionId}), transaction type: {transactionType}, status: {status}, status code: {statusCode}', ['transactionId' => $opayo_transaction->getTransactionId(), 'transactionType' => $transaction_type, 'status' => $status, 'statusCode' => $status_code]);
    }

    if (property_exists($response, 'transactionId') && $new_opayo_transaction)
      $opayo_transaction->set('transaction_id', $response->transactionId);
    if (property_exists($response, 'acsTransId'))
      $opayo_transaction->set('acs_trans_id', $response->acsTransId);
    if (property_exists($response, 'dsTransId'))
      $opayo_transaction->set('ds_trans_id', $response->dsTransId);


    if (property_exists($response, 'transactionType'))
      $opayo_transaction->set('transaction_type', $response->transactionType);
    if (property_exists($response, 'status'))
      $opayo_transaction->set('status', $response->status);
    if (property_exists($response, 'statusCode'))
      $opayo_transaction->set('status_code', $response->statusCode);
    if (property_exists($response, 'statusDetail'))
      $opayo_transaction->set('status_detail', $response->statusDetail);


    if (property_exists($response, 'additionalDeclineDetail'))
      $opayo_transaction->set('additional_decline_detail', json_encode($response->additionalDeclineDetail));
    if (property_exists($response, 'retrievalReference'))
      $opayo_transaction->set('retrieval_reference', $response->retrievalReference);
    if (property_exists($response, 'bankResponseCode'))
      $opayo_transaction->set('bank_response_code', $response->bankResponseCode);
    if (property_exists($response, 'avsCvcCheck'))
      $opayo_transaction->set('avs_cvc_check', json_encode($response->avsCvcCheck));
    if (property_exists($response, 'paymentMethod'))
    {
      $payment_method = $response->paymentMethod;
      /** @var stdClass $payment_method */
      if (property_exists($payment_method, 'card')) {
        $paymethod_card = $payment_method->card;
        /** @var stdClass $paymethod_card */
        if (property_exists($paymethod_card, 'cardType'))
          $opayo_transaction->set('card_type', $paymethod_card->cardType);
        if (property_exists($paymethod_card, 'lastFourDigits'))
          $opayo_transaction->set('card_last_four_digits', $paymethod_card->lastFourDigits);
        if (property_exists($paymethod_card, 'expiryDate'))
          $opayo_transaction->set('card_expiry', $paymethod_card->expiryDate);
      }
      $opayo_transaction->set('payment_method', json_encode($payment_method));
    }
    if (property_exists($response, 'currency')) {
      $opayo_transaction->set('currency', $response->currency);
      if (property_exists($response, 'amount'))
      {
        $amount = $response->amount;
        /** @var stdClass $amount */
        if (property_exists($amount, 'totalAmount'))
        {
          $amount_in_pence = strval($amount->totalAmount);
          $amount_in_pounds = Calculator::divide($amount_in_pence, '100', 2);
          $price = new Price($amount_in_pounds, $response->currency);
          $opayo_transaction->set('amount', $price);
        }
      }
    }

    if (array_key_exists('3DSecure', $responseArray))
    {
      $three_d_secure = $responseArray['3DSecure'];
      /** @var array $three_d_secure */
      if (array_key_exists('status', $three_d_secure))
        $opayo_transaction->set('3d_secure_status', $three_d_secure['status']);
    }

    if ($requestPayload !=null && $new_opayo_transaction) {
      // Save the request payload with the response
      $opayo_transaction->set('request_payload', json_encode($requestPayload));
    }
    if ($requestURL !=null && $new_opayo_transaction) {
      // Save the request URL with the response
      $opayo_transaction->set('request_url', $requestURL);
    }
    // Save the 'mode' (test/live) of the transaction
    $opayo_transaction->set('mode', $configuration['mode']);

    if ($new_opayo_transaction) {
      // Time that the response was received
      $time_now = new DateTime();
      $opayo_transaction->set('received', $time_now->getTimestamp());
      }

    // Reference
    $opayo_transaction->set('gateway', $this->parentEntity->id());
    $opayo_transaction->set('plugin', $this->pluginId);


    // Save
    if ($saveEntity) {
      $opayo_transaction->save();
    }

    // Log the status of the Opayo transaction entity after the update
    $this->logVerboseDebug('OpayoPiPaymentGateway::processTransactionResponseSuccess, transaction (ID: {transactionId}), updated transaction type: {transactionType}, status: {status}, status code: {statusCode}',
      ['transactionId' => $opayo_transaction->getTransactionId(), 'transactionType' => $opayo_transaction->getTransactionType(), 'status' => $opayo_transaction->getStatus(), 'statusCode' => $opayo_transaction->getStatusCode()]);

    return $opayo_transaction;
  }



  /**
   * Process an Opayo Pi transaction response that calls for a 'Customer Forward' (in our case 3DS challenge) follow-up
   *
   * @param StdClass $response
   *   Response from Opayo as a JSON decoded StdClass
   * @param StdClass $requestPayload
   *   Payload of the transaction request
   * @param string $requestURL
   *   URL of the transaction request
   * @parram array $configuration
   *   Gateway configuration settings
   *
   * @return \Drupal\commerce_opayo_pi\Entity\OpayoTransaction
   *   Newly created OpayoTransaction entity capturing the response info
   *
   */
  public function createOpayoTransactionFromResponseForwardCustomer(StdClass $response, StdClass $requestPayload, $requestURL, $configuration) {

    $opayo_transaction = OpayoTransaction::create();

    $status_code = 0;
    $status = null;

    // Payload fields of the 3CS
    if (property_exists($response, 'transactionId'))
      $opayo_transaction->set('transaction_id', $response->transactionId);
    if (property_exists($response, 'acsTransId'))
      $opayo_transaction->set('acs_trans_id', $response->acsTransId);
    if (property_exists($response, 'dsTransId'))
      $opayo_transaction->set('ds_trans_id', $response->dsTransId);
    if (property_exists($response, 'status'))
    {
      $status = $response->status;
      $opayo_transaction->set('status', $status);
    }
    if (property_exists($response, 'statusCode'))
    {
      $status_code = $response->statusCode;
      $opayo_transaction->set('status_code', $status_code);
    }
    if (property_exists($response, 'statusDetail'))
    $opayo_transaction->set('status_detail', $response->statusDetail);
    if (property_exists($response, 'acsUrl'))
    $opayo_transaction->set('acs_url', $response->acsUrl);
    if (property_exists($response, 'cReq'))
    $opayo_transaction->set('c_req', $response->cReq);

    // Save the request payload with the response
    $opayo_transaction->set('request_payload', json_encode($requestPayload));
    // Save the request URL with the response
    $opayo_transaction->set('request_url', $requestURL);

    // Time that the response was received
    $time_now = new DateTime();
    $opayo_transaction->set('received', $time_now->getTimestamp());

    // Reference
    $opayo_transaction->set('gateway', $this->parentEntity->id());
    $opayo_transaction->set('plugin', $this->pluginId);
    $opayo_transaction->set('mode', $configuration['mode']);

    if ($status == '3DAuth' || $status_code == 2021)
    {
      // 3D secure redirect - save the interim transaction info
      $opayo_transaction->save();
    }

    return $opayo_transaction;
  }


    /**
   * {@inheritdoc}
   */
  public function process3DSAuthenticationResult(OrderInterface &$order, OpayoTransaction $opayoTransaction, string $cRes) {

    $success = false;
    $configuration = $this->getConfiguration();
    $opayo_transaction_id = $opayoTransaction->getTransactionId();
    $this->setOrder($order);  // Called from our controller, explicitly set the order

    $curl = curl_init();

    // Challenge response goes into the POST body
    $post_fields = new StdClass();
    $post_fields->cRes = $cRes;
    $payload_json = json_encode($post_fields);
    $url = $configuration['uri']['transactions'] . $opayo_transaction_id . '/3d-secure-challenge'; // Transaction ID goes into the request URL;

    $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: cart {orderId} (uid: {orderUID}), mode: {mode}, 3DS challenge processing, transaction id: {transactionId}, URL: {url}, transaction payload: {payload}',
      ['mode' => $configuration['mode'], 'orderId' => $order->id(), 'orderUID' => $order->uuid(), 'payload' => $payload_json, 'transactionId' => $opayo_transaction_id, 'url' => $url]);

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $payload_json,
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic " . $configuration['auth'],
        "Cache-Control: no-cache",
        "Content-Type: application/json"
      ),
    ));

    //Tell cURL that it should only spend 20 seconds
    //trying to connect to the URL in question.
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
    // Set a timeout for the requests
    $timeout = intval($configuration['transaction_timeout']);
    curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);

    try {
      // Execute the request (presumably synchronously)
      $this->log(LogLevel::DEBUG, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: cart {orderId}, transaction ID: {transactionId}, payment transaction request using {url}', ['orderId' => $order->id(), 'url' => $url, 'transactionId' => $opayo_transaction_id]);

      $raw_response = curl_exec($curl);
      $http_code = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
      $response = null;
      $response_array = null;

      // Check the response
      $err = curl_error($curl);
      if (!empty($err)) {
        $this->log(LogLevel::WARNING,
          'OpayoPiPaymentGateway::process3DSAuthenticationResult: cart {orderId} (3DS challenge submit error, transaction id: {transactionId}, URL: {url}, error: {error}',
          ['orderId' => $order->id(), 'transactionId' => $opayo_transaction_id, 'url' =>$url, 'error' => print_r($err, TRUE)]
        );

        throw new PaymentGatewayException('Transport error during Opayo Pi 3DS response submission, error: ' . print_r($err, TRUE));
      }
      else {
        $response = json_decode($raw_response);
        $response_array = json_decode($raw_response, TRUE);
      }

      $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: cart {orderId}, 3D Challenge result submission response received: {url}, transaction id: {transactionId}, response payload: {payload}', ['payload' => $raw_response, 'orderId' => $order->id(), 'url' => $url, 'transactionId' => $opayo_transaction_id]);
    }
    catch (Exception $e) {
      $this->log(LogLevel::ERROR, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: cart {orderId}, transaction ID: {transactionId}, URL: {url}, exception during payment transaction request: {exception}', ['exception' => ($e->getMessage() . ' ' . $e->getTraceAsString()), 'transactionId' => $opayo_transaction_id, 'orderId' => $this->getOrder()->id(), 'url' => $url]);
    }
    finally {
      curl_close($curl);
    }

    if ($raw_response != null && $response != null) {
      // Successfully received a response to the request

      if (($http_code != 201) || property_exists($response, 'errors')) {
        // Response from Opayo but errors detected at their end
        try {
          $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: payment transaction failed, cart {orderId}, transaction response http code {httpcode}',
            ['orderId' => $order->id(), 'httpcode' => $http_code]);
        } catch (Exception $le) {}

        $decline_message = '';
        $technical_message = '';

        $opayo_errors = array();

        if (property_exists($response, 'errors')) {
          // Opayo response returned one or more error
          $opayo_errors = array_merge($opayo_errors, $response->errors);
        }
        else {
          // Assume single error
          $opayo_errors[] = $response;
        }
        /** @var StdClass $opayo_error */
        foreach($opayo_errors as $opayo_error)
        {
          try {
            $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: cart {orderId} (uid: {orderUID}), mode: {mode}, transaction response error {errorCode} for: {url}, property {property}, error description: {errorDescr}',
              ['mode' => $configuration['mode'], 'orderId' => $order->id(), 'orderUID' => $order->uuid(), 'url' => $url, 'errorCode' => $opayo_error->code, 'property' => $opayo_error->property, 'errorDescr' => $opayo_error->description]);
          }
          catch (Exception $e){
            $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: cart {orderId} (uid: {orderUID}), mode: {mode}, transaction response has error for: {url}, error: {error}',
              ['mode' => $configuration['mode'], 'orderId' => $order->id(), 'orderUID' => $order->uuid(), 'url' => $url, 'error' => print_r($opayo_error, TRUE)]);
          }
          if (FALSE) {
            // Not yet sure which error messages indicate a 'decline' instead of a technical issue
            $decline_message = $decline_message . (strlen($decline_message) == 0 ? '' : ', ') . $opayo_error->description;
          }
          else {
            $technical_message = $technical_message . (strlen($technical_message) == 0 ? '' : ', ') . $opayo_error->description;
          }
          // Save error information with the entity
          if ($opayo_error->code != '0000' && $opayo_error->code != 0)
            $opayoTransaction->set('status_code', $opayo_error->code);
          $opayoTransaction->set('status_detail', $opayo_error->description);
          $opayoTransaction->save();
        }

        $this->orderResetPaymentMethod($this->getOrder());

        // Revert checkout flow back to 'order_information' stage
        $this->resetCheckoutFlow($order);

        if ($decline_message != "")
        {
          $this->storePaymentErrorMessage($this->getOrder()->id(), $decline_message, $opayoTransaction);
          throw new DeclineException('Errors in Opayo Pi transaction response, HTTP code: ' . $http_code . ', ' . $decline_message);
        }
        else {
          $this->storePaymentErrorMessage($this->getOrder()->id(), $technical_message, $opayoTransaction);
          throw new PaymentGatewayException('Errors in Opayo Pi transaction response, HTTP code: ' . $http_code . ', ' . $technical_message);
        }

      }
      elseif ($http_code == 201 && property_exists($response, 'statusCode') && $response->statusCode != '0000') {
        // declined/rejected
        $status_detail = $response->statusDetail;
        $status = $response->status;
        $transaction_id = $response->transactionId;

        $this->log(LogLevel::WARNING,
          'OpayoPiPaymentGateway::process3DSAuthenticationResult: cart {orderId}, payment transaction (ID: {transactionId}) failed with status: {status} ({statusCode} - {statusDetail})',
          ['orderId' => $order->id(), 'transactionId' => $transaction_id, 'statusCode' => $response->statusCode, 'status' => $status, 'statusDetail' => $status_detail]
        );

        // Save error information with the entity
        if ($response->statusCode != '0000' && $response->statusCode != 0)
          $opayoTransaction->set('status_code', $response->statusCode);
        $opayoTransaction->set('status_detail', $status_detail);
        if ($status != 'Ok')
          $opayoTransaction->set('status', $status);
        $opayoTransaction->save();

        $this->orderResetPaymentMethod($this->getOrder());

        // Revert checkout flow back to 'order_information' stage
        $this->resetCheckoutFlow($order);

        if ($response->statusCode == '2000' || $response->statusCode == '4026') {
          // Declined for logical reasons
          $this->storePaymentErrorMessage($this->getOrder()->id(), $status_detail, $opayoTransaction);
          throw new DeclineException('Payment request declined, details: ' . $status_detail);
        }
        else {
          // Failure is technical
          $this->storePaymentErrorMessage($this->getOrder()->id(), $status_detail, $opayoTransaction);
          throw new PaymentGatewayException('Payment request declined/rejected, details: ' . $status_detail);
        }
      }
      elseif ($http_code == 201 && property_exists($response, 'statusCode') && $response->statusCode == '0000') {
        try {
          // Log successfull transaction response
          $this->log(LogLevel::INFO,
            'OpayoPiPaymentGateway::process3DSAuthenticationResult: payment transaction success response after 3DS challenge for cart {orderId}, (HTTP code: 201, status code: 0, transaction ID: {transactionId})',
            ['transactionId' => $opayo_transaction_id, 'orderId' => $order->id()]);
        } catch (Exception $le) {}

        try {
          // Update the entity capturing the Opayo response details
          $opayo_transaction = $this->processTransactionResponseSuccess($response, $response_array, $configuration, $opayoTransaction, null, $url);

          // Save the payment and the commerce_order

          // Create a payment record associated with the order being paid in full.
          $payment_method = $order->get('payment_method')->getValue();
          $payment = Payment::create([
            'type' => 'payment_default',
            'payment_gateway' => $this->getPaymentGateway()->id(),
            'payment_gateway_mode' => $configuration['mode'],
            'payment_method' => $payment_method[0]['target_id'],
            'order_id' => $order->id(),
            'amount' => $order->getTotalPrice(),
            'state' => 'completed',
            'remote_id' => $opayo_transaction_id,
            'remote_state' => $opayo_transaction->getStatus(),
            'completed' => (new DateTime())->getTimestamp(),
          ]);
          $avs_cvs = $opayo_transaction->get('avs_cvc_check')->value;
          /** @var stdClass $avs_cvs_obj */
          $avs_cvs_obj = json_encode($avs_cvs);
          if (property_exists($avs_cvs_obj, 'securityCode')) {
            $payment->setAvsResponseCode($avs_cvs_obj->securityCode);
          }
          $payment->save();

          try {
            // Log saving of the payment
            $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: order {orderId}, payment {paymentId} created (transaction ID: {transactionId})', ['paymentId' => $payment->getRemoteId(), 'transactionId' => $opayo_transaction_id, 'orderId' => $order->id()]);
          } catch (Exception $le) {
          }

          // Link the payment to the Opayo transaction
          $opayo_transaction->set('payment', $payment);
          $opayo_transaction->save();
          $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: Opayo transaction saved - status {status}, transaction ID: {transactionId}, cart ID: {orderId}', ['transactionId' => $opayo_transaction_id, 'orderId' => $order->id(), 'status' => $opayo_transaction->getStatus()]);
        }
        catch (Exception $e) {
          // Log exception details
          $this->log(LogLevel::ERROR, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: order {orderId} (uid: {orderUID}), mode: {mode}, Opayo transaction ID: {transactionId}, URL: {url}, internal exception: {exception}',
            ['exception' => ($e->getMessage() . ' ' . $e->getTraceAsString()), 'mode' => $configuration['mode'], 'transactionId' => $opayo_transaction_id, 'orderId' => $order->id(), 'orderUID' => $order->uuid(), 'url' => $configuration['uri']['transactions']]);
          // Rethrow
          throw $e;
        }

        $success = true;
      }
    }
    else {
      // No response from the request - result of the payment request is unclear

      // Try for approx 20 seconds to find out the request's status
      $retry = 20;
      $opayo_transaction_info = null;
      while ($opayo_transaction_info == null && $retry > 0) {

        $opayo_transaction_info = $this->getOpayoPiTransaction($opayo_transaction_id, $response, $opayoTransaction);

        if ($opayo_transaction_info != null) {
          $this->logVerboseDebug('OpayoPiPaymentGateway::submitNewOpayoPiTransaction: retrieved Opayo transaction info for ID ' . $opayo_transaction_id . ": status: " . $opayo_transaction_info->getStatus() . 'code: ' . $opayo_transaction_info->getStatusCode() . ' for order ' . $order->id());

          if ($opayo_transaction_info->getStatusCode() == "0000") {
            // Payment succesfully authorised
            $retry = -1;
          }
          else if ($opayo_transaction_info->getStatus() != "3DAuth") {
            // Payment no longer waiting for 3DAuth - but not successful - assume failure
            $retry = -1;
          }
        }
        else {
          $this->logVerboseDebug('OpayoPiPaymentGateway::process3DSAuthenticationResult: unexpected failure to retrieve Opayo transaction for ID ' . $opayo_transaction_id . ' for order ' . $order->id());
        }
        if ($opayo_transaction_info == null && $retry > 0) {
          // Sleep for 5 seconds and try again
          sleep(5);
          $retry = $retry - 5;
        }
      }

      if ($opayo_transaction_info != null) {
        try {
          // Updated fields have already been copied to '$opayoTransaction' during 'getOpayoPiTransaction'
          $opayoTransaction->save();

          if ($opayo_transaction_info->getStatusCode() == "0000") {
            // Success

            // Save the payment and the commerce_order
            $payment_method = $order->get('payment_method')->getValue();
            $payment = Payment::create([
              'type' => 'payment_default',
              'payment_gateway' => $this->getPaymentGateway()->id(),
              'payment_gateway_mode' => $configuration['mode'],
              'payment_method' => $payment_method[0]['target_id'],
              'order_id' => $order->id(),
              'amount' => $order->getTotalPrice(),
              'state' => 'completed',
              'remote_id' => $opayo_transaction_id,
              'remote_state' => $opayoTransaction->getStatus(),
              'completed' => (new DateTime())->getTimestamp(),
            ]);
            $avs_cvs = $opayoTransaction->get('avs_cvc_check')->value;
            /** @var stdClass $avs_cvs_obj */
            $avs_cvs_obj = json_encode($avs_cvs);
            if (property_exists($avs_cvs_obj, 'securityCode')) {
              $payment->setAvsResponseCode($avs_cvs_obj->securityCode);
            }
            $payment->save();
            try {
              $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: order {orderId}, payment {paymentId} created (transaction ID: {transactionId})', ['paymentId' => $payment->getRemoteId(), 'transactionId' => $opayo_transaction_id, 'orderId' => $order->id()]);
            } catch (Exception $le) {
            }
            // Link the payment to the Opayo transaction
            $opayoTransaction->set('payment', $payment);
            $opayoTransaction->save();
            $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: Opayo transaction saved - status {status}, transaction ID: {transactionId}, cart ID: {orderId}', ['transactionId' => $opayo_transaction_id, 'orderId' => $order->id(), 'status' => $opayoTransaction->getStatus()]);

            $success = true;
          }
          else if ($opayo_transaction_info->getStatus() != "3DAuth") {
            $this->orderResetPaymentMethod($this->getOrder());
            // Revert checkout flow back to 'order_information' stage
            $this->resetCheckoutFlow($order);
            $status_detail = $opayo_transaction_info->get('status_detail')->getValue();
            $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: payment authentication failed for transaction ID: {transactionId}, cart ID: {orderId}, status detail: {statusDetail}', ['transactionId' => $opayo_transaction_id, 'orderId' => $order->id(), 'statusDetail' => $status_detail]);
            if ($opayo_transaction_info->getStatusCode() == '2000' || $opayo_transaction_info->getStatusCode() == '4026') {
              // Declined for logical reasons
              $this->storePaymentErrorMessage($this->getOrder()->id(), $status_detail, $opayoTransaction);
              throw new DeclineException('Payment request declined, details: ' . $status_detail);
            }
            else {
              // Failure is technical
              $this->storePaymentErrorMessage($this->getOrder()->id(), $status_detail, $opayoTransaction);
              throw new PaymentGatewayException('Payment request declined/rejected, details: ' . $status_detail);
            }
          }
          else {
            // After 20 seconds of polling the status is still '3DAuth' - must assume that the authorisation was unsuccessful
            $this->orderResetPaymentMethod($this->getOrder());
            // Revert checkout flow back to 'order_information' stage
            $this->resetCheckoutFlow($order);
            $this->storePaymentErrorMessage($this->getOrder()->id(), 'Assume payment authentication failed', $opayoTransaction);
            $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: no status change away from 3DAuth for transaction ID: {transactionId}, cart ID: {orderId}, assume authentication failure', ['transactionId' => $opayo_transaction_id, 'orderId' => $order->id()]);
            throw new PaymentGatewayException('Payment request declined/rejected, assume payment authentication failed');
          }
        }
        catch (Exception $e) {
          // Log exception details
          $this->log(LogLevel::ERROR, 'OpayoPiPaymentGateway::submitNewOpayoPiTransaction: order {orderId} (uid: {orderUID}), mode: {mode}, Opayo transaction ID: {transactionId}, URL: {url}, internal exception: {exception}',
            ['exception' => ($e->getMessage() . ' ' . $e->getTraceAsString()), 'mode' => $configuration['mode'], 'transactionId' => $opayo_transaction_id, 'orderId' => $order->id(), 'orderUID' => $order->uuid(), 'url' => $configuration['uri']['transactions']]);
          // Rethrow
          throw $e;
        }
      }
      else {
        // Unable to reach Opayo (after attempting for 20 seconds) to establish payment authentication status - assume failure
        $this->orderResetPaymentMethod($this->getOrder());
        // Revert checkout flow back to 'order_information' stage
        $this->resetCheckoutFlow($order);
        $this->storePaymentErrorMessage($this->getOrder()->id(), 'Assume payment authentication failed', $opayoTransaction);
        $this->log(LogLevel::ERROR, 'OpayoPiPaymentGateway::process3DSAuthenticationResult: cannot retrieve info for transaction ID: {transactionId}, cart ID: {orderId}', ['transactionId' => $opayo_transaction_id, 'orderId' => $order->id()]);
        throw new PaymentGatewayException('Payment request declined/rejected, assume payment authentication failed');
      }
    }

    $this->logVerboseDebug('OpayoPiPaymentGateway::process3DSAuthenticationResult - result: {result}, cart {orderId}, mode: {mode}, transaction id: {transactionId}', ['mode' => $configuration['mode'], 'orderId' => $order->id(), 'transactionId' => $opayo_transaction_id, 'result' => ($success ? 'success' : 'failed')]);
    $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::process3DSAuthenticationResult - result: {result}, cart {orderId}, mode: {mode}, transaction id: {transactionId}', ['mode' => $configuration['mode'], 'orderId' => $order->id(), 'transactionId' => $opayo_transaction_id, 'result' => ($success ? 'success' : 'failed')]);

    return $success;
  }


  /**
   * {@inheritdoc}
   */
  public function getOpayoPiTransaction(string $opayoTransactionId, ?stdClass &$response = null, ?OpayoTransaction &$opayoTransaction = null) {

    $configuration = $this->getConfiguration();
    $url = $configuration['uri']['transactions'] . $opayoTransactionId;
    $curl = curl_init();
    $raw_response = '';
    $http_code = -1;

    $this->logVerboseDebug('OpayoPiPaymentGateway::getOpayoPiTransaction called for transaction ID ' . $opayoTransactionId . ' using URL: ' . $url);

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic " . $configuration['auth'],
        "Cache-Control: no-cache",
        "Content-Type: application/json"
      ),
    ));

    //Tell cURL that it should only spend 20 seconds
    //trying to connect to the URL in question.
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
    $timeout = intval($configuration['transaction_timeout']);
    curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);

    try {
      // Execute the request
      $this->log(LogLevel::DEBUG, 'OpayoPiPaymentGateway::getOpayoPiTransaction: retrieve transaction request using {url}', ['url' => $url]);
      $raw_response = curl_exec($curl);
      $http_code = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
      $response = null;
      $response_array = null;

      // Check the response
      $err = curl_error($curl);
      if (!empty($err)) {
        $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::getOpayoPiTransaction: transaction request error for: {url}, error: {error}', ['url' => $url, 'error' => print_r($err, TRUE)]);
        $this->gatewayFailureResponse('Transport error during Opayo Pi \'retrieve a transaction\' request, error: ' . print_r($err, TRUE));
      }
      else {
        $response = json_decode($raw_response);
        $response_array = json_decode($raw_response, TRUE);
      }
    }
    catch (Exception $e) {
      $this->log(LogLevel::ERROR, 'OpayoPiPaymentGateway::getOpayoPiTransaction: URL: {url}, exception during transaction request: {exception}', ['exception' => ($e->getMessage() . ' ' . $e->getTraceAsString()), 'url' => $url]);
      throw new PaymentGatewayException('Exception during transaction retrieve request');
    }
    finally {
      curl_close($curl);
    }

    $this->log(LogLevel::DEBUG, 'OpayoPiPaymentGateway::getOpayoPiTransaction: retrieve transaction request completed ({url}), HTTP code: {httpCode}', ['url' => $url, 'httpCode' => $http_code]);

    try {
      $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::getOpayoPiTransaction: response payload: {payload}', ['payload' => $raw_response]);
    } catch (Exception $le) {
    }

    if ($http_code == 200 && property_exists($response, 'statusCode')) {
      // Successful retrieval
      // Create an opayo entity from the response but don't save it
      $opayo_transaction = $this->processTransactionResponseSuccess($response, $response_array, $configuration, $opayoTransaction, null, $url, false);
      // Return it
      return $opayo_transaction;
    }
    else {
      return null;
    }
  }


  /**
   * {@inheritdoc}
   */
  public function getOpayoPiTransactionStatusByVendorTxCode(string $vendorTxCode, string &$opayo_transaction_id) {

    $configuration = $this->getConfiguration();
    $url = $configuration['uri']['access'];
    $account_user = $configuration['account_user'];
    $account_password = $configuration['account_password'];

    $this->logVerboseDebug('OpayoPiPaymentGateway::getOpayoPiTransactionByVendorTxCode called for Vendor Tx Code ' . $vendorTxCode . ' using URL: ' . $url);

    if (isset($account_user) && isset($account_password) && isset($url)) {
      try {
        $request_contents = $this->requestXML($configuration['vendor'], $account_user, $account_password, 'getTransactionDetail', '<vendortxcode>' . $vendorTxCode . '</vendortxcode>');

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => http_build_query(["XML" => $request_contents]),
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded"
          ),
        ));

        $this->logger->debug('OpayoPiPaymentGateway::getOpayoPiTransactionByVendorTxCode: about to call \'getTransactionDetail\' for Vendor Tx Code: {vendorTxCode}', ['vendorTxCode' => $vendorTxCode]);
        $raw_response = curl_exec($curl);
        $http_code = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
        $err = curl_error($curl);

        if (!empty($err)) {
          $this->logger->warning('OpayoPiPaymentGateway::getOpayoPiTransactionByVendorTxCode: Unsuccessful getTransactionDetail Vendor Tx Code: {vendorTxCode}, error: {error} ', ['vendorTxCode' => $vendorTxCode, 'error' => $err]);
        }
        else {
          $this->logger->debug('OpayoPiPaymentGateway::getOpayoPiTransactionByVendorTxCode: Successful getTransactionDetail Vendor Tx Code: {vendorTxCode}', ['vendorTxCode' => $vendorTxCode]);
        }

        $contents = json_decode(json_encode(simplexml_load_string($raw_response)), true);

        curl_close($curl);

        if (array_key_exists('errorcode', $contents)) {
          $error_code = $contents['errorcode'];

          if ($error_code == '0000') {
            // success
            $txstateid = $contents['txstateid'];
            $opayo_transaction_id = $contents['vpstxid'];
            // return the status code for the transaction
            return $txstateid;
          }
          else {
            $error = '';
            if (array_key_exists('error', $contents))
              $error = $contents['error'];
            $this->logger->warning('OpayoPiPaymentGateway::getOpayoPiTransactionByVendorTxCode: getTransactionDetail returns error for Vendor Tx Code: {vendorTxCode}, code: {errorCode}, error {error}', ['vendorTxCode' => $vendorTxCode, 'errorCode' => $error_code, 'error' => $error]);
          }
        }
        else {
          $this->logger->warning('OpayoPiPaymentGateway::getOpayoPiTransactionByVendorTxCode: getTransactionDetail unexpected response for Vendor Tx Code: {vendorTxCode}, payload: {payload} ', ['vendorTxCode' => $vendorTxCode, 'payload' => $raw_response]);
        }
      }
      catch (Exception $e) {
        $this->logger->error('OpayoPiPaymentGateway::getOpayoPiTransactionByVendorTxCode: exception {exception}', ['exception' => ($e->getMessage() . ' ' . $e->getTraceAsString())]);
      }
    }
    return null;
  }


  /**
   * Creates an XML request message
   *
   * @param array $vendor
   *   Opayo 'vendor'
   * @param string $user
   *   Opayo account username
   * @param string $password
   *   Opayo account password
   * @param string $command
   *   Command to submit - see API
   * @param string $other
   *   Additional XML fields to support the command
   *
   * @return string
   *   request message in XML format
   */
  protected function requestXML($vendor, $user, $password, $command, $other = null) {
    $xml_string_contents = '<command>' . $command . '</command>' .
    '<vendor>' . $vendor . '</vendor>' .
    '<user>' . $user . '</user>' .
    (isset($other) ? $other : '');

    // MD5 Hash the contents with the password included
    $xml_string_contents_hash = md5($xml_string_contents . '<password>' . $password . '</password>');

    // Return contents and the MD5 hash signature
    return '<vspaccess>' . $xml_string_contents . '<signature>' . $xml_string_contents_hash . '</signature></vspaccess>';
  }



  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {

    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction */
    $opayo_transaction = null;

    // Perform the refund request here, error handling and exceptions are inside 'submitOpayoPiRefundTransaction'
    $decimal_amount = $amount->getNumber();
    $opayo_transaction = $this->submitOpayoPiRefundTransaction($payment, $decimal_amount);

    // Determine whether payment has been fully or partially refunded.
    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }


    /**
   * Submit the refund transaction request to Opayo
   *
   * @param PaymentInterface $payment
   *   The original/reference payment against which to refund
   * @param string $amount
   *   The amount to refund
   *
   * @return \Drupal\commerce_opayo_pi\Entity\OpayoTransaction:null
   *  The newly created Opayo Transaction entity
   */
  public function submitOpayoPiRefundTransaction(PaymentInterface &$payment, string $amount) {

    $configuration = $this->getConfiguration();
    $url = $configuration['uri']['transactions'];
    $curl = curl_init();

    // Find the Opayo transaction for the payment
    /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $ref_opayo_transaction */
    $ref_opayo_transaction = null;
    try {
      $query = $this->entityTypeManager->getStorage('opayo_transaction')->getQuery();
      $query->accessCheck(FALSE);
      $query->condition('payment', $payment->id());
      $query->condition('status', 'Ok');
      $ids = $query->execute();
      if (count($ids) == 1) {
        /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $ref_opayo_transaction */
        $ref_opayo_transaction = $this->entityTypeManager->getStorage('opayo_transaction')->load(array_values($ids)[0]);
      }
    } catch (Exception $e) {
      $this->log(LogLevel::WARNING,
        'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: against payment {paymentId}, mode: {mode}, internal exception: {exception}',
        ['exception' => ($e->getMessage() . ' ' . $e->getTraceAsString()), 'mode' => $configuration['mode'], 'paymentId' => $payment->id()]
      );
      // Rethrow
      throw $e;
    }

    if ($ref_opayo_transaction == null)
    {
      $this->log(LogLevel::WARNING,
        'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: against payment {paymentId}, mode: {mode}, opayo reference transaction not found',
        ['mode' => $configuration['mode'], 'paymentId' => $payment->id()]
      );
      throw new PaymentGatewayException("Reference Opayo transaction not found");
    }

    // Construct the contents of the body of the transaction request post, ready for conversion into JSON format
    $payload = $this->refundTransactionRequestContents($ref_opayo_transaction, $amount);
    $payload_json = json_encode($payload);

    $this->log(LogLevel::DEBUG, 'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: reference payment / Opayo transaction ID: {paymentId}/{opayoTransactionId}, mode: {mode}, create transaction payload: {payload}',
      ['mode' => $configuration['mode'], 'paymentId' => $payment->id(), 'opayoTransactionId' => $ref_opayo_transaction->getTransactionId(), 'payload' => $payload_json]);

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $payload_json,
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic " . $configuration['auth'],
        "Cache-Control: no-cache",
        "Content-Type: application/json"
      ),
    ));

    //Tell cURL that it should only spend 20 seconds
    //trying to connect to the URL in question.
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
    // Set a timeout for the requests, TODO currently set to 3 minutes - may need to be configurable
    $timeout = intval($configuration['transaction_timeout']);
    curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);

    // Execute the request
    $this->log(LogLevel::DEBUG,
      'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: reference payment / Opayo transaction ID: {paymentId}/{opayoTransactionId}, mode: {mode}, transaction request: {url}',
      ['mode' => $configuration['mode'], 'paymentId' => $payment->id(), 'opayoTransactionId' => $ref_opayo_transaction->getTransactionId(), 'url' => $url]);
    $raw_response = curl_exec($curl);
    $http_code = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
    $response = null;
    $response_array = null;

    // Check the response
    $err = curl_error($curl);
    if (!empty($err)) {
      $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: reference payment / Opayo transaction ID: {paymentId}/{opayoTransactionId}, mode: {mode}, transaction request error for: {url}, error: {error}',
        ['mode' => $configuration['mode'], 'paymentId' => $payment->id(), 'opayoTransactionId' => $ref_opayo_transaction->getTransactionId(), 'url' => $url, 'error' => print_r($err, TRUE)]);
      throw new PaymentGatewayException('Transport error during Opayo Pi transaction request, error: ' . print_r($err, TRUE));
    }
    else {
      $response = json_decode($raw_response);
      $response_array = json_decode($raw_response, TRUE);
    }
    curl_close($curl);

    try {
      $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: reference payment / Opayo transaction ID: {paymentId}/{opayoTransactionId}, mode: {mode}, transaction request response received for: {url}, response payload: {payload}',
        ['mode' => $configuration['mode'], 'paymentId' => $payment->id(), 'opayoTransactionId' => $ref_opayo_transaction->getTransactionId(), 'url' => $url, 'payload' => $raw_response]);
    } catch (Exception $le) {}

    if (($http_code != 201 && $http_code != 202) || property_exists($response, 'errors'))
    {
      // Response from Opayo but errors detected at their end
      try {
        $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: reference payment / Opayo transaction ID: {paymentId}/{opayoTransactionId}, mode: {mode}, transaction response http code {httpcode} for: {url}',
          ['mode' => $configuration['mode'], 'paymentId' => $payment->id(), 'opayoTransactionId' => $ref_opayo_transaction->getTransactionId(), 'url' => $url, 'httpcode' => $http_code]);
      } catch (Exception $le) {}

      $decline_message = '';
      $technical_message = '';

      $opayo_errors = array();

      if (property_exists($response, 'errors')) {
        // Opayo response returned one or more error
        $opayo_errors = array_merge($opayo_errors, $response->errors);
      }
      else {
        // Assume single error
        $opayo_errors[] = $response;
      }
      /** @var StdClass $opayo_error */
      foreach($opayo_errors as $opayo_error)
      {
        try {
          $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: reference payment / Opayo transaction ID: {paymentId}/{opayoTransactionId}, mode: {mode}, transaction response error {errorCode} for: {url}, property {property}, error description: {errorDescr}',
            ['mode' => $configuration['mode'], 'paymentId' => $payment->id(), 'opayoTransactionId' => $ref_opayo_transaction->getTransactionId(), 'url' =>$url, 'errorCode' => $opayo_error->code, 'property' => $opayo_error->property, 'errorDescr', $opayo_error->description]);
        }
        catch (Exception $e){
          $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: reference payment / Opayo transaction ID: {paymentId}/{opayoTransactionId}, mode: {mode}, transaction response error for: {url}, error: {error}',
            ['mode' => $configuration['mode'], 'paymentId' => $payment->id(), 'opayoTransactionId' => $ref_opayo_transaction->getTransactionId(), 'url' => $url, 'error' => print_r($err, TRUE)]);
        }
        if ($opayo_error->code == 1011) {
          $decline_message = $decline_message . (strlen($decline_message) == 0 ? '' : ', ') . $opayo_error->description;
        }
        else {
          $technical_message = $technical_message . (strlen($technical_message) == 0 ? '' : ', ') . $opayo_error->description;
        }
      }
      if ($decline_message != "")
      {
        throw new DeclineException('Errors in Opayo Pi transaction response, HTTP code: ' . $http_code . ', ' . $decline_message);
      }
      else {
        throw new PaymentGatewayException('Errors in Opayo Pi transaction response, HTTP code: ' . $http_code . ', ' . $technical_message);
      }
    }
    elseif ($http_code == 201 && property_exists($response, 'statusCode') && $response->statusCode != 0)
    {
      // declined/rejected
      $status_detail = $response->statusDetail;
      $status = $response->status;
      $transaction_id = $response->transactionId;

      $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: reference payment / Opayo transaction ID: {paymentId}/{opayoTransactionId}, mode: {mode}, transaction (ID: {newTransactionId}) failed with status: {status} ({statusCode} - {statusDetail}) for: {url}',
        ['mode' => $configuration['mode'], 'paymentId' => $payment->id(), 'opayoTransactionId' => $ref_opayo_transaction->getTransactionId(), 'url' =>$url, 'newTransactionId' => $transaction_id, 'statusCode' => $response->statusCode, 'status' => $status, 'statusDetail' => $status_detail]);

      if ($response->statusCode == 4035) {
        // Declined for logical reasons
        throw new DeclineException('Refund request declined, details: ' . $status_detail);
      }
      else {
        // Failure is technical
        throw new PaymentGatewayException('Refund request declined/rejected, details: ' . $status_detail);
      }
    }
    elseif ($http_code == 201 && property_exists($response, 'statusCode') && $response->statusCode == 0)
    {
      // Successful refund
      $transaction_id = '';
      if (property_exists($response, 'transactionId'))
        $transaction_id = $response->transactionId;

      try {
        // Log successfull transaction response
        $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: reference payment / Opayo transaction ID: {paymentId}/{opayoTransactionId}, mode: {mode}, transaction success response (HTTP code: 201, status code: 0, transaction ID: {newTransactionId}): {url}',
          ['mode' => $configuration['mode'], 'paymentId' => $payment->id(), 'opayoTransactionId' => $ref_opayo_transaction->getTransactionId(), 'url' =>$url, 'newTransactionId' => $transaction_id]);
      } catch (Exception $le) {}

      $payment_state = $payment->get('state')->value;
      $payment_save_result = -1;

      try {
        // Save the commerce (refund) 'payment'

        // Update the state of the payment and save
        $next_state = 'completed';
        $payment->setState($next_state);
        $payment->setRemoteId($transaction_id); // Set the remote ID to Opayo's transaction ID
        $payment_save_result = $payment->save();
        try {
          // Log saving of the 'payment'
          $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: reference payment / Opayo transaction ID: {paymentId}/{opayoTransactionId}, mode: {mode}, (refund)payment saved (transaction ID: {newTransactionId}): {url}',
          ['mode' => $configuration['mode'], 'paymentId' => $payment->id(), 'opayoTransactionId' => $ref_opayo_transaction->getTransactionId(), 'url' =>$url, 'newTransactionId' => $transaction_id]);
        } catch (Exception $le) {}
      }
      catch (Exception $e) {
        // Log exception details
        $this->log(LogLevel::ERROR, 'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: reference payment / Opayo transaction ID: {paymentId}/{opayoTransactionId}, mode: {mode}, Opayo transaction ID: {newTransactionId}, URL: {url}, internal exception: {exception}',
          ['exception' => ($e->getMessage() . ' ' . $e->getTraceAsString()), 'mode' => $configuration['mode'], 'paymentId' => $payment->id(), 'opayoTransactionId' => $ref_opayo_transaction->getTransactionId(), 'url' => $url, 'newTransactionId' => $transaction_id]);
        // Rethrow
        throw $e;
      }
      finally {
        try {
          // Create the entity capturing the Opayo response details
          $new_opayo_transaction = $this->processTransactionResponseSuccess($response, $response_array, $configuration, null, $payload, $url);
          $new_opayo_transaction->set('ref_transaction_id', $ref_opayo_transaction->getTransactionId());  // Store the transaction ID of the original payment transaction with the refund
          $new_opayo_transaction->set('order', $ref_opayo_transaction->getOrder()); // Link the refund to the original order
          $new_opayo_transaction->save();
          $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: Opayo transaction saved - status {status}, transaction ID: {transactionId}', ['transactionId' => $new_opayo_transaction->getTransactionId(), 'status' => $new_opayo_transaction->getStatus()]);

          if ($payment_save_result == -1)
          {
            // Expected updates on the order and the payment did not happen but the (remote) refund was successful
            // Must have been caused by database issue or a bug in the code above
            try {
              // Log saving of the payment
              $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: reference payment / Opayo transaction ID: {paymentId}/{opayoTransactionId}, mode: {mode}, problem saving order and/or payment (refund Opayo transaction ID: {newTransactionId}): {url}',
                ['mode' => $configuration['mode'], 'paymentId' => $payment->id(), 'opayoTransactionId' => $ref_opayo_transaction->getTransactionId(), 'url' => $url, 'newTransactionId' => $transaction_id]);
            } catch (Exception $le) {}
            throw new PaymentGatewayException('Internal error after successful Opayo Pi transaction request');
          }

          if ($payment_save_result != -1) {
            $new_opayo_transaction->set('payment', $payment);
            $new_opayo_transaction->save();
          }
        }
        catch (Exception $ef) {
          // Nothing's working
          throw new PaymentGatewayException('Internal error after successful Opayo Pi transaction request, error: ' . print_r($ef, TRUE));
        }
      }
    }
    else {
      $this->log(LogLevel::WARNING,
        'OpayoPiPaymentGateway::submitOpayoPiRefundTransaction: reference payment / Opayo transaction ID: {paymentId}/{opayoTransactionId}, mode: {mode}, unexpected result for: {url}, HTTP code {httpCode}',
        ['mode' => $configuration['mode'], 'paymentId' => $payment->id(), 'opayoTransactionId' => $ref_opayo_transaction->getTransactionId(), 'url' => $url, 'httpCode' => $http_code]
      );
      throw new PaymentGatewayException('Unexpected response in Opayo Pi transaction response, HTTP code: ' . $http_code);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {

    $this->logVerboseDebug('OpayoPiPaymentGateway::createPaymentMethod - called');

    $form_type = $this->configuration['form_type'];
    $required_keys = [
      'card-identifier', 'merchant-session-key'
    ];

    // Use PaymentGatewayException instead of InvalidArgumentException to handle missing data items.
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        $this->log(LogLevel::ERROR, 'OpayoPiPaymentGateway::createPaymentMethod - $payment_details must contain the key: ' . $required_key);
        throw new PaymentGatewayException(sprintf('In OpayoPiPaymentGateway::createPaymentMethod - $payment_details must contain the %s key.', $required_key));
      }
    }

    if ($form_type == 'ownform') {
      // The expiry of the 'card identifier' was 'calculated' by the javascript
      $cardIdentifierExpiry = new DateTime($payment_details["card-identifier-expiry"]);
      $payment_method->setExpiresTime($cardIdentifierExpiry->getTimestamp());

      // Card type and last four digits provided by javascript, useful for feedback on 'review' checkout pane
      if (array_key_exists('card-type', $payment_details))
        $payment_method->card_type = $this->mapCreditCardType($payment_details['card-type']);
      if (array_key_exists('card-last-four-digits', $payment_details)) {
        $payment_method->card_number = $payment_details['card-last-four-digits'];
        if (array_key_exists('extra-info', $payment_details)) {
          $payment_details['extra-info']->cardLastFourDigits = $payment_details['card-last-four-digits'];
        }
      }

    }
    else {
      // Tokenised 'Payment Identifier' is valid for 400 seconds according to Opayo Pi documentation.
      // When using the drop-in form the Opayo javascript doesn't provide us the expiry time
      // Assume the time of the requestion minus 5 seconds.
      $cardIdentifierExpiry = \Drupal::time()->getRequestTime() + 400 - 5;
      $payment_method->setExpiresTime($cardIdentifierExpiry);
    }

    // Currently not doing reusable, perhaps sometime in the future
    $payment_method->setReusable(false);

    // Combine Opayo provided key and identifier into the 'Remote ID' to be used to submit the transaction
    $card_identifier = $payment_details['card-identifier'];
    $merchant_session_key = $payment_details['merchant-session-key'];
    $payment_method->setRemoteId($merchant_session_key . ":" . $card_identifier);
    $payment_method->save();

    $this->logVerboseDebug('OpayoPiPaymentGateway::createPaymentMethod - updated and saved payment method (id: ' . $payment_method->id() . '), remote ID: ' . $payment_method->getRemoteId() );

    if (array_key_exists('extra-info', $payment_details)) {
      $extra_info = $payment_details['extra-info'];
      $extra_info_json = json_encode($extra_info);
      if (is_string($extra_info_json) && $payment_method->hasField('extra_info_json') && strlen($extra_info_json) <= 4000) {
        // Store the extra info with the payment method in field 'extra_info'
        $payment_method->set('extra_info_json', $extra_info_json);
        $payment_method->save();
      }
      else {
        $session = \Drupal::request()->getSession();
        // Store the extra info in the session linked to the payment method's 'remote id'
        $session->set('checkout-id-' . $payment_method->getRemoteId() . '-browser-extra-info', $extra_info);
      }
    }

    // Log saving of the merchant session key and card identifier with the payment method
    $configuration = $this->getConfiguration();
    if ($this->getOrder() != null)
        $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::createPaymentMethod: order {orderId} (uid: {orderUID}), mode: {mode}, saved payment method {methodId} with remote ID {remoteId}: merchant session key: {msk}, card identifier: {cardIdentifier}',
          ['mode' => $configuration['mode'], 'orderId' => $this->getOrder()->id(), 'orderUID' => $this->getOrder()->uuid(),
          'msk' => $merchant_session_key, 'methodId' => $payment_method->id(), 'cardIdentifier' => $card_identifier, 'remoteId' => $payment_method->getRemoteId() ]);
    else
        $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::createPaymentMethod: saved payment method {methodId} with remote ID {remoteId}: merchant session key: {msk}, card identifier: {cardIdentifier}',
          ['mode' => $configuration['mode'], 'msk' => $merchant_session_key, 'methodId' => $payment_method->id(), 'cardIdentifier' => $card_identifier, 'remoteId' => $payment_method->getRemoteId() ]);
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Currently not supporting  'reusable identifiers' or 'credentials on file'
    // Even if we did, Opayi PI API does not seem to offer the ability to delete thse

    // Delete the local entity.
    $payment_method->delete();
  }



  /**
   * Returns a unique identifier for the checkout/payment session, normally the order ID
   *
   * @return string:null
   *   Unique identifier
   */
  protected function getUniqueIdentifier() {
    if ($this->getOrder() != null)
      return $this->getOrder()->id();
    else
      return null;
  }


  /**
   * Returns the JSON representation of the (payment) transaction request parameters
   *
   * @param OrderInterface $order
   *   Order details
   * @param PaymentInterface $payment
   *   Payment settings
   * @param array $configuration
   *   Payment gateway configuration data
   * @param stdClass $extra_info
   *   Extra payment info returned
   *
   * @return stdClass
   *   stdClass representation of the JSON representing the parameters
   */
  protected function transactionRequestContents(OrderInterface $order, PaymentInterface $payment, $configuration, ?stdClass &$extra_info) {

    /**
     * @var \Symfony\Component\HttpFoundation\Request $request;
     */
    $request = $this->requestStack->getCurrentRequest();
    $payment_method = $payment->getPaymentMethod();

    //Change default description value with order items labels
    $configuration['opayo_order_description'] .= ', items:' . $this->getDescriptionFromProducts($order);

    $post_fields = new StdClass();
    $post_fields->transactionType = "Payment";

    if ($configuration['test_with_deferred_payment'] == true) {
      $post_fields->transactionType = "Deferred";
      $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::transactionRequestContents: changed transactionType from Payment to Deferred for order {orderId}, mode: {mode}', ['mode' => $configuration['mode'], 'orderId' => $order->id()]);
    }

    // Use the merchant session key and card identifier as the payment method
    $remote_id = $payment_method->getRemoteId();
    if ($remote_id != null) {
      $remote_id_array = explode(':', $remote_id);
      if (empty($remote_id_array[0]) || empty($remote_id_array[1])) {
        if ($remote_id != null && $remote_id != '')
        {
          // If the 'remote ID' contains a string but not the combination of 'merchant session key' and 'card identifier' then it will be
          // an Opayo 'transaction ID'. This means that the 'merchant session key' and/or 'card identifier' have been 'used' and will need
          // to be regenerated.
          $this->messenger()->addMessage('Unable to complete the transaction. Please go back to check and re-enter your card details', \Drupal\Core\Messenger\MessengerInterface::TYPE_WARNING);
          $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::transactionRequestContents: card details will have to be re-entered for order {orderId} (uid: {orderUID}), mode: {mode}, remote ID: {remoteId}', ['remoteId' => $remote_id, 'mode' => $configuration['mode'], 'orderId' => $order->id(), 'orderUID' => $order->uuid()]);
          $payment_method->delete();
          // Ideally set the 'error step id' for the checkout flow but that's not accessible from here.
          // Instead update the order's
          throw new DeclineException('ERROR Merchant key or Card identifier missing in "transactionRequestContents" function');
        }
        else {
          $this->messenger()->addMessage('Technical error. Please re-enter your card details and try again.', \Drupal\Core\Messenger\MessengerInterface::TYPE_WARNING);
          $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::transactionRequestContents: Merchant key or card identifier missing for order {orderId} (uid: {orderUID}), mode: {mode}', ['mode' => $configuration['mode'], 'orderId' => $order->id(), 'orderUID' => $order->uuid()]);
          $payment_method->delete();
          throw new PaymentGatewayException('ERROR Merchant key or Card identifier missing in "transactionRequestContents" function');
        }
      }
    }
    else {
      $this->messenger()->addMessage('Technical error. Please re-enter your card details and try again.', \Drupal\Core\Messenger\MessengerInterface::TYPE_WARNING);
      $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::transactionRequestContents: Remote ID missing for payment method {paymentMethodId}, order {orderId} (uid: {orderUID}), mode: {mode}', ['paymentMethodId' => $payment_method->id(), 'mode' => $configuration['mode'], 'orderId' => $order->id(), 'orderUID' => $order->uuid()]);
      $payment_method->delete();
      throw new PaymentGatewayException('ERROR missing payment method Remote ID in "transactionRequestContents" function');
    }

    $paymentMethod = new stdClass();
    $paymentMethod->card = new StdClass();
    $paymentMethod->card->merchantSessionKey = $remote_id_array[0];
    $paymentMethod->card->cardIdentifier = $remote_id_array[1];
    $paymentMethod->card->reusable = 'false';
    $paymentMethod->card->save = 'false';
    $post_fields->paymentMethod = $paymentMethod;

    $post_fields->vendorTxCode = $payment->getOrderId()  .  time();

    // Opayo expects the amount in pence
    $amount_in_pence = intval(Calculator::multiply($order->getTotalPrice()->getNumber(), '100', 0));
    $post_fields->amount = $amount_in_pence;

    $currency_code = $payment->getAmount()->getCurrencyCode();
    $post_fields->currency = $currency_code;
    $post_fields->description = $configuration['opayo_order_description'];
    // Opayo API 'description' is 40 characters max
    if (strlen($post_fields->description) > 40)
      $post_fields->description = substr($post_fields->description, 0, 40);

    $billing_address = null;
    $billing_profile = null;
    if ($billing_profile = $payment_method->getBillingProfile())
    {
      /** @var \Drupal\Core\Field\FieldItemBase $address_item */
      $address_item = $billing_profile->get('address')->first();
      $billing_address = $address_item->toArray();
    }
    if ($billing_address == null) {
      $this->messenger()->addMessage('Billing address missing in "transactionRequestContents" function', \Drupal\Core\Messenger\MessengerInterface::TYPE_WARNING);
      $this->log(LogLevel::WARNING, 'OpayoPiPaymentGatewayBilling::transactionRequestContents: address missing for order {orderId} (uid: {orderUID}), mode: {mode}', ['mode' => $configuration['mode'], 'orderId' => $order->id(), 'orderUID' => $order->uuid()]);
      throw new PaymentGatewayException('Billing address missing in "transactionRequestContents" function');
    }

    $post_fields->apply3DSecure = $configuration['3d_secure'];
    $post_fields->applyAvsCvcCheck = $configuration['opayo_apply_avs_cvc'];

    $post_fields->customerFirstName = substr($billing_address['given_name'], 0, 20);
    $post_fields->customerLastName = substr($billing_address['family_name'], 0, 20);


    $billingAddress = new stdClass();
    $billingAddress->address1 = substr($billing_address['address_line1'], 0, 50);
    $billingAddress->address2 = substr($billing_address['address_line2'], 0, 50);
    $billingAddress->city = substr($billing_address['locality'], 0, 40);
    $billingAddress->postalCode = substr($billing_address['postal_code'], 0, 10);
    $billingAddress->country = $billing_address['country_code'];
    $billingAddress->state = $billing_address['administrative_area'];

    // When testing - hardcode the address and postcode to the ones associated with the test cards
    if ($configuration['mode'] == 'test') {
      $billingAddress->address1 = '88';
      $billingAddress->postalCode = '412';
    }

    $post_fields->billingAddress = $billingAddress;

    $post_fields->entryMethod = 'Ecommerce';

    // Context fields to support strong customer authentication
    $base_path = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    $notification_path = Url::fromRoute('commerce_opayo_pi.3d_secure_page', ['order' => $order->id()], ['absolute' => TRUE])->toString();
    $strongCustomerAuthentication = new stdClass();
    $strongCustomerAuthentication->website = $base_path;
    $strongCustomerAuthentication->notificationURL = $notification_path;
    $strongCustomerAuthentication->browserIP = $request->getClientIp();
    $acceptContentTypes = $request->getAcceptableContentTypes();
    $strongCustomerAuthentication->browserAcceptHeader = implode(', ', $acceptContentTypes);
    $strongCustomerAuthentication->browserUserAgent = $request->headers->get('User-Agent');

    // The following settings are required by Opayo
    $strongCustomerAuthentication->browserJavaEnabled = 'false';  // Hardcoded fixed value - no browser reports back on this anymore

    $extra_info = null;

    // If 'own form' is chosen then the javascript has collected and posted back some browser info settings
    $acceptLanguages = $request->getLanguages();
    try {
      if ($payment_method->hasField('extra_info_json'))
      {
        $extra_info_json = $payment_method->get('extra_info_json')->value;
        if ($extra_info_json != null && strlen($extra_info_json) > 0) {
          $extra_info = json_decode($extra_info_json);
        }
      }
      if ($extra_info == null) {
        $session = $this->requestStack->getCurrentRequest()->getSession();
        $extra_info = $session->get('checkout-id-' . $remote_id . '-browser-extra-info');
      }

      if ($extra_info != null) {
        if (array_search($extra_info->colorDepth, ['1', '4', '8', '15', '16', '24', '32', '48']))
          $strongCustomerAuthentication->browserColorDepth = $extra_info->colorDepth;
        else
          $strongCustomerAuthentication->browserColorDepth = 32;
        $strongCustomerAuthentication->browserScreenHeight = $extra_info->screenHeight;
        $strongCustomerAuthentication->browserScreenWidth = $extra_info->screenWidth;
        $strongCustomerAuthentication->browserTZ = $extra_info->timeZoneOffset;
        if (property_exists($extra_info, 'language') && $extra_info->language != null)
          $strongCustomerAuthentication->browserLanguage = $extra_info->language;
      }
      else {
        // defaults
        $extra_info = new stdClass();
        $strongCustomerAuthentication->browserColorDepth = 32;
        $strongCustomerAuthentication->browserScreenHeight = 512;
        $strongCustomerAuthentication->browserScreenWidth = 1024;
        $strongCustomerAuthentication->browserTZ = 60;
      }
    }
    catch (Exception $e) {
      // defaults
      $extra_info = new stdClass();
      $strongCustomerAuthentication->browserColorDepth = 32;
      $strongCustomerAuthentication->browserScreenHeight = 512;
      $strongCustomerAuthentication->browserScreenWidth = 1024;
      $strongCustomerAuthentication->browserTZ = 60;
    }

    // Pi integration only works if javascript is enabled
    $strongCustomerAuthentication->browserJavascriptEnabled = 'true';
    // Get the browser language from the client's request headers if not provided by the javascript
    if ((!property_exists($strongCustomerAuthentication, 'browserLanguage')) && count($acceptLanguages) > 0) {
      $lng = explode('_', $acceptLanguages[0]);
      $strongCustomerAuthentication->browserLanguage = $lng[0];
    }
    $strongCustomerAuthentication->challengeWindowSize = 'Small';
    $strongCustomerAuthentication->transType = 'GoodsAndServicePurchase';

    $post_fields->strongCustomerAuthentication = $strongCustomerAuthentication;

    // Customer email and phone required
    $custContacts = $this->getCustomerEmailAndPhone($order, $configuration, $extra_info);

    if ($custContacts['customerEmail'] != null)
      $post_fields->customerEmail = substr($custContacts['customerEmail'], 0, 80);
    else
      $post_fields->customerEmail = null;
    if ($custContacts['customerPhone'] != null)
    $post_fields->customerPhone = substr($custContacts['customerPhone'], 0, 19);
    else {
      // Has a phone number field been added to the billing profile?
      try {
        $phone_nr_field_name = $configuration['telephone_field_name'];

        /** @var \Drupal\Core\Field\FieldItemBase $phone_nr_item */
        $phone_nr_item = $billing_profile->get($phone_nr_field_name)->first();
        $phoneNr = $phone_nr_item->toArray();
        $post_fields->customerPhone = substr($phoneNr['value'], 0, 19);
      } catch (Exception $pe) {
      }
    }

    return $post_fields;
  }


    /**
   * Returns the JSON representation of the refund transaction request parameters
   *
   * @param OpayoTransaction $referenceOpayoTransaction
   *   Opayo transaction representing the original/reference payment against which the refund is made
   * @param string $amount
   *   Refund amount
   *
   * @return stdClass
   *   stdClass representation of the JSON representing the parameters
   */
  protected function refundTransactionRequestContents(OpayoTransaction $referenceOpayoTransaction, string $amount) {

    $post_fields = new StdClass();
    $post_fields->transactionType = "Refund";
    $post_fields->referenceTransactionId = $referenceOpayoTransaction->getTransactionId();
    $post_fields->vendorTxCode = $referenceOpayoTransaction->getOrder()->id() . time();

    // Opayo expects the amount in pence
    $amount_in_pence = intval(Calculator::multiply($amount, '100', 0));
    $post_fields->amount = $amount_in_pence;

    // Description - ideally provided by the admin person doing the refund but 'order/payment' comments are not an available feature
    // Instead supply the name/email of the admin user
    $user = \Drupal::currentUser();
    $post_fields->description = $user->getDisplayName() . '(' . $user->getEmail() . ')';
    // Opayo API 'description' is 40 characters max
    if (strlen($post_fields->description) > 40)
      $post_fields->description = substr($post_fields->description, 0, 40);

    return $post_fields;
  }

  /**
   * get description from order items labels
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return bool|string
   */
  protected function getDescriptionFromProducts(OrderInterface $order) {
    $items = $order->getItems();
    // Create basket from saved products.
    /** @var OrderItemInterface $item */
    $description = "";
    foreach ($items as $item) {
      $description = $description . " " . $item->label() . "(quantity: " . $item->getQuantity() . "),";
    }

    return substr($description, 0, -1);
  }


  /**
   * Customer's email address and phone are required for creating an Opayo Pi transaction
   * Please note that the phone number must be in the format of '+' and 'country code' and 'phone number'.
   *
   * @param OrderInterface $order
   *   Order details
   * @param array $configuration
   *   Payment gateway configuration data
   * @param stdClass $extraInfo
   *   Additional info from the session and the 'add payment method' step
   *
   * @return array
   *   Array containing the customer's email and phone number
   */
  protected function getCustomerEmailAndPhone(OrderInterface $order, $configuration, StdClass $extraInfo) {
    $email = null;
    $phone = null;
    $user = \Drupal::currentUser();

    // Email
    if ($user->id() == 0) {
      // Guest user, check for email address and phone number stored with the order
      foreach ($order->getItems() as $order_item) {
        if ($email == null)
        {
          // Email address normally collected on the checkout page
          $email = $order_item->getData('mail');
        }
      }
    } else {
      // Get email from the user's profile
      $email = $user->getEmail();
    }

    if (property_exists($extraInfo, 'billingTelephoneNumber')) {
      // Telephone number part of the 'extra info'
      $phone = $extraInfo->billingTelephoneNumber;
    }
    else {
      // Phone number - Gateway config determines which field has the telephone number
      $field_name = $configuration['telephone_field_name'];
      foreach ($order->getItems() as $order_item) {
        if ($phone == null) {
          $phone = $order_item->getData($field_name);
        }
      }
    }

    $custContact = array();
    $custContact['customerEmail'] = $email;
    $custContact['customerPhone'] = $phone;

    // Allow email address and phone number to be overridden
    $this->moduleHandler->alter('commerce_opayo_pi_email_and_phone', $custContact, $order, $extraInfo);

    return $custContact;
  }


  /**
   * {@inheritdoc}
   */
  public function getNewMerchantSessionKey($orderId = null) {

    if ($orderId != null) {
      // If there are delays or validation errors during checkout there may be multiple merchant session keys requested and used
      // in the process of checkout.
      // Keep track of previously requested ones and store them in an array

      // Gets the list of merchant session keys for the order
      $merchant_session_keys = $this->privateTempStore->get('merchantSessionKey-' . $orderId);
      if ($merchant_session_keys == null || gettype($merchant_session_keys) != 'array')
        $merchant_session_keys = array();

      // Request new merchant session key from Opayo
      $merchant_session_key = $this->requestMerchantSessionKey();

      // Keep track of the number of times a merchant session key has been requested for the order
      $merchant_session_key->sequenceNr = count($merchant_session_keys) + 1;
      $merchant_session_key->isUsed = null;

      // Store in the private temp store

      array_push($merchant_session_keys, $merchant_session_key);
      $this->privateTempStore->set('merchantSessionKey-' . $orderId, $merchant_session_keys);

      $config = $this->getConfiguration();
      $this->log(LogLevel::DEBUG, 'OpayoPiPaymentGateway::getNewMerchantSessionKey: sequence nr: {sequence} for session key: {msk} (expiry: {mskexpiry}) for order {orderId} (uid: {orderUID}), mode: {mode}',
        ['mode' => $config['mode'], 'orderId' => $this->getOrder()->id(), 'orderUID' => $this->getOrder()->uuid(), 'sequence' => $merchant_session_key->sequenceNr, 'msk' => $merchant_session_key->merchantSessionKey, 'mskexpiry' => $merchant_session_key->expiry]);

      return $merchant_session_key;
    }
    else {
      // Scenario where we have to take payment but there is no 'order'
      // Request new merchant session key from Opayo
      $merchant_session_key = $this->requestMerchantSessionKey();
      $merchant_session_key->isUsed = null;

      $config = $this->getConfiguration();
      $this->log(LogLevel::DEBUG, 'OpayoPiPaymentGateway::getNewMerchantSessionKey: new session key: {msk} (expiry: {mskexpiry}), mode: {mode}',
        ['mode' => $config['mode'], 'msk' => $merchant_session_key->merchantSessionKey, 'mskexpiry' => $merchant_session_key->expiry]
      );
      return $merchant_session_key;
    }
  }

  /**
   * Log a message at the payment gateway's 'extra verbose debug' level
   *
   * @param string|\Stringable $message
   *   Log message
   * @param array $context
   *   Additional info
   */
  public function logVerboseDebug(string|\Stringable $message, array $context = []) {
    $gateway_log_level = $this->getConfiguration()['gateway_log_level'];
    if ($gateway_log_level >= self::LOGLEVEL_VERBOSEDEBUG) {

      /**
       * @var \Symfony\Component\HttpFoundation\Request $request;
       */
      $request = $this->requestStack->getCurrentRequest();
      $requestid = $request->getRequestUri() . '/' . $request->getQueryString() . '-' . $request->getSession()->getId();
      $uniqueid = sprintf("%08x", abs(crc32($request->getClientIp() . (isset($_SERVER) && array_key_exists('REQUEST_TIME', $_SERVER) ? $_SERVER['REQUEST_TIME'] : '') . (isset($_SERVER) && array_key_exists('REMOTE_PORT', $_SERVER) ? $_SERVER['REMOTE_PORT'] : ''))));
      $now = new DateTime('now');

      $this->logger->log(LogLevel::DEBUG, '(' . $now->format("Y-m-d H:i:s") . '.' . substr($now->format("Uv"), -3) . ') ' . $message . ' (' . $requestid . ' ' . $uniqueid . ')', $context);
    }
  }

  /**
   * Log a message via the standard logger(s)
   *
   * @param $level
   *   Log level
   * @param string|\Stringable $message
   *   Log message
   * @param array $context
   *   Additional info
   */
  public function log($level, string|\Stringable $message, array $context = []): void {
    $gateway_log_level = $this->getConfiguration()['gateway_log_level'];
    $requested_log_level = $this->logLevelPsrToRfc($level);
    if ($gateway_log_level >= $requested_log_level) {
      // Log the message
      $this->logger->log($level, $message, $context);
    }
  }

  protected function logLevelPsrToRfc($level) {
    if ($level == LogLevel::DEBUG)
      return RfcLogLevel::DEBUG;
    elseif ($level == LogLevel::INFO)
      return RfcLogLevel::INFO;
    elseif ($level == LogLevel::NOTICE)
      return RfcLogLevel::NOTICE;
    elseif ($level == LogLevel::WARNING)
      return RfcLogLevel::WARNING;
    elseif ($level == LogLevel::ERROR)
      return RfcLogLevel::ERROR;
    elseif ($level == LogLevel::CRITICAL)
      return RfcLogLevel::CRITICAL;
    elseif ($level == LogLevel::ALERT)
      return RfcLogLevel::ALERT;
    elseif ($level == LogLevel::EMERGENCY)
      return RfcLogLevel::EMERGENCY;
  }


  /**
   * {@inheritdoc}
   */
  public function getCurrentMerchantSessionKey($order_id) {
    $merchant_session_keys = $this->privateTempStore->get('merchantSessionKey-' . $order_id);
    if ($merchant_session_keys == null)
      return null;
    else
      // The last one
      return end($merchant_session_keys);
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateCurrentMerchantSessionKey($order_id) {
    // Gets the merchant session key from the private temp store, if available
    $merchant_session_keys = $this->privateTempStore->get('merchantSessionKey-' . $order_id);

    if ($merchant_session_keys != null) {
      $merchant_session_keys[count($merchant_session_keys) - 1]->isUsed = new DateTime('now');
      $this->privateTempStore->set('merchantSessionKey-' . $order_id, $merchant_session_keys);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateMerchantSessionKey($order_id, $merchant_session_key) {
    // Gets the merchant session key from the private temp store, if available
    $merchant_session_keys = $this->privateTempStore->get('merchantSessionKey-' . $order_id);

    $match = false;
    $index = 0;
    while ($match == false && $index < count($merchant_session_keys))
    {
      $merchantSessionKey = $merchant_session_keys[$index];
      if ($merchantSessionKey->merchantSessionKey == $merchant_session_key) {
        $merchant_session_keys[$index]->isUsed = new DateTime('now');
        $this->privateTempStore->set('merchantSessionKey-' . $order_id, $merchant_session_keys);
      }
      $index++;
    }
  }


  /**
   * {@inheritdoc}
   */
  public function deleteAllMerchantSessionKeys($order_id) {
    $this->privateTempStore->delete('merchantSessionKey-' . $order_id);
  }

  /**
   * Request Opayo merchant session key creation
   *
   * The 'merchant session key' is needed when the browser sends the card details to Opayo for tokenisation into a 'card identifier'
   * It is valid for only 400 seconds
   *
   * @return mixed
   */
  public function requestMerchantSessionKey() {
    $config = $this->getConfiguration();
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $config['uri']['merchant_session_key'],
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => '{ "vendorName": "' . $config['vendor'] . '" }',
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic " . $config['auth'],
        "Cache-Control: no-cache",
        "Content-Type: application/json"
      ),
    ));

    $this->log(LogLevel::DEBUG, 'OpayoPiPaymentGateway::requestMerchantSessionKey: Before merchant session key request, mode: {mode}', ['mode' => $config['mode']]);

    $response = curl_exec($curl);
    $http_code = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
    $err = curl_error($curl);

    if (!empty($err))
      $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::requestMerchantSessionKey: Unsuccessful merchant session key request, mode: {mode}, errors: {errors}', ['mode' => $config['mode'], 'errors' => $err]);
    else
      $this->log(LogLevel::DEBUG, 'OpayoPiPaymentGateway::requestMerchantSessionKey: Successful merchant session key response, mode: {mode}', ['mode' => $config['mode']]);

    $response = json_decode($response);
    curl_close($curl);

    if (!empty($response->merchantSessionKey)) {
      $this->log(LogLevel::INFO, 'OpayoPiPaymentGateway::requestMerchantSessionKey: new merchant session key: {msk} (expiry: {mskexpiry}), mode: {mode}', ['mode' => $config['mode'], 'msk' => $response->merchantSessionKey, 'mskexpiry' => $response->expiry]);
      return $response;
    }
    else {
      $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::requestMerchantSessionKey: Unsuccessful merchant session key request, mode: {mode}, HTTP code: {httpcode}', ['mode' => $config['mode'], 'httpcode' => $http_code]);
      throw new PaymentGatewayException("Could not acquire Opayo 'merchant session key', error: " . $err . "(code: " . $http_code . ")");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getOrder() {
    if ($this->order === NULL) {
      $this->order = $this->routeMatch->getParameter('commerce_order');
    }
    return $this->order;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrder(OrderInterface $order) {
    $this->order = $order;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentGateway() {
    return $this->parentEntity;
  }

  /**
   * If an Opayo payment transaction fails then the merchant session key and payment identifier it used are no longer valid.
   * These were associated with a 'payment method' which is also no longer valid.
   * Delete the payment method and disassociate from the order/cart
   *
   * @param OrderInterface $order
   *   The cart
   *
   */
  protected function orderResetPaymentMethod(OrderInterface $order) {
    /** @var PaymentMethodInterface $payment_method */
    $payment_method = $order->get('payment_method')->entity;
    $payment_method_id = '';
    $remote_id = '';
    if ($payment_method != null) {
      $payment_method_id = $payment_method->id();
      $remote_id = $payment_method->getRemoteId();
      // Delete entity
      $payment_method->delete();
    }

    $order->set('payment_gateway', null);
    $order->set('payment_method', null);
    $order->save();
    $this->log(LogLevel::WARNING, 'OpayoPiPaymentGateway::orderResetPaymentMethod: cart {orderId}, unset payment method (ID: {paymentMethodId}, remote ID: {remoteId}', ['orderId' => $this->order->id(), 'paymentMethodId' => $payment_method_id, 'remoteId' => $remote_id]);
  }


  public function storePaymentErrorMessage(int $orderId, string$message, ?OpayoTransactionInterface $opayoTransaction = null) {

    $session = \Drupal::request()->getSession();
    // Store the error message in the session linked to the order ID
    $session->set('order-id-' . $orderId . '-payment-error-message', $message);

    if ($opayoTransaction != null) {
    // 3D secure responses get received from Opayo and therefore don't share a session with the end user.
    // We need to get a message to the user about the failed payment.
    // Store the message with the 'Opayo Transaction' entity's database record
      $opayoTransaction->setUserWarningMessage($message);
      $opayoTransaction->save();
      $this->logVerboseDebug('OpayoPiPaymentGateway::storePaymentErrorMessage - Opayo transaction: ' . $opayoTransaction->getTransactionId() . ', message: ' . $message);
    }
    else {
      $this->logVerboseDebug('OpayoPiPaymentGateway::storePaymentErrorMessage - no Opayo transaction, stored in session, message: ' . $message);
    }
  }


  /**
   * Maps the Sagepay credit card type to a Commerce credit card type.
   *
   * @param string $card_type
   *   The Opayo credit card type.
   *
   * @return string
   *   The Commerce credit card type.
   */
  protected function mapCreditCardType($card_type) {
    $map = [
      'amex' => 'amex',
      'AmericanExpress' => 'amex',
      'diners' => 'dinersclub',
      'Diners' => 'dinersclub',
      'discover' => 'discover',
      'jcb' => 'jcb',
      'mastercard' => 'mastercard',
      'MasterCard' => 'mastercard',
      'MC' => 'mastercard',
      'Visa' => 'visa',
      'visa' => 'visa',
      'Maestro' => 'maestro'
    ];
    if (!isset($map[$card_type])) {
      throw new HardDeclineException(sprintf('Unsupported credit card type "%s".', $card_type));
    }

    return $map[$card_type];
  }

}
