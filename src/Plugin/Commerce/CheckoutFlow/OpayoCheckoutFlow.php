<?php

namespace Drupal\commerce_opayo_pi\Plugin\Commerce\CheckoutFlow;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesBase;
use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * @CommerceCheckoutFlow(
 *  id = "opayo_checkout_flow",
 *  label = @Translation("Opayo checkout flow"),
 * )
 */
class OpayoCheckoutFlow extends CheckoutFlowWithPanesBase {

  /**
   * Log a message at the payment gateway's 'extra verbose debug' level
   *
   * @param string|\Stringable $message
   *   Log message
   * @param array $context
   *   Additional info
   */
  public function logVerboseDebug(string|\Stringable $message, array $context = []) {
    if ($this->order != null && $this-> order->get('payment_gateway') != null && $this->order->get('payment_gateway')->entity != null && $this->order->get('payment_gateway')->entity->getPlugin() != null) {
      /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $payment_gateway_plugin */
      $payment_gateway_plugin = $this->order->get('payment_gateway')->entity->getPlugin();
      $checkout_flow = $this->order->get('checkout_flow')->entity;
      $payment_gateway_plugin->logVerboseDebug('OpayoCheckoutFlow(' . $checkout_flow->id() . ':' . $this->pluginId . ')::' . $message, $context);
    }
  }


  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    // Note that previous_label and next_label are not the labels
    // shown on the step itself. Instead, they are the labels shown
    // when going back to the step, or proceeding to the step.
    return [
      'login' => [
        'label' => $this->t('Login'),
        'previous_label' => $this->t('Go back'),
        'has_sidebar' => FALSE,
      ],
      'order_information' => [
        'label' => $this->t('Order information'),
        'has_sidebar' => TRUE,
        'previous_label' => $this->t('Go back'),
      ],
      'review' => [
        'label' => $this->t('Review'),
        'next_label' => $this->t('Continue to review'),
        'previous_label' => $this->t('Go back'),
        'has_sidebar' => TRUE,
      ],
      'payment' => [
        'label' => $this->t('Payment'),
        'next_label' => $this->t('Pay and complete purchase'),
        'has_sidebar' => FALSE,
        'hidden' => TRUE,
      ],
      'threedsecure' => [
        'label' => $this->t('3D Secure'),
        'next_label' => $this->t('Continue'),
        'previous_label' => $this->t('Go back'),
        'has_sidebar' => TRUE,
      ],
      'complete' => [
        'label' => $this->t('Complete'),
        'next_label' => $this->t('Complete checkout'),
        'has_sidebar' => FALSE,
      ],
    ];
  }


  /**
   * {@inheritdoc}
   */
  protected function onStepChange($step_id) {
    $order = $this->getOrder();
    $this->logVerboseDebug('onStepChange step id: ' . $step_id . ', order: ' . $order->id() . ', current state: ' . $order->getState()->getId());

    // Lock the order while on the 'payment' checkout step. Unlock elsewhere.
    if ($step_id == 'payment' || $step_id == 'threedsecure') {
      $order->lock();
    }
    elseif ($step_id != 'payment' && $step_id != 'threedsecure') {
      $order->unlock();
    }
    // Place the order.
    if ($step_id == 'complete' && $order->getState()->getId() == 'draft') {
      // Notify other modules.
      $event = new OrderEvent($order);
      $this->eventDispatcher->dispatch($event, CheckoutEvents::COMPLETION);
      $order->getState()->applyTransitionById('place');
    }
  }


  public function onStepChangeCustom($step_id) {
    $this->logVerboseDebug('onStepChangeCustom step id: ' . $step_id);
    $this->onStepChange($step_id);
  }

  public function setOrder($order) {
    $this->order = $order;
  }


  /**
   * {@inheritdoc}
   */
  public function getNextStepId($step_id) {
    $next_step = null;
    if ($step_id == 'complete') {
      // There is no step after complete, return 'complete'
      $next_step = $step_id;
    }
    else {
      $next_step = parent::getNextStepId($step_id);
    }
    return $next_step;
  }


  /**
   * {@inheritdoc}
   */
  public function getVisibleSteps() {

    // In the context of a webform order we may need to selective include/exclude the 'payment information'
    // Normally it's excluded but if there has been a failed payment attempt we need to include it

    if (empty($this->visibleSteps)) {
      $steps = $this->getSteps();
      foreach ($steps as $step_id => $step) {
        if (!$this->isStepVisible($step_id)) {
          unset($steps[$step_id]);
        }
      }
      $this->visibleSteps = $steps;
    }

    return $this->visibleSteps;
  }


  /**
   * {@inheritdoc}
   */
  public function redirectToStep($step_id) {
    $order = $this->getOrder();
    $current_checkout_step = $order->get('checkout_step')->value;

    if (!$this->isStepVisible($step_id)) {
      throw new \InvalidArgumentException(sprintf('Invalid step ID "%s" passed to redirectToStep().', $step_id));
    }

    if ($step_id !== $current_checkout_step) {
      // Payment gateway may have set the step to 'complete' prematurely - prevent duplicate handling of step change
      $this->logVerboseDebug('redirectToStep, order: ' . $order->id() . ' change checkout step from: ' . $current_checkout_step . ' to: ' . $step_id . ', update order');
      $order->set('checkout_step', $step_id);
      $this->onStepChange($step_id);
      $order->save();
    }
    else
      $this->logVerboseDebug('redirectToStep, order: ' . $order->id() . ' unchanged - no change in checkout step (' . $current_checkout_step . ')');

    throw new NeedsRedirectException(Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $order->id(),
      'step' => $step_id,
    ])->toString());
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $step_id = NULL) {
    $this->logVerboseDebug('buildForm - step: ' . $step_id);
    $form = parent::buildForm($form, $form_state, $step_id);
    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $order = $this->getOrder();
    foreach ($this->getVisiblePanes($form['#step_id']) as $pane_id => $pane) {
      if (!isset($form[$pane_id])) {
        continue;
      }
      $this->logVerboseDebug('submitForm, order: ' . $order->id() . ' submit pane: '. $pane_id);
      $pane->submitPaneForm($form[$pane_id], $form_state, $form);
    }
    if ($this->hasSidebar($form['#step_id'])) {
      foreach ($this->getVisiblePanes('_sidebar') as $pane_id => $pane) {
        if (!isset($form['sidebar'][$pane_id])) {
          continue;
        }
        $this->logVerboseDebug('submitForm, order: ' . $order->id() . ' submit sidebar pane: ' . $pane_id);
        $pane->submitPaneForm($form['sidebar'][$pane_id], $form_state, $form);
      }
    }

    if ($next_step_id = $this->getNextStepId($form['#step_id'])) {
      $this->logVerboseDebug('submitForm, order: ' . $order->id() . ' next step: ' . $next_step_id . ', call onStepChange');
      $order->set('checkout_step', $next_step_id);
      $this->onStepChange($next_step_id);

      $form_state->setRedirect('commerce_checkout.form', [
        'commerce_order' => $order->id(),
        'step' => $next_step_id,
      ]);
    }
    else
      $this->logVerboseDebug('submitForm, order: ' . $order->id() . ' no next step');

    $order->save();
  }


}
