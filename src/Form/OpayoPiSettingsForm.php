<?php

namespace Drupal\commerce_opayo_pi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Opayo Pi module settings
 */
class OpayoPiSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'commerce_opayo_pi.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_opayo_pi_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['vendor'] = [
      '#type' => 'textfield',
      '#title' => t('Opayo Vendor Name'),
      '#description' => t('This is the vendor name provided by Opayo when you set up your account.'),
      '#required' => TRUE,
      '#default_value' => $config->get('vendor'),
    ];

    $form['live_integration_key'] = [
      '#type' => 'textfield',
      '#title' => t('LIVE Integration Key'),
      '#default_value' => $config->get('live_integration_key'),
      '#required' => TRUE,
    ];

    $form['live_integration_password'] = [
      '#type' => 'textfield',
      '#title' => t('LIVE Integration Password'),
      '#default_value' => $config->get('live_integration_password'),
      '#required' => TRUE,
    ];

    $form['test_integration_key'] = [
      '#type' => 'textfield',
      '#title' => t('TEST Integration Key'),
      '#default_value' => $config->get('test_integration_key'),
      '#required' => TRUE,
    ];

    $form['test_integration_password'] = [
      '#type' => 'textfield',
      '#title' => t('TEST Integration Password'),
      '#default_value' => $config->get('test_integration_password'),
      '#required' => TRUE,
    ];

    $form['payment_method_expiry_cleanup'] = [
      '#type' => 'textfield',
      '#title' => t('Commerce expired payment methods cleanup'),
      '#description' => t('Nr days after wich expired Commerce payment methods get deleted (0 to never delete, -1 to delete once expired)'),
      '#required' => TRUE,
      '#default_value' => $config->get('payment_method_expiry_cleanup'),
    ];

    $form['transaction_expiry_cleanup'] = [
      '#type' => 'textfield',
      '#title' => t('Opayo expired transactions cleanup'),
      '#description' => t('Nr days after wich the record of expired - and never completed - Opayo transaction attempts get deleted (0 to never delete)'),
      '#required' => TRUE,
      '#default_value' => $config->get('transaction_expiry_cleanup'),
    ];

    $form['enable_batch_id_matching'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable fetching of the bank settlement (batch) IDs for Opayo transactions'),
      '#description' => $this->t('Enable regular cron-based queries to fetch the bank settlement batch IDs for transactions using the Reporting & Admin API, wich requires the account password (different from API integration password)'),
      '#default_value' => $config->get('enable_batch_id_matching'),
    ];

    $form['live_account_user'] = [
      '#type' => 'textfield',
      '#title' => t('LIVE Account User'),
      '#default_value' => $config->get('live_account_user'),
      '#required' => FALSE,
    ];

    $form['live_account_password'] = [
      '#type' => 'textfield',
      '#title' => t('LIVE Account Password'),
      '#default_value' => $config->get('live_account_password'),
      '#required' => FALSE,
    ];

    $form['test_account_user'] = [
      '#type' => 'textfield',
      '#title' => t('TEST Account User'),
      '#default_value' => $config->get('test_account_user'),
      '#required' => FALSE,
    ];

    $form['test_account_password'] = [
      '#type' => 'textfield',
      '#title' => t('TEST Account Password'),
      '#default_value' => $config->get('test_account_password'),
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::SETTINGS)
      // Set the submitted configuration setting.
    ->set('vendor', $form_state->getValue('vendor'))
    ->set('live_integration_key', $form_state->getValue('live_integration_key'))
    ->set('live_integration_password', $form_state->getValue('live_integration_password'))
    ->set('test_integration_key', $form_state->getValue('test_integration_key'))
    ->set('test_integration_password', $form_state->getValue('test_integration_password'))
     ->set('enable_batch_id_matching', $form_state->getValue('enable_batch_id_matching'))
     ->set('live_account_user', $form_state->getValue('live_account_user'))
     ->set('live_account_password', $form_state->getValue('live_account_password'))
     ->set('test_account_user', $form_state->getValue('test_account_user'))
     ->set('test_account_password', $form_state->getValue('test_account_password'))
     ->set('payment_method_expiry_cleanup', $form_state->getValue('payment_method_expiry_cleanup'))
     ->set('transaction_expiry_cleanup', $form_state->getValue('transaction_expiry_cleanup'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
