<?php

namespace Drupal\commerce_opayo_pi\PluginForm\OpayoPi;

use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\commerce_payment\PluginForm\PaymentMethodFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use DateTime;
use Drupal;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldItemList;
use Exception;
use stdClass;
use Psr\Log\LogLevel;

class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;


    /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static(
      $container->get('commerce_store.current_store'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('logger.channel.commerce_payment')
    );
    $instance->setModuleHandler($container->get('module_handler'));
    return $instance;
  }

  public function setModuleHandler(ModuleHandlerInterface $moduleHandler) {
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form = PaymentMethodFormBase::buildConfigurationForm($form, $form_state);

    $this->processPaymentWarningMessage($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;

    $form['payment_details'] = [
      '#parents' => array_merge($form['#parents'], ['payment_details']),
      '#type' => 'container',
      '#payment_method_type' => $payment_method->bundle(),
    ];

    if ($payment_method->bundle() == 'credit_card_opayo') {
      /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $pgplugin */
      $pgplugin = $this->plugin;
      $gateway_config = $pgplugin->getConfiguration();
      if ($gateway_config['form_type'] == 'dropin') {
        // Use drop-in version of the form
        $form['payment_details'] = $this->buildOpayoPiDropinForm($form['payment_details'], $form_state);
      }
      else {
        // Use the 'own form' version
        $form['payment_details'] = $this->buildOpayoPiOwnForm($form['payment_details'], $form_state);
        // Use an #after_build directive to tweak the payment details fields
        $form['payment_details']['#after_build'][] = [get_class($this), 'customisePaymentDetails'];
      }
    }

    // Use an #after_build directive to tweak the billing address fields
    if ($gateway_config['form_type'] == 'ownform') {
      // For 'own form', have cardholder first name and last name with the card details fields rather than with the address fields
      $form['billing_information']['address']['widget'][0]['address']['#after_build'][] = [get_class($this), 'customiseAddress'];
    }

    // Use an #after_build directive to tweak the billing information fields
    $form['billing_information']['#after_build'][] = [get_class($this), 'customiseBillingInfo'];


    // Move the billing information below the payment details.
    if (isset($form['billing_information'])) {
      $form['billing_information']['#weight'] = 10;
    }

    // Allow the payment info to be altered
    $this->moduleHandler->alter('commerce_opayo_pi_payment_method', $form, $form_state);

    return $form;
  }

  /**
   * Tweak some aspects of the address fields for 'own form'
   */
  public static function customisePaymentDetails(array $element, FormStateInterface $form_state) {

    // We want the credit card field form elements to be displayed (and filled in by the user) but not get returned with the post (a tokenised 'card identifier' is returned instead)
    // Taking away the 'name' attribute should ensure this
    unset($element['cardholder-name']['#name']);
    unset($element['number']['#name']);
    unset($element['expiration']['month']['#name']);
    unset($element['expiration']['year']['#name']);
    unset($element['security-code']['#name']);
    // Opayo require the 'first name' and 'last name' when submitting the transaction so these fields are not excluded from the 'post'

    return $element;
  }

  /**
   * Tweak some aspects of the address fields
   */
  public static function customiseAddress(array $element, FormStateInterface $form_state) {

    // We already have the cardholder name field (both for 'drop-in' and 'own form') so don't need the first name, middle name and surname as well
    unset($element['container0']);
    unset($element['given_name']);
    unset($element['family_name']);
    unset($element['additional_name']);

    // Set maximum lengths
    if (array_key_exists('address_line1', $element)) {
      $element['address_line1']['#size'] = 50;
      $element['address_line1']['#maxlength'] = 50;
    }
    if (array_key_exists('address_line2', $element)) {
      $element['address_line2']['#size'] = 50;
      $element['address_line2']['#maxlength'] = 50;
    }
    if (array_key_exists('locality', $element)) {
      $element['locality']['#size'] = 40;
      $element['locality']['#maxlength'] = 40;
    }
    if (array_key_exists('postal_code', $element)) {
      $element['postal_code']['#size'] = 10;
      $element['postal_code']['#maxlength'] = 10;
    }

    return $element;
  }

  /**
   * Tweak some aspects of the billing information
   */
  public static function customiseBillingInfo(array $billingElement, FormStateInterface $form_state) {

    $element = $billingElement;

    if ($form_state->getValue('process_input') == false) {
      $telephoneFieldName = null;
      $payment_gateway_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
      $payment_gateway = null;
      $all_payment_gateways = $payment_gateway_storage->loadMultiple();
      foreach ($all_payment_gateways as $pg_id => $pg) {
        $payment_gateway_plugin = $pg->getPlugin();
        if (is_a($payment_gateway_plugin, 'Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGateway')) {
          $payment_gateway = $pg;
        }
      }
      if ($payment_gateway != null) {
       /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $plugin  */
        $plugin = $payment_gateway->getPlugin();
        $configuration = $plugin->getConfiguration();
        $telephoneFieldName = $configuration['telephone_field_name'];
      }

      if ($telephoneFieldName != null && array_key_exists($telephoneFieldName, $element)) {
        // Phone number is required
        if ($element[$telephoneFieldName]['#required'] == false)
          $element[$telephoneFieldName]['#required'] = true;

        // Match the phone number's country to the billing country

        // This is only effective on initial form build. When the user changes the country the Javascript only affects the 'address' section and
        // will not update the international telephone number field. Update in javascript instead.
        $country_code = null;
        try {
          if (array_key_exists('address', $element) && array_key_exists('widget', $element['address']) && array_key_exists(0, $element['address']['widget']) && array_key_exists('address', $element['address']['widget'][0]) && array_key_exists('country_code', $element['address']['widget'][0]['address'])) {
            $country_code = $element['address']['widget'][0]['address']['country_code']['#value'];
            if ($country_code != null) {
              // Need to get the field name for the telephone number from the gateway configuration
              try {
                // If the telephone field is of type 'phone_international' (https://www.drupal.org/project/phone_international) then the following code copies the country code
                $element[$telephoneFieldName]['widget'][0]['value']['#country'] = $country_code;
                $element[$telephoneFieldName]['widget'][0]['value']['int_phone']['#attributes']['data-country'] = $country_code;
              } catch (Exception $ite) {
              }
            }
          }
        }
        catch (Exception $e) {
        }
      }
    }

    // Allow the billing information to be altered
    \Drupal::service('module_handler')->alter('commerce_opayo_pi_billing_info', $element, $form_state);

    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    PaymentMethodFormBase::validateConfigurationForm($form, $form_state);

    if ($form_state->isSubmitted()) {

      /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
      $payment_method = $this->entity;

      $form_user_input = $form_state->getUserInput();
      $this->logVerboseDebug('PaymentMethodAddForm::validateConfigurationForm - payment method bundle: ' . $payment_method->bundle() . ", form id: " . (array_key_exists('form_id', $form_user_input) ? $form_user_input['form_id'] : '') . ", form build id: " . (array_key_exists('form_build_id', $form_user_input) ? $form_user_input['form_build_id'] : '') . ", form token: " . (array_key_exists('form_token', $form_user_input) ? $form_user_input['form_token'] : ''));

      if ($payment_method->bundle() == 'credit_card_opayo') {
        /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $pgplugin */
        $pgplugin = $this->plugin;
        $gateway_config = $pgplugin->getConfiguration();
        $card_identifier_result = null;
        if ($gateway_config['form_type'] == 'dropin') {
          // Use drop-in version of the form
          $card_identifier_result = $this->validateOpayoPiDropinForm($form['payment_details'], $form_state);
        }
        else {
          $card_identifier_result = $this->validateOpayoPiOwnForm($form['payment_details'], $form_state);

          $cardHolderFirstname = $form_state->getValue($form["payment_details"]["cardholder"]["cardholder-first-name"]['#parents']);
          $cardHolderLastname = $form_state->getValue($form["payment_details"]["cardholder"]["cardholder-last-name"]['#parents']);
          $billingAddress = $form_state->getValue($form["billing_information"]["address"]["widget"][0]["address"]["#parents"]);

          if ($card_identifier_result != null && $card_identifier_result != 'error' && $billingAddress != null)
          {
            // Need to copy the first name and last name from the 'payment details' section to the billing profile
            // (if it's done here the changes will get saved to the database - cannot wait until 'submitConfigurationForm')
            $billingAddress['given_name'] = $cardHolderFirstname;
            $billingAddress['family_name'] = $cardHolderLastname;
            $form_state->setValueForElement($form["billing_information"]["address"]["widget"][0]["address"], $billingAddress);
          }

          // Length checks, see https://developer.elavon.com/products/opayo/v1/api-reference#tag/Transactions/operation/createTransaction
          if (strlen($cardHolderFirstname) > 20)
            $form_state->setErrorByName('payment_details][cardholder][cardholder-first-name', 'First Name too long, maximum length 20 characters');
          if (strlen($cardHolderLastname) > 20)
            $form_state->setErrorByName('payment_details][cardholder][cardholder-last-name', 'Last Name too long, maximum length 20 characters');
          if ($billingAddress != null) {
            if (array_key_exists('address_line1', $billingAddress) && strlen($billingAddress['address_line1']) > 50)
              $form_state->setErrorByName('billing_information][address][widget][0][address][address_line1', 'Street address line 1 too long, maximum length 50 characters');
            if (array_key_exists('address_line2', $billingAddress) && strlen($billingAddress['address_line2']) > 50)
              $form_state->setErrorByName('billing_information][address][widget][0][address][address_line2', 'Street address line 2 too long, maximum length 50 characters');
            if (array_key_exists('locality', $billingAddress) && strlen($billingAddress['locality']) > 40)
              $form_state->setErrorByName('billing_information][address][widget][0][address][locality', 'Post town too long, maximum length 40 characters');
            if (array_key_exists('postal_code', $billingAddress) && strlen($billingAddress['postal_code']) > 10)
              $form_state->setErrorByName('billing_information][address][widget][0][address][postal_code', 'Postal code too long, maximum length 10 characters');
          }
        }

        if ($card_identifier_result != null && $card_identifier_result != 'error') {
          // Only validate the telephone number once there's a proper submit that has a 'card identitifier'
          $this->validateTelephoneNumber($form, $form_state, $gateway_config);
        }
        else if ($card_identifier_result == null) {
          $this->noCardTokenisationAttempt($form_state);
        }

        // Update the form_state to add information about any card tokenisation
        $tokenisation_result = $card_identifier_result != null ? $card_identifier_result : 'no-tokenisation';
        $form_state->setValue('tokenisation-result', $tokenisation_result);

        $this->logVerboseDebug('PaymentMethodAddForm::validateConfigurationForm finished, nr validation errors detected: ' . count($form_state->getErrors()) . ", form build id: " . (array_key_exists('form_build_id', $form_user_input) ? $form_user_input['form_build_id'] : ''));
      }
    }
    else {
      $this->logVerboseDebug('PaymentMethodAddForm::validateConfigurationForm - do nothing, form not submitted');
    }
  }

  protected function noCardTokenisationAttempt(FormStateInterface $form_state) {
    // there was no attempt at card tokenisation
  }


  protected function validateTelephoneNumber(array &$form, FormStateInterface $form_state, array $gateway_config) {

    $telephone_source = $gateway_config['telephone_source'];
    $telephone_number = '';

    if ($telephone_source == 'phone_international') {
      $value = $form_state->getValue(['payment_information', 'add_payment_method', 'payment_details', 'billing-telephone-number']);
      if (isset($value) && count($value) > 0 && array_key_exists('value', $value[0]) && strlen($value[0]['value']) > 0) {
        $telephone_number = $value[0]['value'];
      } else {
        // Check the raw form input
        $user_input = $form_state->getUserInput();
        $payment_details = $this->getPaymentDetails($user_input);
        if ($payment_details != null && array_key_exists('billing-telephone-number', $payment_details)) {
          $value = $payment_details['billing-telephone-number'];
          if (isset($value) && count($value) > 0 && array_key_exists('value', $value[0])) {
            if (is_array($value[0]) && array_key_exists('value', $value[0])) {
              foreach ($value[0]['value'] as $key => $val) {
                if (is_string($val) && strlen($val) > 0 && $key == 'full_number')
                  // assume field has the phone number value
                  $telephone_number = \Drupal\Component\Utility\Html::escape($val);
              }
            } elseif (is_string($value[0]))
              $telephone_number = \Drupal\Component\Utility\Html::escape($value[0]);
          }
        }
      }
    }
    else if ($telephone_source == 'text') {
      // Get from the form state
      $telephone_number = $form_state->getValue($form["payment_details"]["billing-telephone-number"]['#parents']);
    }
    else {
      // Expect that a field of type 'phone-international' has been added to the 'Customer' profile

      try {
        $telephone_field_name = $gateway_config['telephone_field_name'];

        $error_field_name = null;
        if (isset($telephone_field_name)) {
          // Check for 'copy shipping address to billing address' (which includes the telephone number)
          $copy_shipping = false;
          if (array_key_exists('copy_fields', $form['billing_information'])) {
            $copy_fields_value = $form_state->getValue($form['billing_information']['copy_fields']['#parents']);
            if ($copy_fields_value != null && array_key_exists('enable', $copy_fields_value)) {
              $enable_val = $copy_fields_value['enable'];
              if ($enable_val != null && is_int($enable_val) && $enable_val > 0) {
                $copy_shipping = true;
              }
            }
          }

          if ($copy_shipping) {
            // Shipping address is billing address
            $values = $form_state->getValues();
            if ($values != null && array_key_exists('shipping_information', $values) && array_key_exists('shipping_profile', $values['shipping_information']) && array_key_exists($telephone_field_name, $values['shipping_information']['shipping_profile'])) {
              $value = $values['shipping_information']['shipping_profile'][$telephone_field_name];
              $error_field_name = 'shipping_information[shipping_profile][' . $telephone_field_name;
              if (isset($value) && count($value) > 0 && array_key_exists('value', $value[0]) && strlen($value[0]['value']) > 0) {
                $telephone_number = $value[0]['value'];
              }
              else {
                // Check the raw form input
                $values = $form_state->getUserInput();
                if ($values != null && array_key_exists('shipping_information', $values) && array_key_exists('shipping_profile', $values['shipping_information']) && array_key_exists($telephone_field_name, $values['shipping_information']['shipping_profile'])) {
                  $value = $values['shipping_information']['shipping_profile'][$telephone_field_name];
                  if (isset($value) && count($value) > 0 && array_key_exists('value', $value[0])) {
                    if (is_array($value[0]) && array_key_exists('value', $value[0])) {
                      foreach($value[0]['value'] as $key => $val) {
                        if (is_string($val) && strlen($val) > 0)
                          // assume field has the phone number value
                          $telephone_number = \Drupal\Component\Utility\Html::escape($val);
                      }
                    }
                    elseif (is_string($value[0]))
                      $telephone_number = \Drupal\Component\Utility\Html::escape($value[0]);
                  }
                }
              }
            }
          }
          elseif ((!$copy_shipping) && array_key_exists('billing_information', $form) && array_key_exists($telephone_field_name, $form['billing_information'])) {
            $error_field_name = 'billing_information][' . $telephone_field_name;
            $value = $form_state->getValue($form["billing_information"][$telephone_field_name]['widget']['#parents']);
            if (isset($value) && count($value) > 0 && array_key_exists('value', $value[0]) && strlen($value[0]['value']) > 0) {
              $telephone_number = $value[0]['value'];
            }
            else {
              // Check the raw form input
              $values = $form_state->getUserInput();
              if ($values != null && array_key_exists('billing_information', $values) && array_key_exists($telephone_field_name, $values['billing_information'])) {
                $value = $values['billing_information'][$telephone_field_name];
                if (isset($value) && count($value) > 0 && array_key_exists('value', $value[0])) {
                  if (is_array($value[0]) && array_key_exists('value', $value[0])) {
                    foreach ($value[0]['value'] as $key => $val) {
                      if (is_string($val) && strlen($val) > 0)
                        // assume field has the phone number value
                        $telephone_number = \Drupal\Component\Utility\Html::escape($val);
                    }
                  } elseif (is_string($value[0]))
                    $telephone_number = \Drupal\Component\Utility\Html::escape($value[0]);
                }
              }
            }
          }

          if (strlen($telephone_number) == 0) {
            if ($copy_shipping) {
              // Check whether we're using a billing profile
              /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
              $payment_method = $this->entity;
              $billing_profile = $payment_method->getBillingProfile();
              if (isset($billing_profile)) {
                /** @var \Drupal\Core\Field\FieldItemBase $tel_no_item */
                $tel_no_item = $billing_profile->get($telephone_field_name)->first();
                if ($tel_no_item != null) {
                  $tel_no_parts = $tel_no_item->toArray();
                  $telephone_number = $tel_no_parts['value'];
                }
              }
              else
                $this->logVerboseDebug('PaymentMethodAddForm::validateTelephoneNumber no shipping telephone number detected in any form field');
            }
            else {
              // Check whether we're using a billing profile
              /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
              $payment_method = $this->entity;
              $billing_profile = $this->getSelectedBillingProfile($payment_method, $form_state, $form);
              if (isset($billing_profile)) {
                /** @var \Drupal\Core\Field\FieldItemBase $tel_no_item */
                $tel_no_item = $billing_profile->get($telephone_field_name)->first();
                if ($tel_no_item != null) {
                  $tel_no_parts = $tel_no_item->toArray();
                  $telephone_number = $tel_no_parts['value'];
                }
              }
              else
                $this->logVerboseDebug('PaymentMethodAddForm::validateTelephoneNumber - no billing telephone number detected in any form field');
            }
          }
        }
      }
      catch (Exception $e) {}
    }

    if ($telephone_number == '' || (substr($telephone_number, 0, 1) != '+') || strlen($telephone_number) < 5) {
      // Not a valid international telephone number
      if ($error_field_name != null) {
        // Display error message for the telephone number field
        $form_state->setErrorByName($error_field_name, 'You need to supply a telephone number to help authentication and verification.');
      } else {
        // Generic error message
        $form_state->setErrorByName('billing_information', 'You need to supply a telephone number (in international format) to help authentication and verification.');
      }

      // Temporary debug purposes, log the telephone number supplied by the customer
      if ($telephone_number != '') {
        $this->log(LogLevel::DEBUG, 'Customer supplied invalid telephone number: ' . $telephone_number);
      } else {
        // No telephone number picked up from the 'form_state' - dump the form_state to the log
        $this->log(LogLevel::DEBUG, 'No telephone number found in form input: ' . print_r($form_state->getUserInput(), TRUE));
      }
    }
    else {
      // Length check
      if (strlen($telephone_number) > 19) {
        // Not a valid international telephone number
        if ($error_field_name != null) {
          // Display error message for the telephone number field
          $form_state->setErrorByName($error_field_name, 'Telephone Number too long, maximum length 19 characters');
        } else {
          // Generic error message
          $form_state->setErrorByName('billing_information', 'Telephone Number too long, maximum length 19 characters');
        }
      }
      else {
        // Found telephone number in international format, add to 'form_state'
        $form_state->setValue('billing-telephone-number', $telephone_number);
      }
    }
  }


  /**
   * Returns the billing profile selected by the customer
   *
   * @return \Drupal\profile\Entity\ProfileInterface
   *  The billing profile (if any) selected by the customer

   */
  protected function getSelectedBillingProfile(PaymentMethodInterface $payment_method, FormStateInterface $form_state, array $form) {
    $billing_profile = $payment_method->getBillingProfile();
    if (!isset($billing_profile)) {
      // Not attached to the 'payment_method' yet

      // Look for the billing profile's selected address
      if ($form != null) {
        if (array_key_exists('billing_information', $form) && array_key_exists('select_address', $form['billing_information'])) {
          $profile_id = $form_state->getValue($form['billing_information']['select_address']['#parents']);
          try {
            $billing_profile = $this->entityTypeManager->getStorage('profile')->load($profile_id);
          } catch (Exception $e) {
          }
        }
      }
    }
    return $billing_profile;
  }


  protected function processPaymentWarningMessage(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $pgplugin */
    $pgplugin = $this->plugin;
    $order = $pgplugin->getOrder();

    if (isset($order)) {
      $key = 'order-id-' . $order->id() . '-payment-error-message';
      $session = \Drupal::request()->getSession();
      $message = $session->get($key);

      if (isset($message)) {
        \Drupal::messenger()->addWarning($message);
        $session->remove($key);
      }
      else {
        // Message may be stored with failed Opayo Transaction info
        $query = $this->entityTypeManager->getStorage('opayo_transaction')->getQuery();
        $query->accessCheck(FALSE);
        $query->condition('order', $order->id());
        $query->sort('received', 'DESC');
        $ids = $query->execute();
        if (count($ids) >= 1) {
          // The most recent one if there are several
          /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction */
          $opayo_transaction = $this->entityTypeManager->getStorage('opayo_transaction')->load(array_values($ids)[0]);

          $message = $opayo_transaction->getUserWarningMessage();
          if (isset($message) && strlen($message) > 0) {
            // Display the message one-time
            \Drupal::messenger()->addWarning($message);
            $opayo_transaction->setUserWarningMessage('');
            $opayo_transaction->save();
          }
        }
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $pgplugin */
    $pgplugin = $this->plugin;

    $form_user_input = $form_state->getUserInput();
      if ($payment_method != null && $form_user_input != null)
        $this->logVerboseDebug('PaymentMethodAddForm::submitConfigurationForm - payment method bundle: ' . $payment_method->bundle() . ", form id: " . (array_key_exists('form_id', $form_user_input) ? $form_user_input['form_id'] : '') . ", form build id: " . (array_key_exists('form_build_id', $form_user_input) ? $form_user_input['form_build_id'] : '') . ", form token: " . (array_key_exists('form_token', $form_user_input) ? $form_user_input['form_token'] : ''));
      else
        $this->logVerboseDebug('PaymentMethodAddForm::submitConfigurationForm');

    PaymentMethodFormBase::submitConfigurationForm($form, $form_state);


    $card_identifier = null;
    $extra_info = null;

    if ($payment_method->bundle() == 'credit_card_opayo') {
      $gateway_config = $pgplugin->getConfiguration();
      if ($gateway_config['form_type'] == 'dropin') {
        // Use drop-in version of the form
        $card_identifier = $this->submitOpayoPiDropinForm($form['payment_details'], $form_state);
      }
      else {
        // 'Own Form' version
        $extra_info = new stdClass();
        $card_identifier = $this->submitOpayoPiOwnForm($form['payment_details'], $form_state, $extra_info);

        // Allow extra info to be tagged onto the payment method
        $this->moduleHandler->alter('commerce_opayo_pi_payment_method_extra_info', $form, $form_state, $extra_info);
      }
    }

    if ($form_state->hasValue('billing-telephone-number')) {
      // Store the billing telephone number with the 'extra_info' settings
      if ($extra_info == null)
        $extra_info = new stdClass();
      $extra_info->billingTelephoneNumber = $form_state->getValue('billing-telephone-number');
    }

    if ($card_identifier != null) {
      // Received a valid card identifier

      // Submitted checkout form values
      $form_values = $form_state->getValue($form['#parents']);

      // Add the order id
      $order = $pgplugin->getOrder();
      if ($order != null)
        $form_values['payment_details']['order-id'] = $order->id();

      if ($gateway_config['form_type'] == 'ownform') {
        // Card type and last four digits supplied by javascript
        $user_input = $form_state->getUserInput();
        try {
          if (array_key_exists('card-type', $user_input))
            $form_values['payment_details']['card-type'] = \Drupal\Component\Utility\Html::escape($user_input['card-type']);
          if (array_key_exists('card-last-four-digits', $user_input))
            $form_values['payment_details']['card-last-four-digits'] = \Drupal\Component\Utility\Html::escape($user_input['card-last-four-digits']);
          if ($extra_info != null)
            // Pass the 'extra info' values with the payment details for the 'createPaymentMethod'
            $form_values['payment_details']['extra-info'] = $extra_info;
        } catch (Exception $e) {
        }
      }
      else {
        // Dropin form - add merchant session key info to the payment details before calling the plugin
        $user_input = $form_state->getUserInput();
        $payment_details = $this->getPaymentDetails($user_input);
        if ($payment_details != null) {
          $form_values['payment_details']['merchant-session-key'] = \Drupal\Component\Utility\Html::escape($payment_details["merchant-session-key"]);
          $form_values['payment_details']['merchant-session-key-sequence-nr'] = \Drupal\Component\Utility\Html::escape($payment_details["merchant-session-key-sequence-nr"]);
          $form_values['payment_details']['merchant-session-key-expiry'] = \Drupal\Component\Utility\Html::escape($payment_details["merchant-session-key-expiry"]);
        }
        else {
          $this->log(LogLevel::ERROR, "PaymentMethodAddForm::submitConfigurationForm: could not find payment details in the form fields");
          throw new PaymentGatewayException(t('We encountered an unexpected error processing your payment method. Please try again later.'));
        }
      }

      // The payment method form is customer facing. For security reasons
      // the returned errors need to be more generic.
      try {
        $pgplugin->createPaymentMethod($payment_method, $form_values['payment_details']);
      }
      catch (DeclineException $e) {
        $this->log(LogLevel::WARNING, $e->getMessage());
        throw new DeclineException(t('We encountered an error processing your payment method. Please verify your details and try again.'));
      }
      catch (PaymentGatewayException $e) {
        $this->log(LogLevel::ERROR, $e->getMessage());
        throw new PaymentGatewayException(t('We encountered an unexpected error processing your payment method. Please try again later.'));
      }
    }
    else {
      $this->log(LogLevel::ERROR, 'PaymentMethodAddForm::submitConfigurationForm - no card identifier returned');
      throw new PaymentGatewayException(t('We encountered an unexpected error processing your payment method. Please try again later.'));
    }
  }


  /**
   * Builds the credit card form.
   *
   * @param array $element
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built credit card form.
   */
  protected function buildOpayoPiDropinForm(array $element, FormStateInterface $form_state) {

    /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $pgplugin  */
    $pgplugin = $this->plugin;
    $order = $pgplugin->getOrder();
    $configuration = $pgplugin->getConfiguration();

    // Merchant session key needs to be presupplied

    /** @var StdClass $merchant_session_key  */
    $merchant_session_key = $order != null ? $pgplugin->getNewMerchantSessionKey($order->id()): null;

    if ($merchant_session_key != null) {
      // How many seconds until expiry
      $expiryinsecs = (new DateTime($merchant_session_key->expiry))->getTimestamp() - (new DateTime('now'))->getTimestamp();

      $computed_settings = [
        'merchantSessionKey' => $merchant_session_key->merchantSessionKey,
        'merchantSessionKeyExpirySecs' => $expiryinsecs,
        'merchantSessionKeyExpiry' => $merchant_session_key->expiry,
        'merchantSessionKeySequenceNr' => $merchant_session_key->sequenceNr,
        'submitButtonSelector' => $this->getSubmitButtonQuerySelector(),
        'formQuerySelector' => $this->getFormQuerySelector(),
        'telephoneSource' => $configuration['telephone_source'],
        'paymentGateway' => $pgplugin->getPaymentGateway()->id(),
      ];
      if ($order != null && $order->get('payment_gateway') != null && $order->get('payment_gateway')->entity != null) {
        $computed_settings['orderId'] = $order->id();
      }

      // Pass the settings including the merchant session to the Javascript
      $element['#attached']['drupalSettings']['commerceopayopi']['opayoCheckout'] = $computed_settings;
    }

    // Javascript that calls Opayo's 'sagepayCheckout' function through Drupal.behaviors
    $element['#attached']['library'][] = 'commerce_opayo_pi/opayo_pi_dropinform';

    $gateway_config = $pgplugin->getConfiguration();
    $opayo_js_ref = null;
    if($gateway_config['mode'] == 'test')
      $opayo_js_ref = 'commerce_opayo_pi/opayo_sagepay_test';
    else
       $opayo_js_ref = 'commerce_opayo_pi/opayo_sagepay_live';
    // Attach the external Opayo provided javascript
    $element['#attached']['library'][] = $opayo_js_ref;

    // Feedback about expiry of the card fields
    $element['expiry-msg'] = [
      '#type' => 'markup',
      '#markup' => '<div id="expiry-msg" class="commerce-opayo-pi-expiry" style="font-size:smaller"></div>',
    ];

    // Hidden elements with the merchant session key info
    $element['merchant-session-key'] = [
      '#type' => 'hidden',
      '#value' => $merchant_session_key == null ? '' : $merchant_session_key->merchantSessionKey,
      '#attributes' => [
        'id' => ['merchant-session-key'],
      ],
    ];
    $element['merchant-session-key-sequence-nr'] = [
      '#type' => 'hidden',
      '#value' => $merchant_session_key == null ? '' : $merchant_session_key->sequenceNr,
      '#attributes' => [
        'id' => ['merchant-session-key-sequence-nr'],
      ],
    ];
    $element['merchant-session-key-expiry'] = [
      '#type' => 'hidden',
      '#value' => $merchant_session_key == null ? '' : $merchant_session_key->expiry,
      '#attributes' => [
        'id' => ['merchant-session-key-expiry'],
      ],
    ];

    // HTML element that will contain the iFrame for Opayo's 'Drop-In' elements
    $element['sp-container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => ['sp-container'],
      ],
    ];

    // Optional field for telephone number, depending on the configuration setting
    $telephone_source = $gateway_config['telephone_source'];

    if ($telephone_source == 'phone_international') {

      // Creathe a 'phone_international' type field with widget
      $field_definition = BaseFieldDefinition::create('phone_international')
      ->setName('billing-telephone-number')
      ->setRequired(TRUE)
        ->setLabel('Telephone Number')
        ->setTargetEntityTypeId('dummy');

      // Create a new field item list.
      $items = new FieldItemList($field_definition, 'phone_international');

      /** @var \Drupal\Core\Field\WidgetPluginManager $widget_plugin_manager */
      $widget_plugin_manager = \Drupal::service('plugin.manager.field.widget');

      // Build the widget form element.
      $widget = $widget_plugin_manager->getInstance([
        'field_definition' => $field_definition,
        'configuration' => [
          // Widget ID.
          'type' => 'phone_international_widget',
          // Widget settings.
          'settings' => [
            'show_label' => TRUE,
            'countries' => 'exclude',
            'exclude_countries' => [],
            'geolocation' => TRUE,
            'initial_country' => 'GB',
            'preferred_countries' => ['GB'],
          ],
        ],
      ]);
      $element['telephone'] = $widget->form($items, $element, $form_state);
    } elseif ($telephone_source == 'text') {
      $element['billing-telephone-number'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Telephone number'),
        '#description' => 'Telephone number is required for card payments. Please input in international format (e.g. +44 for UK)',
        '#required' => TRUE,
        '#maxlength' => 19,
        '#size' => 25,
      ];
    }

    return $element;
  }


  /**
   * Validates the credit card form.
   *
   * @param array $element
   *   The credit card form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return string
   *  Returns the card identifier if there was a successful card tokenisation, 'error' if unsuccessful and null if there was no card tokenisation attempt
   */
  protected function validateOpayoPiDropinForm(array &$element, FormStateInterface $form_state) {

    $card_tokenisation_attempted = false;
    $card_identifier = '';
    $card_identifier_http_code = 0;
    $card_identifier_error_code = 0;
    $card_identifier_error_message = '';

    if (array_key_exists('card-identifier', $form_state->getUserInput())) {
      // Opayo's 'Drop-In' logic should have provided a 'Card Identifier' among the posted fields
      $card_identifier = \Drupal\Component\Utility\Html::escape($form_state->getUserInput()['card-identifier']);
      $card_tokenisation_attempted = true;
    }

    if (!$card_identifier) {
      // Check to see if we had an expired merchant session key

      /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGateway $pgplugin  */
      $pgplugin = $this->plugin;
      $currentMerchantSessionKey = $pgplugin->getCurrentMerchantSessionKey($pgplugin->getOrder() != null ? $pgplugin->getOrder()->id() : '');
      if ($currentMerchantSessionKey != null)
      {
        if (new DateTime('now') >= DateTime::createFromFormat('U', $currentMerchantSessionKey->expiry))
          $form_state->setErrorByName('sp-container', $this->t('Your card details could not be validated.'));
      }

      // Check for any error fields in the POST that were supplied by the Opayo javascript
      if (array_key_exists('card-identifier-http-code', $form_state->getUserInput()))
        $card_identifier_http_code = \Drupal\Component\Utility\Html::escape($form_state->getUserInput()['card-identifier-http-code']);
      if (array_key_exists('card-identifier-error-code', $form_state->getUserInput()))
        $card_identifier_error_code = \Drupal\Component\Utility\Html::escape($form_state->getUserInput()['card-identifier-error-code']);
      if (array_key_exists('card-identifier-error-message', $form_state->getUserInput()))
        $card_identifier_error_message = \Drupal\Component\Utility\Html::escape($form_state->getUserInput()['card-identifier-error-message']);

      if (isset($card_identifier_http_code) && isset($card_identifier_error_code) && isset($card_identifier_error_message))
      {
        // TODO: refine
        $form_state->setErrorByName('sp-container', 'HTTP Code: ' . $card_identifier_http_code . ', Error code: ' . $card_identifier_error_code . ', Error message: ' . $card_identifier_error_message);
        $card_tokenisation_attempted = true;
      }

      if ($card_tokenisation_attempted) {
        // Invalidate the current merchant session key
        /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGateway $pgplugin  */
        $pgplugin = $this->plugin;
        $pgplugin->invalidateCurrentMerchantSessionKey($pgplugin->getOrder() != null ? $pgplugin->getOrder()->id() : '');
      }
    }

    // Do not call the parent method (it would try to validate card details that are handled by the drop-in form instead)

    if ($card_tokenisation_attempted) {
      if (isset($card_identifier))
        return $card_identifier;
      else
        return 'error';
    }
    else
      return null;
  }

  /**
   * Handles the submission of the credit card form.
   *
   * @param array $element
   *   The credit card form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * * @return string
   *  the card identifier
   */
  protected function submitOpayoPiDropinForm(array $element, FormStateInterface &$form_state) {
    // Opayo's 'Drop-In' logic should have provided a 'Card Identifier' among the posted fields
    $card_identifier = \Drupal\Component\Utility\Html::escape($form_state->getUserInput()['card-identifier']);

    // Update the form_state to contain the payment details, in this case the only payment detail is the 'card_identifier' token
    $payment_details = [ 'card-identifier' => $card_identifier ];
    $form_state->setValueForElement($element, $payment_details);

    // Current merchant session key has been used succesfully  with the card details to generate a 'card identifier'. It cannot be
    // used again, invalidate it
    /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGateway $pgplugin  */
    $pgplugin = $this->plugin;
    $pgplugin->invalidateCurrentMerchantSessionKey($pgplugin->getOrder() != null ? $pgplugin->getOrder()->id() : '');

    return $card_identifier;
  }

  /**
   * Builds the credit card form.
   *
   * @param array $element
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built credit card form.
   */
  protected function buildOpayoPiOwnForm(array $element, FormStateInterface $form_state) {

    // Credit card fields
    $element = $this->buildCreditCardForm($element, $form_state);

    /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $pgplugin  */
    $pgplugin = $this->plugin;
    $order = $pgplugin->getOrder();
    $configuration = $pgplugin->getConfiguration();
    $mode = $configuration['mode'];

    // Settings needed by javascript - including info passed back using Ajax calls
    $computed_settings = [
      'submitButtonSelector' => $this->getSubmitButtonQuerySelector(),
      'formQuerySelector' => $this->getFormQuerySelector(),
      'paymentGateway' => $pgplugin->getPaymentGateway()->id(),
      'opayoPiPaymentGatewayChoiceSelector' => $this->getOpayoPiPaymentGatewayChoiceQuerySelector(),
      'paymentMethodWrapper' => $this->getOpayoPiPaymentMethodWrapper(),
      'logLevel' => $configuration['gateway_log_level'],
      'telephoneSource' => $configuration['telephone_source'],
      'mode' => $mode,
    ];
    if ($order != null && $order->get('payment_gateway') != null && $order->get('payment_gateway')->entity != null) {
      $computed_settings['orderId'] = $order->id();
    }
    if ($this->getUseTriggeringElement()) {
      $computed_settings['useTriggeringElement'] = "true";
    }

    // Javascript that calls 'sagepayOwnForm' through Drupal.behaviors
    $element['#attached']['library'][] = 'commerce_opayo_pi/opayo_pi_ownform';
    // Pass the settings including the merchant session to the Javascript
    $element['#attached']['drupalSettings']['commerceopayopi']['opayoCheckout'] = $computed_settings;

    $opayo_js_ref = null;
    if ($mode == 'test')
      $opayo_js_ref = 'commerce_opayo_pi/opayo_sagepay_test';
    else
      $opayo_js_ref = 'commerce_opayo_pi/opayo_sagepay_live';
    // Attach the external Opayo provided javascript
    $element['#attached']['library'][] = $opayo_js_ref;

    $this->logVerboseBuildOpayoOwnForm($configuration);

    // hidden element for the card identifier pass-back
    $element['card-identifier'] = [
      '#type' => 'hidden',
      '#value' => '',
      '#attributes' => [
        'id' => ['card-identifier'],
      ],
    ];
    $element['card-identifier-expiry'] = [
      '#type' => 'hidden',
      '#value' => '',
      '#attributes' => [
        'id' => ['card-identifier-expiry'],
      ],
    ];
    $element['card-identifier-result'] = [
      '#type' => 'hidden',
      '#value' => '',
      '#attributes' => [
        'id' => ['card-identifier-result'],
      ],
      '#required' => TRUE,
    ];
    $element['card-identifier-error-httpcode'] = [
      '#type' => 'hidden',
      '#value' => '',
      '#attributes' => [
        'id' => ['card-identifier-error-httpcode'],
      ],
    ];
    // Hidden elements with the merchant session key info
    $element['merchant-session-key'] = [
      '#type' => 'hidden',
      '#value' => '',
      '#attributes' => [
        'id' => ['merchant-session-key'],
      ],
    ];
    $element['merchant-session-key-sequence-nr'] = [
      '#type' => 'hidden',
      '#value' => 0,
      '#attributes' => [
        'id' => ['merchant-session-key-sequence-nr'],
      ],
    ];
    $element['merchant-session-key-expiry'] = [
      '#type' => 'hidden',
      '#value' => '',
      '#attributes' => [
        'id' => ['merchant-session-key-expiry'],
      ],
    ];

    return $element;
  }


  protected function logVerboseBuildOpayoOwnForm(array $configuration) {
    $this->logVerboseDebug('PaymentMethodAddForm::buildOpayoPiOwnForm - mode: ' . $configuration['mode'] . ', log level: ' . $configuration['gateway_log_level']);
  }


  /**
   * Normally the ID for the button that submits the card details form is 'edit-actions-next'
   *
   * @return string
   */
  protected function getSubmitButtonQuerySelector() {
    return 'input[data-drupal-selector="edit-actions-next"]';
  }

  /**
   * Normally the query selector to identify the checkout form is form[id^="commerce-checkout"]
   *
   * @return string
   */
  protected function getFormQuerySelector() {
    return 'form[id^="commerce-checkout"]';
  }

  /**
   * Should the javascript use triggering element directive for the form submit
   *
   * @return bool
   */
  protected function getUseTriggeringElement() {
    return true;
  }

  /**
   * Element on the page that wraps choice of payment methods/gateways
   */
  protected function getOpayoPiPaymentMethodWrapper() {
    return 'fieldset[data-drupal-selector="edit-payment-information-payment-method"]';
  }

  /**
   * Element on the page that represent a choice of the Opayo Pi payment gateway/method
   */
  protected function getOpayoPiPaymentGatewayChoiceQuerySelector() {
    return 'input.form-radio[name="payment_information[payment_method]"][value^="new--credit_card_opayo"]';
  }

  /**
   * Builds the credit card form.
   *
   * @param array $element
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built credit card form.
   */
  protected function buildCreditCardForm(array $element, FormStateInterface $form_state) {

    /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $pgplugin */
      $pgplugin = $this->plugin;
      $gateway_config = $pgplugin->getConfiguration();

    // Build a month select list that shows months with a leading zero.
    $months = [];
    for ($i = 1; $i < 13; $i++) {
      $month = str_pad($i, 2, '0', STR_PAD_LEFT);
      $months[$month] = $month;
    }
    // Build a year select list that uses a 4 digit key with a 2 digit value.
    $current_year_4 = date('Y');
    $current_year_2 = date('y');
    $years = [];
    for ($i = 0; $i < 10; $i++) {
      $years[$current_year_4 + $i] = $current_year_2 + $i;
    }

    $element['#attributes']['class'][] = 'credit-card-form';
    // Placeholder for the detected card type. Set by validateCreditCardForm().
    $element['type'] = [
      '#type' => 'hidden',
      '#value' => '',
    ];
    // Put cardholder first and last name with the card details
    $element['cardholder'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['credit-card-form__cardholder'],
      ],
    ];
    $element['cardholder']['cardholder-first-name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First Name'),
      '#attributes' => ['autocomplete' => 'off', 'data-card-details' => 'cardholder-first-name'],
      '#required' => TRUE,
      '#maxlength' => 20,
      '#size' => 20,
    ];
    // Allow the surname to stay blank in 'test' mode so that the first name can be used for Opayo 'Magic CardHolder Values'
    $element['cardholder']['cardholder-last-name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last Name'),
      '#attributes' => ['autocomplete' => 'off', 'data-card-details' => 'cardholder-last-name'],
      '#required' => TRUE,
      '#maxlength' => 20,
      '#size' => 20,
    ];
    $element['cardholder-name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Card Holder'),
      '#attributes' => ['autocomplete' => 'off', 'data-card-details' => 'cardholder-name'],
      '#required' => TRUE,
      '#maxlength' => 40,
      '#size' => 25,
    ];
    $element['number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Card number'),
      '#attributes' => ['autocomplete' =>'off', 'data-card-details' => 'card-number'],
      '#required' => TRUE,
      '#maxlength' => 19,
      '#size' => 25,
    ];
    $element['expiration'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['credit-card-form__expiration'],
      ],
    ];
    $element['expiration']['month'] = [
      '#type' => 'select',
      '#title' => $this->t('Expiry Month'),
      '#attributes' => ['data-card-details' => 'expiry-date-month'],
      '#options' => $months,
      '#default_value' => date('m'),
      '#required' => TRUE,
    ];
    $element['expiration']['year'] = [
      '#type' => 'select',
      '#title' => $this->t('Expiry Year'),
      '#attributes' => ['data-card-details' => 'expiry-date-year'],
      '#options' => $years,
      '#default_value' => $current_year_4,
      '#required' => TRUE,
    ];
    $element['security-code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CVV'),
      '#attributes' => ['autocomplete' => 'off', 'data-card-details' => 'security-code'],
      '#required' => TRUE,
      '#maxlength' => 4,
      '#size' => 4,
    ];

    // Optional field for telephone number, depending on the configuration setting
    $telephone_source = $gateway_config['telephone_source'];

    if ($telephone_source == 'phone_international') {

      // Creathe a 'phone_international' type field with widget
      $field_definition = BaseFieldDefinition::create('phone_international')
        ->setName('billing-telephone-number')
        ->setRequired(TRUE)
        ->setLabel('Telephone Number')
        ->setTargetEntityTypeId('dummy');

      // Create a new field item list.
      $items = new FieldItemList($field_definition, 'phone_international');

      /** @var \Drupal\Core\Field\WidgetPluginManager $widget_plugin_manager */
      $widget_plugin_manager = \Drupal::service('plugin.manager.field.widget');

      // Build the widget form element.
      $widget = $widget_plugin_manager->getInstance([
        'field_definition' => $field_definition,
        'configuration' => [
          // Widget ID.
          'type' => 'phone_international_widget',
          // Widget settings.
          'settings' => [
            'show_label' => TRUE,
            'countries' => 'exclude',
            'exclude_countries' => [],
            'geolocation' => TRUE,
            'initial_country' => 'GB',
            'preferred_countries' => ['GB'],
          ],
        ],
      ]);
      $element['telephone'] = $widget->form($items, $element, $form_state);
    }
    elseif ($telephone_source == 'text') {
      $element['billing-telephone-number'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Telephone number'),
        '#description' => 'Telephone number is required for card payments. Please input in international format (e.g. +44 for UK)',
        '#required' => TRUE,
        '#maxlength' => 19,
        '#size' => 25,
      ];
    }

    return $element;
  }

  /**
   * Validates the credit card form when using 'Own Form' appreach.
   *
   * @param array $element
   *   The credit card form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return string
   *  Returns the card identifier if there was a successful card tokenisation, 'error' if unsuccessful and null if there was no card tokenisation attempt

   */
  protected function validateOpayoPiOwnForm(array &$element, FormStateInterface $form_state) {

    $user_input = $form_state->getUserInput();
    $payment_details = $this->getPaymentDetails($user_input);
    // Was there a card tokenisation attempt?
    $card_tokenisation_attempted = $payment_details != null && array_key_exists('card-identifier-result', $payment_details) && strlen($payment_details["card-identifier-result"]) > 0;
    $card_identifier = null;

    $this->logVerboseDebug('PaymentMethodAddForm::validateOpayoPiOwnForm - card tokenisation attempted: ' . $card_tokenisation_attempted . ' (form build id: ' . $user_input['form_build_id'] . ')');

    if ($card_tokenisation_attempted)
    {
      // Checks needed when payment details have been submitted
      $card_identifier_http_error_code = null;
      $card_identifier_errors = array();

      // The values, including 'payment_details', that are added to the form by the javascript are not found in the $form_state's 'values' but
      // can be retrieved from its 'input' field.

      $payment_details_prefix = $this->getPaymentDetailsPrefix($user_input);

      try {
        $result = \Drupal\Component\Utility\Html::escape($payment_details["card-identifier-result"]);

        $this->logVerboseDebug('PaymentMethodAddForm::validateOpayoPiOwnForm - card tokenisation result: ' .$result . ' (form build id: ' . $user_input['form_build_id'] . ')');

        if ($result == "true") {
          $card_identifier = \Drupal\Component\Utility\Html::escape($payment_details["card-identifier"]);
          $this->logVerboseDebug('PaymentMethodAddForm::validateOpayoPiOwnForm - card identifier: ' . $card_identifier . ' (form build id: ' . $user_input['form_build_id'] . ')');
        }
        else {
          $card_identifier_http_error_code = \Drupal\Component\Utility\Html::escape($payment_details["card-identifier-error-httpcode"]);
          $this->logVerboseDebug('PaymentMethodAddForm::validateOpayoPiOwnForm - HTTP error code: ' . $card_identifier_http_error_code . ' (form build id: ' . $user_input['form_build_id'] . ')');

          $index = 0;
          if (array_key_exists("card-identifier-error-" . $index . "-code", $user_input))
          {
            $errorDetails = new stdClass();
            $errorDetails->code = $user_input["card-identifier-error-" . $index . "-code"];
            $errorDetails->message = $user_input["card-identifier-error-" . $index . "-message"];
            array_push($card_identifier_errors, $errorDetails);
            $this->logVerboseDebug('PaymentMethodAddForm::validateOpayoPiOwnForm - error detail ' . $index . ': code ' . $errorDetails->code . ', message ' . $errorDetails->message . ' (form build id: ' . $user_input['form_build_id'] . ')');
            $index++;
          }
        }
      }
      catch (Exception $e) {
      }

      if ($form_state->hasAnyErrors()) {
        // Some error messages are the result of the fact that the credit card fields are on the form but their values are tokenised using javascript
        // and not returned here. Filter those out.
        $this->logVerboseDebug('PaymentMethodAddForm::validateOpayoPiOwnForm - filter errors ' . ' (form build id: ' . $user_input['form_build_id'] . ')');

        $form_errors = $form_state->getErrors();
        $filtered_form_errors = array();
        /** @var Drupal\Core\StringTranslation\TranslatableMarkup $errorElement */
        foreach ($form_errors as $errorElement => $errorObject) {
          $ignore = (strval($errorElement) == $payment_details_prefix . '][cardholder][cardholder-first-name' ||
            strval($errorElement) == $payment_details_prefix . '][cardholder][cardholder-last-name' ||
            strval($errorElement) == $payment_details_prefix . '][cardholder-name' ||
            strval($errorElement) == $payment_details_prefix . '][number' ||
            strval($errorElement) == $payment_details_prefix . '][security-code');
          if (!$ignore) {
            $filtered_form_errors[$errorElement] = $errorObject;
          }
        }
        if (sizeof($filtered_form_errors) < sizeof($form_errors)) {
          // Some errors have been filtered out - reset
          $form_state->clearErrors();
          foreach ($filtered_form_errors as $errorElement => $errorObject) {
            $form_state->setErrorByName($errorElement, $errorObject);
            $this->logVerboseDebug('PaymentMethodAddForm::validateOpayoPiOwnForm - validation error detected ' . $errorElement . ' (form build id: ' . $user_input['form_build_id'] . ')');
          }
        }
      }

      if ($card_identifier == null) {
        // No card-identifier returned with the form

        $this->logVerboseDebug('PaymentMethodAddForm::validateOpayoPiOwnForm - no card identifier returned, http error code: ' . $card_identifier_http_error_code . ' (form build id: ' . $user_input['form_build_id'] . ')');

        $date_utc = new \DateTime("now", new \DateTimeZone("UTC"));
        if ($card_identifier_http_error_code != null) {
          $errorFeedback = false;
          if ($card_identifier_http_error_code == 422)
          {
            $cardholder_name_warning = '';
            $card_number_warning = '';
            $card_expiry_warning = '';
            $card_cvv_warning = '';
            $token_problem = '';

            $feedback = '';

            foreach($card_identifier_errors as $errorDetails) {
              $feedback = $feedback . 'code: ' . $errorDetails->code . 'message: ' . $errorDetails->message;
              if (str_contains(strtolower($errorDetails->message), 'card number')) {
                $card_number_warning .= (strlen($card_number_warning) > 0 ? ', ' : '') . $errorDetails->message;
              }
              else if (str_contains(strtolower($errorDetails->message), 'cardholder name')) {
                $cardholder_name_warning .= (strlen($cardholder_name_warning) > 0 ? ', ' : '') . $errorDetails->message;
              }
              else if (str_contains(strtolower($errorDetails->message), 'security code')) {
                $card_cvv_warning .= (strlen($card_cvv_warning) > 0 ? ', ' : '') . $errorDetails->message;
              }
              else if ($errorDetails->code == '1006' || $errorDetails->code == '1011')
                $token_problem .= (strlen($token_problem) > 0 ? ', ' : '') . $errorDetails->code;
              else if ($errorDetails->code == '1008')
                $card_number_warning .= (strlen($card_number_warning) > 0 ? ', ' : '') . $errorDetails->message;
            }

            if ($card_number_warning != '')
              $form_state->setErrorByName($payment_details_prefix . '][number', $card_number_warning);
            if ($cardholder_name_warning != '')
              $form_state->setErrorByName($payment_details_prefix . '][cardholder-name', $cardholder_name_warning);
            if ($card_cvv_warning != '')
              $form_state->setErrorByName($payment_details_prefix . '][security-code', $card_cvv_warning);
            if ($card_expiry_warning != '')
              $form_state->setErrorByName($payment_details_prefix . '][expiration', $card_expiry_warning);
            if ($token_problem)
              $form_state->setErrorByName($payment_details_prefix . '][number', $token_problem);

            if ($card_number_warning != '' || $cardholder_name_warning != '' || $card_cvv_warning != '' || $card_expiry_warning != '' || $token_problem != '') {
              $errorFeedback = true;
              $this->logVerboseDebug('PaymentMethodAddForm::validateOpayoPiOwnForm - detected validation error feedback ' . ' (form build id: ' . $user_input['form_build_id'] . ')');
            }
            else {
              $this->logVerboseDebug('PaymentMethodAddForm::validateOpayoPiOwnForm - unexpected validation error feedback: ' . $feedback . ' (form build id: ' . $user_input['form_build_id'] . ')');
            }
          }
          else
          {
            $card_number_warning = '';
            $token_problem = '';
            foreach($card_identifier_errors as $errorDetails) {
              if ($errorDetails->code == '1000' || $errorDetails->code == '1001' || $errorDetails->code == '1002')
                $card_number_warning .= (strlen($card_number_warning) > 0 ? ', ' : '') . $errorDetails->message;
              else {
                $token_problem .= (strlen($token_problem) > 0 ? ', ' : '') . $errorDetails->code;
              }
            }

            if ($card_number_warning != '')
              $form_state->setErrorByName($payment_details_prefix . '][number', $card_number_warning);
            if ($token_problem)
              $form_state->setErrorByName($payment_details_prefix . '][number', $token_problem);

            if ($card_number_warning != '' || $token_problem != '') {
              $errorFeedback = true;
            }
            $this->logVerboseDebug('PaymentMethodAddForm::validateOpayoPiOwnForm - no card identifier returned, http error code: ' . $card_identifier_http_error_code . ', card number warning: ' . $card_number_warning . ', token problem: ' . $token_problem . ' (form build id: ' . $user_input['form_build_id'] . ')');
          }


          if ($errorFeedback == false) {
            // Havent't got detailed feedback about issue, instead put out a generic message with the error codes
            $errorCodes = '';
            foreach ($card_identifier_errors as $errorDetails) {
              $errorCodes .= (strlen($errorCodes) > 0 ? ', ' : '') . $errorDetails->code;
            }
            $this->logVerboseDebug('PaymentMethodAddForm::validateOpayoPiOwnForm - no card identifier returned, http error code: ' . $card_identifier_http_error_code . ', error codes: ' . $errorCodes . ' (form build id: ' . $user_input['form_build_id'] . ')');
            $form_state->setErrorByName($payment_details_prefix . '][number', $this->t('Technical error validating your card details (HTTP code: ' . $card_identifier_http_error_code . ' , Error codes: ' . $errorCodes . ' at ' . $date_utc->format(\DateTime::RFC850) . ')'));
          }
        }
        else {
          // No card identifier but also no feedback from tokenisation
          $this->logVerboseDebug('PaymentMethodAddForm::validateOpayoPiOwnForm - no card identifier returned and no http error code: (form build id: ' . $user_input['form_build_id'] . ')');
          $form_state->setErrorByName($payment_details_prefix . '][number', $this->t('Technical error validating your card details (' . $date_utc->format(\DateTime::RFC850) . ')'));
        }
      }

      // Do not call the parent's 'validateCreditCardForm' method (it would try to validate card details but these are not returned with the form)
    }

    if ($card_tokenisation_attempted) {
      if (isset($card_identifier))
        return $card_identifier;
      else
        return 'error';
    }
    else
      return null;
  }

  /**
   * Returns the form array element holding the payment details
   *
   * @param array $user_input
   *  Form user input
   *
   * @return array
   *   payment details array
   */
  protected function getPaymentDetails($user_input) {
    if (array_key_exists("payment_information", $user_input) && array_key_exists("add_payment_method", $user_input["payment_information"]) && array_key_exists("payment_details", $user_input["payment_information"]["add_payment_method"]))
      return $user_input["payment_information"]["add_payment_method"]["payment_details"];
    else
      return null;
  }

  /**
   * Returns the 'prefix' to use for the form's payment details
   *
   * @param array $user_input
   *  Form user input
   *
   * @return string
   *   Prefix
   */
  protected function getPaymentDetailsPrefix($user_input) {
    return 'payment_information][add_payment_method][payment_details';
  }



  /**
   * Handles the submission of Opayo Pi payment details form.
   *
   * @param array $element
   *   The credit card form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   * @param StdClass $extraInfo
   *   Info supplied by the javascript about the customer's browser
   * @return string
   *  the card identifier
   *
   */
  protected function submitOpayoPiOwnForm(array $element, FormStateInterface &$form_state, &$extraInfo) {

    $card_identifier = null;
    $merchant_session_key = null;

    $user_input = $form_state->getUserInput();
    $this->logVerboseDebug('PaymentMethodAddForm::submitOpayoPiOwnForm called (form build id: ' . $user_input['form_build_id'] . ')');

    // Javascript should have filled in the 'card-identifier' and 'card-identifier-expiry' fields before submitting the form
    // Javascript manipulation of Drupal form fields is outside the scope of '$form_state', need to be copied explicitly
    $user_input = $form_state->getUserInput();
    $payment_details = $this->getPaymentDetails($user_input);
    if ($payment_details != null) {
      try {
        if (array_key_exists('card-identifier', $payment_details))
          $card_identifier = \Drupal\Component\Utility\Html::escape($payment_details["card-identifier"]);
        if (array_key_exists('card-identifier-expiry', $payment_details))
          $card_identifier_expiry = \Drupal\Component\Utility\Html::escape($payment_details["card-identifier-expiry"]);
        if (array_key_exists('merchant-session-key', $payment_details))
          $merchant_session_key = \Drupal\Component\Utility\Html::escape($payment_details["merchant-session-key"]);
        if (array_key_exists('merchant-session-key-expiry', $payment_details))
          $merchant_session_key_expiry = \Drupal\Component\Utility\Html::escape($payment_details["merchant-session-key-expiry"]);
        if (array_key_exists('merchant-session-key-sequence-nr', $payment_details))
          $merchant_session_key_sequence_nr = \Drupal\Component\Utility\Html::escape($payment_details["merchant-session-key-sequence-nr"]);

        if (isset($card_identifier))
          $form_state->setValueForElement($element["card-identifier"], $card_identifier);
        if (isset($card_identifier_expiry))
          $form_state->setValueForElement($element["card-identifier-expiry"], $card_identifier_expiry);
        if (isset($merchant_session_key))
          $form_state->setValueForElement($element["merchant-session-key"], $merchant_session_key);
        if (isset($merchant_session_key_expiry))
          $form_state->setValueForElement($element["merchant-session-key-expiry"], $merchant_session_key_expiry);
        if (isset($merchant_session_key_sequence_nr))
          $form_state->setValueForElement($element["merchant-session-key-sequence-nr"], $merchant_session_key_sequence_nr);
      } catch (Exception $e) {
      }

      // Browser settings collected by javascript to be used for 'strong customer authentication'
      try {
        $extraInfo->colorDepth = 32;
        $extraInfo->screenHeight = 512;
        $extraInfo->screenWidth = 1024;
        $extraInfo->timeZoneOffset = 60;
        $extraInfo->language = 'en-GB';
        if (array_key_exists('browser-color-depth', $user_input))
          $extraInfo->colorDepth = $user_input['browser-color-depth'];
        if (array_key_exists('browser-screen-heighth', $user_input))
          $extraInfo->screenHeight = $user_input['browser-screen-height'];
        if (array_key_exists('browser-screen-width', $user_input))
          $extraInfo->screenWidth = $user_input['browser-screen-width'];
        if (array_key_exists('browser-timezone-offset', $user_input))
          $extraInfo->timeZoneOffset = $user_input['browser-timezone-offset'];
        if (array_key_exists('browser-language', $user_input))
          $extraInfo->language = $user_input['browser-language'];
      } catch (Exception $e) {
      }

      $this->logVerboseDebug('PaymentMethodAddForm::submitOpayoPiOwnForm card identifier: ' . $card_identifier . ' returned for merchant session key: ' . $merchant_session_key . ' (form build id: ' . $user_input['form_build_id'] . ')');

      return $card_identifier;
    }
    else {
      $this->log(LogLevel::ERROR, "PaymentMethodAddForm::submitOpayoPiOwnForm: could not find payment details in the form fields");
      return null;
    }
  }


  public function logVerboseDebug(string|\Stringable $message, array $context = []) {
    $pgplugin = $this->plugin;
    /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $pgplugin */
    $pgplugin->logVerboseDebug($message, $context);
  }

  public function log($level, string|\Stringable $message, array $context = []): void {
    $pgplugin = $this->plugin;
    /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $pgplugin */
    $pgplugin->log($level, $message, $context);
  }

}
