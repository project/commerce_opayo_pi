<?php

namespace Drupal\commerce_opayo_pi;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;

/**
 * Service for the module
 *
 */
class OpayoPi {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;


  /**
   * Module settings
   *
   * @var \Drupal\core\Config\Config $settings
   */
  protected $settings;

  /**
   * Constructs a new Cron object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LoggerChannelInterface $logger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger;
    $this->settings = $config = \Drupal::service('config.factory')->getEditable('commerce_opayo_pi.settings');

    // Add in the URLs for reporting access
    $this->settings->set('testaccess', 'https://sandbox.opayo.eu.elavon.com/access/access.htm');
    $this->settings->set('liveaccess', 'https://live.opayo.eu.elavon.com/access/access.htm');
  }

  /**
   * Get the settings for the module
   *
   * @return array
   *   The settings
   */
  public function getSettings() {
    return $this->settings->getRawData();
  }

}
