<?php

namespace Drupal\commerce_opayo_pi;

use DateTime;
use Drupal\commerce\Interval;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\queue_unique\UniqueQueueDatabaseFactory;
use Exception;
use stdClass;

/**
 * Default cron implementation.
 *
 */
class Cron {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The commerce_cart_expiration queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Queue for operations that only need to happen once a day
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $daily_queue;

    /**
   * Logger
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The payment gateways that use the 'opayo_pi' plugin
   *
   * @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface[] $payment_gateways
   */
   protected $payment_gateways;

     /**
   * The module's service
   *
   * @var \Drupal\commerce_opayo_pi\OpayoPi
   */
  protected $opayoPiService;

  /**
   * Constructs a new Cron object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, QueueFactory $queue_factory, LoggerChannelInterface $logger, UniqueQueueDatabaseFactory $unique_queue_factory, OpayoPi $opayoPi) {
    $this->entityTypeManager = $entityTypeManager;
    $this->queue = $queue_factory->get('commerce_opayo_pi');
    $this->daily_queue = $unique_queue_factory->get('commerce_opayo_pi_daily');
    $this->logger = $logger;
    $this->opayoPiService = $opayoPi;

    /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
    $payment_gateway_storage = $this->entityTypeManager->getStorage('commerce_payment_gateway');
    $all_payment_gateways = $payment_gateway_storage->loadMultiple();
    $this->payment_gateways = array();
    foreach ($all_payment_gateways as $pg_id => $pg) {
      $payment_gateway_plugin = $pg->getPlugin();
      if (is_a($payment_gateway_plugin, 'Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGateway')) {
        $this->payment_gateways[] = $pg;
      }
    }
  }

  // No 'create' method here (not inheriting from anything), arguments passed into __construct defined in 'commerce_opayo_pi.services.yml'

  /**
   * {@inheritdoc}
   */
  public function run() {
    // Ask queue to process opayo transaction delete, processing a maximum of 50 records
    $this->deleteExpiredOpayoTransactions(50);
    // Ask queue to process commerce payment method delete, processing a maximum of 50 records
    foreach($this->payment_gateways as $payment_gateway) {
      $this->deleteExpiredCommercePaymentMethods($payment_gateway->id(), 50);
    }
    // Run the task that queries the Opayo reporting API for bank 'batch' IDs
    $this->queryAndFetchBankBatchIDs();
  }


  /**
   * Delete Opayo Transaction entities from the database that represent customer transaction info for transactions that never completed succesfully.
   * It's probably useful to keep these for a certain length of time to support tracing issues or queries but you might want to delete old ones
   * to keep the table relatively tidy. The payment gateway config sets the behaviour in terms of the age of entries to be deleted.
   * By default the maximum number of entities deleted in one service call is 50 to limit any theoretical burden on the cron processing.
   * This is easily changed in code if necessary.
   *
   * @param int $maxNrToDelete
   *  Maximum number or entity records to delete from the database in one function call. Use value '0' for infinite.
   */
  public function deleteExpiredOpayoTransactions(int $maxNrToDelete = 50) {

    $configuration = $this->opayoPiService->getSettings();

    if ($configuration['transaction_expiry_cleanup'] > 0) {
      // Delete expired transactions
      $nr_days = $configuration['transaction_expiry_cleanup'];
      $cutoff_date = new DateTime((new DateTime('now'))->format('Y-m-d'));
      $cutoff_date->modify('-' . $nr_days . ' days');

      $this->logger->debug('Opayo Cron::deleteExpiredOpayoTransactions: cleanup \'nr days\' set to ' . $nr_days . ', delete expired entries before ' . $cutoff_date->format('d-m-Y H:i:s'));

      // Delete any Opayo transaction records that are not a successful transaction and happened before the cutoff date
      // Limit to batches of 50
      $range_start = 0;
      $nr_queued_for_deletion = null;
      $range = 50;
      $ids_to_delete = array();

      $nr_results = 1;
      while ($nr_results > 0 && $nr_queued_for_deletion < $maxNrToDelete) {
        $ids = array();
        $remaining = $maxNrToDelete - $nr_queued_for_deletion;
        $range = $remaining < 50 && $maxNrToDelete > 0 ? $remaining : 50;
        try {
          $query = $this->entityTypeManager->getStorage('opayo_transaction')->getQuery();
          $query->accessCheck(FALSE);
          $query->condition('transaction_type', 'null', 'IS NULL');           // No value for 'transaction_type' implies an 'interim' transaction record
          $query->condition('received', $cutoff_date->getTimestamp(), '<');   // Processed before the cutoff date
          $query->range($range_start, $range);
          $query->sort('received', 'ASC');
          $ids = $query->execute();
        } catch (Exception $e) {
          $nr_results = 0;
          $this->logger->error('Opayo Cron::deleteExpiredOpayoTransactions: exception {exception}', ['exception' => ($e->getMessage() . ' ' . $e->getTraceAsString())]);
          throw $e;
        }

        $nr_results = count($ids);

        if ($nr_results > 0) {
          $this->logger->debug('Opayo Cron::deleteExpiredOpayoTransactions: cleanup \'nr days\' set to ' . $nr_days . ', delete expired entries before ' . $cutoff_date->format('d-m-Y H:i:s'));

          $opayo_transactions = $this->entityTypeManager->getStorage('opayo_transaction')->loadMultiple($ids);
          foreach($opayo_transactions as $entity) {
            /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction */
            $opayo_transaction = $entity;
            $item = new stdClass();
            $item->opayoTransactionToDelete = $opayo_transaction->getTransactionId();
            // Add to the queue for deletion
            $this->queue->createItem($item);
            $ids_to_delete[] = $opayo_transaction->getTransactionId();
            $nr_queued_for_deletion++;
          }
        }
        $range_start = $range_start + $nr_results;
      }
      $this->logger->info('Opayo Cron::deleteExpiredOpayoTransactions, transaction IDs queued for deletion: ' . implode(', ', $ids_to_delete));
    }
  }

  /**
   * Delete Commerce payment method entities from the database associated with cases where a customer had the Opayo gateway create
   * a 'card identifier' (encoded within the payment method's 'remote id') which was never succesfully used in a transaction.
   * Normally any expired once should get deleted so that they no longer get offered as an option during customer checkout.
   * Alternatively it allows for keeping the entries for a certain length of time to support tracing issues or queries.
   * The payment gateway config sets the behaviour in terms of the age of entries to be deleted.
   * By default the maximum number of entities deleted in one service call is 50 to limit any theoretical burden on the cron processing.
   * This is easily changed in code if necessary.
   *
   * @param int $maxNrToDelete
   *  Maximum number or entity records to delete from the database in one function call. Use value '0' for infinite.
   */
  public function deleteExpiredCommercePaymentMethods(string $paymentGatewayId, int $maxNrToDelete = 0) {

    $configuration = $this->opayoPiService->getSettings();

    if ($configuration['payment_method_expiry_cleanup'] != 0) {
      // Delete expired transactions
      $nr_days = $configuration['payment_method_expiry_cleanup'];
      $cutoff_time = new DateTime((new DateTime('now'))->format('Y-m-d'));
      $cutoff_time->modify('-' . $nr_days . ' days');
      if ($nr_days == -1) {
        // Delete 'saved' payment methods once they have expired. If a customer had previously put in card details that
        // didn't lead to an order (however a 'payment_method' entry got created) the saved 'payment_method' should normally be deleted once expired because
        // the 'card_identifier' stored with it is no longer valid.
        $cutoff_time = new DateTime('now');
      }

      $this->logger->debug('Opayo Cron::deleteExpiredCommercePaymentMethods: cleanup \'nr days\' set to ' . $nr_days . ', delete expired entries before ' . $cutoff_time->format('d-m-Y H:i:s'));

      // Delete any payment records where the payment gateway
      // Limit to batches of 50
      $range_start = 0;
      $nr_queued_for_deletion = null;
      $range = 50;
      $ids_to_delete = array();

      $nr_results = 1;
      while ($nr_results > 0 && $nr_queued_for_deletion < $maxNrToDelete) {
        $ids = array();
        $remaining = $maxNrToDelete - $nr_queued_for_deletion;
        $range = $remaining < 50 && $maxNrToDelete > 0 ? $remaining : 50;
        try {
          $query = $this->entityTypeManager->getStorage('commerce_payment_method')->getQuery();
          $query->accessCheck(FALSE);
          $query->condition('payment_gateway', $paymentGatewayId, '=');
          $query->condition('reusable', '0', '=');
          $query->condition('expires', $cutoff_time->getTimestamp(), '<');   // Expired before the cutoff time
          $query->range($range_start, $range);
          $query->sort('expires', 'ASC');
          $ids = $query->execute();
        } catch (Exception $e) {
          $nr_results = 0;
          $this->logger->error('Opayo Cron::deleteExpiredCommercePaymentMethods: exception {exception}', ['exception' => ($e->getMessage() . ' ' . $e->getTraceAsString())]);
          throw $e;
        }

        $nr_results = count($ids);

        if ($nr_results > 0) {
          $this->logger->debug('Opayo Cron::deleteExpiredCommercePaymentMethods: cleanup \'nr days\' set to ' . $nr_days . ', delete expired entries before ' . $cutoff_time->format('d-m-Y H:i:s'));

          $commerce_payment_methods = $this->entityTypeManager->getStorage('commerce_payment_method')->loadMultiple($ids);
          foreach($commerce_payment_methods as $entity) {
            /** @var \Drupal\commerce_payment\Entity\PaymentMethod $payment_method */
            $payment_method = $entity;
            $item = new stdClass();
            $item->commercePaymentMethodToDelete = $payment_method->id();
            // Add to the queue for deletion
            $this->queue->createItem($item);
            $ids_to_delete[] = $payment_method->id();
            $nr_queued_for_deletion++;
          }
        }
        $range_start = $range_start + $nr_results;
      }
      $this->logger->info('Opayo Cron::deleteExpiredCommercePaymentMethods, method IDs queued for deletion: ' . implode(', ', $ids_to_delete));
    }
  }


  public function queryAndFetchBankBatchIDs() {

    $configuration = $this->opayoPiService->getSettings();

    if ($configuration['enable_batch_id_matching']) {
      // Limit to the last 60 days, might take too much time otherwise to run the query
      $cutoff_date = new DateTime((new DateTime('now'))->format('Y-m-d'));
      $cutoff_date->modify('-60 days');

      // Run a database query to collect all the dates where there are successful transactions that haven't had a 'Batch ID' allocated to them yet.
      $database = \Drupal::database();
      $query = 'select distinct received from (SELECT cast(from_unixtime(received) as date) received FROM {commerce_opayo_transaction} where transaction_type is not null and batch_id is null and received >= :cutoff_date) as transdates';
      $results = $database->query($query, [':cutoff_date' => $cutoff_date->getTimestamp()]);

      foreach($results as $record) {
        // Found dates for which we need to check and retrieve the Opayo batch information
        $transaction_date = $record->received;


        // The 'daily_queue' uses the 'UniqueDatabaseQueue' to ensure that there is only 1 queue item for any requested 'transaction date'
        $item = new stdClass();
        $item->opayoBatchDateToProcess = $transaction_date;
        // Add to the queue
        $this->daily_queue->createItem($item);
        $this->logger->info('Opayo Cron::queryAndFetchBankBatchIDs, queue processing for transaction date ' . $transaction_date);
      }
    }
  }

}
