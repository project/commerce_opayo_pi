<?php

namespace Drupal\commerce_opayo_pi\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;


/**
 * Defines class representing an Opayo transaction.
 *
 * @ContentEntityType(
 *   id = "opayo_transaction",
 *   label = @Translation("Opayo Transaction"),
 *   label_collection = @Translation("Opayo Transactions"),
 *   label_singular = @Translation("opayo transaction"),
 *   label_plural = @Translation("opayo transactions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count opayo transaction",
 *     plural = "@count opayo transactions",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\commerce_opayo_pi\TransactionListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *     },
 *   },
 *   base_table = "commerce_opayo_transaction",
 *   admin_permission = "administer opayo transactions",
 *   entity_keys = {
 *     "id" = "transaction_id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/commerce/opayotransactions/{opayo_transaction}",
 *     "collection" = "/admin/commerce/orders/opayotransactions",
 *   },
 * )
 */
class OpayoTransaction extends ContentEntityBase implements OpayoTransactionInterface {

  // Limited Interface, getters or setters
  // Entity is anticipated to be used purely for reference through the UI and thus shouldn't need any programmatic manipulation
  // after its initial creation and saving to the database

  /**
   * {@inheritdoc}
   */
  public function getTransactionId() {
    return $this->get('transaction_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatusCode() {
    return $this->get('status_code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChallengeRequest() {
    return $this->get('c_req')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChallengeResponse() {
    return $this->get('c_res')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getReceived() {
    return $this->get('received')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getAcsUrl() {
    return $this->get('acs_url')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionType() {
    return $this->get('transaction_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrder() {
    return $this->get('order')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmount() {
    if (!$this->get('amount')->isEmpty()) {
      return $this->get('amount')->first()->toPrice();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getUserWarningMessage() {
    return $this->get('user_warning')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserWarningMessage(string $message) {
    $this->set('user_warning', $message);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getVendorTxCode() {
    return $this->get('vendor_tx_code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVendorTxCode(string $vendorTxCode) {
    $this->set('vendor_tx_code', $vendorTxCode);
    return $this;
  }


  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['transaction_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Transaction ID'))
      ->setDescription(t('Opayo unique reference for this transaction.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', ['weight' => 1]);

    $fields['payment'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Payment'))
      ->setDescription(t('The payment'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'commerce_payment')
      ->setDisplayOptions('view', ['weight' => 2]);

    $fields['acs_trans_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ACS Transaction ID'))
      ->setDescription(t('Access Control Server (ACS) transaction ID. This is a unique ID provided by the card issuer for 3DSv2 authentications.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 3]);

    $fields['ds_trans_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('DS Transaction ID'))
      ->setDescription(t('Directory Server (DS) transaction ID. This is a unique ID provided by the card scheme for 3DSv2 authentications.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 4]);

    $fields['transaction_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Transaction Type'))
      ->setDescription(t('The type of the transaction.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 5]);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('Result of transaction registration.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 6]);

    $fields['status_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status Code'))
      ->setDescription(t('Code related to the status of the transaction'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 7]);

    $fields['status_detail'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status Detail'))
      ->setDescription(t('A detailed reason for the status of the transaction.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 8]);

    $fields['additional_decline_detail'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Additional Decline Detail'))
      ->setDescription(t('The extended decline code detail which is returned by the card schemes.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 9]);

    $fields['retrieval_reference'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Retrieval Reference'))
      ->setDescription(t('Opayo unique Authorisation Code for a successfully authorised transaction'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 10]);

    $fields['bank_response_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Bank Respone Code'))
      ->setDescription(t('Also known as the decline code, these are codes that are specific to your merchant bank'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 11]);

    $fields['bank_authorisation_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Bank Authorisation Code'))
      ->setDescription(t('The authorisation code returned from the merchant bank.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 12]);

    $fields['avs_cvc_check'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('AVS CVC Check Results'))
      ->setDescription(t('information regarding the AVS/CV2 check results'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 13]);

    $fields['payment_method'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Payment Method'))
      ->setDescription(t('payment method for the transaction.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 14]);

    $fields['card_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Card Type'))
      ->setDescription(t('Card Type used for the payment'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 15]);

    $fields['card_last_four_digits'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Card Last 4 Digits'))
      ->setDescription(t('Last 4 digits of the payment card'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 16]);

    $fields['card_expiry'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Card Expiry Date'))
      ->setDescription(t('Expiry date of the payment card'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 17]);

    $fields['amount'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Amount'))
      ->setDescription(t('The transaction amount.'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 18]);

    $fields['currency'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Currency'))
      ->setDescription(t('Currency.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 19]);

    $fields['3d_secure_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('3D Secure Status'))
      ->setDescription(t('3D Secure status of the transaction, if applied.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 20]);

    $fields['acs_url'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ACS URL'))
      ->setDescription(t('URL that points to the 3D Secure authentication system at the card holder issuing bank'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 21]);

    $fields['c_req'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Challenge Request ID'))
      ->setSetting('max_length', 2000)
      ->setDescription(t('A Base64 encoded message to be passed to the Issuing Bank as part of the 3D Secure Authentication'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 22]);

    $fields['request_payload'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Request Payload'))
      ->setDescription(t('Transaction request JSON payload'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 23]);

    $fields['request_url'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Request URL'))
      ->setDescription(t('Transaction request URL.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 24]);

      $fields['mode'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Mode'))
    ->setDescription(t('Mode'))
    ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 25]);

    $fields['received'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Received'))
      ->setDescription(t('The time when the transaction response was received'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 26]);

    $fields['3d_response_received'] = BaseFieldDefinition::create('timestamp')
    ->setLabel(t('3D Response Received'))
    ->setDescription(t('The time when a 3D response was received'))
    ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 27]);

    $fields['c_res'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Challenge response ID'))
    ->setSetting('max_length', 2000)
      ->setDescription(t('3D Secure challenge response'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 28]);

    $fields['order'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Order'))
      ->setDescription(t('The parent order.'))
      ->setSetting('target_type', 'commerce_order')
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', ['weight' => 29]);

    $fields['batch_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Batch ID'))
      ->setDescription(t('The bank settlement batch number.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 30]);

    $fields['batch_completed'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Batch Completed'))
      ->setDescription(t('The date and time the batch was submitted to the acquiring bank'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 31]);

    $fields['ref_transaction_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Reference Transaction ID'))
      ->setDescription(t('Transaction ID of the original payment for refunds'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 32]);

    $fields['fraud_score_info'] = BaseFieldDefinition::create('text_long')
    ->setLabel(t('Fraud scoring information'))
    ->setDescription(t('Fraud scoring fields contents'))
    ->setSetting('max_length', 2000)
    ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 33]);

    $fields['gateway'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Gateway ID'))
    ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 34]);

    $fields['plugin'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Plugin ID'))
    ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 35]);

    $fields['vendor_tx_code'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Vendor TX Code'))
    ->setDescription(t('Unique code (for every transaction) generated by this software on behalf of the vendor'))
    ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 35]);

    $fields['user_warning'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Warning message to display to user'))
    ->setSetting('max_length', 500);

    $fields['started'] = BaseFieldDefinition::create('timestamp')
    ->setLabel(t('Started'))
    ->setDescription(t('The date/time that the transaction was registered with Opayo'))
    ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 36]);

    $fields['completed'] = BaseFieldDefinition::create('timestamp')
    ->setLabel(t('Completed'))
    ->setDescription(t('The date/time that the transaction was completed'))
    ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 37]);

    return $fields;
  }

}
