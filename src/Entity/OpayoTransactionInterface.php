<?php

namespace Drupal\commerce_opayo_pi\Entity;


use Drupal\Core\Entity\ContentEntityInterface;




interface OpayoTransactionInterface extends ContentEntityInterface {

  /**
   * Gets the Opayo transaction id.
   *
   * @return string
   *   The Opayo transaction id
   */
  public function getTransactionId();

  /**
   * Gets the Opayo transaction status.
   *
   * @return string
   *   The Opayo transaction status
   */
  public function getStatus();

  /**
   * Gets the Opayo transaction status code.
   *
   * @return string
   *   The Opayo transaction status code
   */
  public function getStatusCode();

  /**
   * Gets the Opayo transaction (3DS) challenge request value.
   *
   * @return string
   *   The Opayo transaction (3DS) challenge request value
   */
  public function getChallengeRequest();

  /**
   * Gets the Opayo transaction (3DS) challenge response value.
   *
   * @return string
   *   The Opayo transaction (3DS) challenge reponse value
   */
  public function getChallengeResponse();

  /**
   * Gets the Opayo transaction received timestamp.
   *
   * @return int
   *   The Opayo transaction received timestamp
   */
  public function getReceived();

  /**
   * Gets the Opayo transaction (3DS) ACS URL
   *
   * @return string
   *   The Opayo transaction (3DS) ACS URL
   */
  public function getAcsUrl();

    /**
   * Gets the Opayo transaction type.
   *
   * @return string
   *   The Opayo transaction type
   */
  public function getTransactionType();

  /**
   * Gets the parent order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   The order entity, or null.
   */
  public function getOrder();

  /**
   * Gets the payment amount.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The amount, or NULL.
   */
  public function getAmount();

  /**
   * Gets warning message to display to the user about the Opayo transaction
   *
   * @return string
   *   The message
   */
  public function getUserWarningMessage();

  /**
   * Sets warning message to display to the user about the Opayo transaction
   */
  public function setUserWarningMessage(string $message);


  /**
   * Gets the Vendor Tx Code
   *
   * @return string
   *   The Vendor Tx Code
   */
  public function getVendorTxCode();

  /**
   * Sets the Vendor Tx Code
   */
  public function setVendorTxCode(string $vendorTxCode);

}
