<?php
namespace Drupal\commerce_opayo_pi\Controller;

use DateTime;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartSession;
use Drupal\commerce_cart\CartSessionInterface;
use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_opayo_pi\Ajax\Opayo3DSecureResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_opayo_pi\Ajax\OpayoMerchantSessionKey;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraints\Length;

class OpayoController extends ControllerBase {

  /**
   * The checkout order manager.
   *
   * @var \Drupal\commerce_checkout\CheckoutOrderManagerInterface
   */
  protected $checkoutOrderManager;

  /**
   * The cart session.
   *
   * @var \Drupal\commerce_cart\CartSessionInterface
   */
  protected $cartSession;

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Logger
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new CheckoutController object.
   *
   * @param \Drupal\commerce_checkout\CheckoutOrderManagerInterface $checkout_order_manager
   *   The checkout order manager.
   * @param \Drupal\commerce_cart\CartSessionInterface $cart_session
   *   The cart session.
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart session.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(CheckoutOrderManagerInterface $checkout_order_manager, CartSessionInterface $cart_session, CartProviderInterface $cart_provider, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, RequestStack $requestStack, LoggerChannelInterface $logger) {
    $this->checkoutOrderManager = $checkout_order_manager;
    $this->cartSession = $cart_session;
    $this->cartProvider = $cart_provider;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $requestStack;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_checkout.checkout_order_manager'),
      $container->get('commerce_cart.cart_session'),
      $container->get('commerce_cart.cart_provider'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('commerce_opayo_pi.logger.channel.commerce_opayo_pi'),
    );
  }


  /**
   * During 3DS authentication the ACS redirects the cardholder back to the URL represented by 'Process3DSecure' (all within the context of the
   * iFrame set up within the 'Opayo3DSReview' checkout pane)
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   */
  public function Process3DSecure(RouteMatchInterface $route_match) {

    // We provided the Opayo 'transaction id' as the 'threeDSSessionData' when starting the 3DS challenge - note the 'transaction_id' is still BASE64 encoded

    $request = $this->requestStack->getCurrentRequest();
    try {
      $this->logger->info('OpayoController::Process3DSecure: from {clientIp}', ['clientIp' => $request->getClientIp()]);
    } catch (Exception $le) {
    }

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('order');
    $order_id = $order == null ? '' : $order->id();
    $redirected = false;

    // Use symphony to get the parameters from the POST
    $threeds_session_data_base64 = $request->request->get('threeDSSessionData');
    $challenge_response = $request->request->get('cres');

    $this->logger->info('OpayoController::Process3DSecure: from {clientIp}, received threeDSSessionData: {threeDSSessionData}, cres: {cres}, order: {orderId}', ['clientIp' => $request->getClientIp(), 'threeDSSessionData' => $threeds_session_data_base64, 'cres' =>$challenge_response, 'orderId' => $order_id]);

    $payment_gateway_plugin = null;
    $opayo_transaction = null;
    $opayo_transaction_id = '';
    $configuration = null;

    if ($threeds_session_data_base64) {
      // Match Opayo transaction using the 'threeDSSessionData'

      $threeds_session_data = base64_decode($threeds_session_data_base64);
      $this->logger->info('OpayoController::Process3DSecure: from {clientIp}, received threeDSSessionData: {threeDSSessionData}, cres: {cres}', ['clientIp' => $request->getClientIp(), 'threeDSSessionData' => $threeds_session_data_base64, 'cres' => $challenge_response]);

      // The Opayo API documentation is unclear about what to use as the 'threeDSSessionData' (this identifies between potentially multiple pending 3D Secure challenges)
      // It has been observed that the obvious approach (using the returned 'transactionId') will cause an automatic and unexplained rejection of the 3D secure challenge.
      // Instead we use the 'vendorTxCode' from the original request. It was provided as a parameter to the 3D secure form.
      $vendor_tx_code = $threeds_session_data;

      // Find the Opayo transaction using its 'Vendor Tx Code'
      try {
        $query = $this->entityTypeManager->getStorage('opayo_transaction')->getQuery();
        $query->accessCheck(FALSE);
        $query->condition('vendor_tx_code', $vendor_tx_code);
        $query->condition('c_req', null, 'IS NOT NULL');
        $query->sort('received', 'DESC');
        $ids = $query->execute();
        if (count($ids) >= 1) {
          /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction */
          $opayo_transaction = $this->entityTypeManager->getStorage('opayo_transaction')->load(array_values($ids)[0]);
          $this->logger->info('OpayoController::Process3DSecure: from {clientIp}, matched Opayo transaction: {opayoTransaction} from Vendor Tx Code {vendorTxCode}, order {orderId}', ['clientIp' => $request->getClientIp(), 'opayoTransaction' => $opayo_transaction->getTransactionId(), 'vendorTxCode' => $vendor_tx_code, 'orderId' => $order_id]);
        }
      } catch (Exception $exception) {
      }
    }
    else if ($order_id != '') {
      // Match Opayo transaction using the Order ID
      $opayo_transaction = null;
      try {
        $query = $this->entityTypeManager->getStorage('opayo_transaction')->getQuery();
        $query->accessCheck(FALSE);
        $query->condition('order', $order_id);
        $query->condition('c_req', null, 'IS NOT NULL');
        $query->sort('received', 'DESC');
        $ids = $query->execute();
        if (count($ids) >= 1) {
          /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction */
          $opayo_transaction = $this->entityTypeManager->getStorage('opayo_transaction')->load(array_values($ids)[0]);
          $this->logger->info('OpayoController::Process3DSecure: from {clientIp}, matched Opayo transaction: {opayoTransaction} from order ID {orderId}', ['clientIp' => $request->getClientIp(), 'opayoTransaction' => $opayo_transaction->getTransactionId(), 'orderId' => $order_id]);
        }
      } catch (Exception $exception) {
      }
    }
    else {
      try {
        $this->logger->warning('OpayoController::Process3DSecure: request from from {clientIp} missing threeDSSessionData, payload: {payload}', ['payload' => $request->getContent(), 'clientIp' => $request->getClientIp()]);
      } catch (Exception $le) {
      }

      if ($opayo_transaction == null) {
        // Try to find the matching request a different way - see if there are any 'pending' requests from the same IP address
        $client_ip = $request->getClientIp();
        $cutoff_time = new DateTime();
        $cutoff_time->modify('- 20 minutes');
        try {
          // Fetch recent transactions waiting for a 3D secure response
          $query = $this->entityTypeManager->getStorage('opayo_transaction')->getQuery();
          $query->accessCheck(FALSE);
          $query->condition('received', $cutoff_time->getTimestamp(), '>=');
          $query->condition('c_req', null, 'IS NOT NULL');
          $query->condition('c_res', null, 'IS NULL');
          $query->condition('status_code', '2021');
          $query->sort('received', 'DESC');
          $ids = $query->execute();
          if (count($ids) >= 1) {
            $nr_matched = 0;
            $candidate_opayo_transaction = null;
            $index = 0;
            while ($index < count($ids)) {
              /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $ot */
              $ot = $this->entityTypeManager->getStorage('opayo_transaction')->load(array_values($ids)[$index]);
              // Request payload
              $request_payload = $ot->get('request_payload')->value;
              /** @var stdClass $orig_request */
              $orig_request = json_decode($request_payload);

              if (property_exists($orig_request, 'strongCustomerAuthentication') && property_exists($orig_request->strongCustomerAuthentication, 'browserIP')) {
                $browser_ip = $orig_request->strongCustomerAuthentication->browserIP;
                if ($request->getClientIp() == $browser_ip) {
                  // Match on IP address
                  $nr_matched++;
                  if ($candidate_opayo_transaction == null)
                    $candidate_opayo_transaction = $ot;
                }
              }
              $index++;
            }
            if ($nr_matched == 1) {
              // Precisely 1 Opayo transaction waiting for a 3D response matches the IP address
              $opayo_transaction = $candidate_opayo_transaction;
              $this->logger->info('OpayoController::Process3DSecure: matched Opayo transaction: {opayoTransaction} from {clientIp} (order ID {orderId})', ['clientIp' => $request->getClientIp(), 'opayoTransaction' => $opayo_transaction->getTransactionId(), 'orderId' => $order_id]);
            }
          }
        } catch (Exception $exception) {
        }
      }
    }

    if ($opayo_transaction != null && $opayo_transaction->getChallengeResponse() == null) {
      $opayo_transaction_id = $opayo_transaction->getTransactionId();
      try {
        /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
        $payment_gateway_storage = $this->entityTypeManager->getStorage('commerce_payment_gateway');

        $gateway_id = $opayo_transaction->get('gateway')->value;

        /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
        $payment_gateway = $payment_gateway_storage->load($gateway_id);
        /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $payment_gateway_plugin */
        $payment_gateway_plugin = $payment_gateway->getPlugin();
        $configuration = $payment_gateway_plugin->getConfiguration();

        if ($order == null)
          // Get the order from the opayo transaction, if there is an order
          $order = $opayo_transaction->getOrder();

        $payment_gateway_plugin->logVerboseDebug('OpayoController::Process3DSecure - found Opayo transaction entity for transaction id {transactionId}, cart ID: {orderId}', ['transactionId' => $opayo_transaction_id, 'orderId' => ($order != null ? $order->id() : '')]);

        // Can be called from a different 'session' (may operate within an iFrame)
        if ($order != null && (!$this->cartSession->hasCartId($order->id(), CartSession::ACTIVE))) {
          // Expect the cart to be 'active' in 3D Secure phase
          $this->cartSession->addCartId($order->id(), CartSession::ACTIVE);
          $payment_gateway_plugin->logVerboseDebug('OpayoController::Process3DSecure - add cart ID: {orderId} to Commerce \'cart session\'', ['orderId' => ($order != null ? $order->id() : '')]);
        }

        // Update the opayo transaction state to indicate that we've received a challenge response
        $opayo_transaction->set('c_res', $challenge_response);
        $cutoff_time = new DateTime();
        $opayo_transaction->set('3d_response_received', $cutoff_time->getTimestamp());
        $opayo_transaction->save();

        try {
          $this->logger->info('OpayoController::Process3DSecure: transaction {transactionId}, client IP: {clientIp}, saved challenge response {cRes}',
            ['transactionId' => $opayo_transaction_id, 'clientIp' => $request->getClientIp(), 'cRes' => $challenge_response]);
        } catch (Exception $le) {
        }

        /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $payment_gateway_plugin  */
        if ($payment_gateway_plugin != null && $order != null) {
          try {
            // Opayo Pi Payment gateway to use the 3DS response challenge result to request payment transaction
            $result = $payment_gateway_plugin->process3DSAuthenticationResult($order, $opayo_transaction, $challenge_response);

            if ($result) {
              try {
                $this->logger->info('OpayoController::Process3DSecure: successful payment transaction after successful 3D secure challenge response for transaction {transactionId}, (base64: {base64TransId}), client IP: {clientIp}',
                  ['transactionId' => $opayo_transaction_id, 'base64TransId' => $threeds_session_data_base64, 'clientIp' => $request->getClientIp(), 'clientIp' => $request->getClientIp()]);
              } catch (Exception $le) {
              }

              if ($configuration['3d_iframe'] == true) {

                $payment_gateway_plugin->logVerboseDebug('OpayoController::Process3DSecure - 3D secure response within iFrame for transaction id {transactionId}, cart ID: {orderId}', ['transactionId' => $opayo_transaction_id, 'orderId' => ($order != null ? $order->id() : '')]);

                $computed_settings = [
                  'logLevel' => $configuration['gateway_log_level'],
                ];

                // Return a page (within the iFrame) that sends a success message to the iFrame's parent
                // Render array
                return [
                  '#markup' => '<p>3D Secure success</p>',
                  '#prefix' => '<div id="commerce-opayo-3d-success">',
                  '#suffix' => '</div>',
                  '#attached' => [
                    'library' => ['commerce_opayo_pi/opayo_pi_3dsecure_iframe'],
                    'drupalSettings' => ['commerceopayopi' => ['opayo_3dsecure' => $computed_settings]],
                  ],
                ];
                $payment_gateway_plugin->logVerboseDebug('OpayoController::Process3DSecure - returning \'commerce-opayo-3d-success\' element to customer iFrame for transaction id {transactionId}, cart ID: {orderId}', ['transactionId' => $opayo_transaction_id, 'orderId' => ($order != null ? $order->id() : '')]);
              }
              else {
                $payment_gateway_plugin->logVerboseDebug('OpayoController::Process3DSecure - non-iFrame 3D secure response, transaction id {transactionId}, cart ID: {orderId}', ['transactionId' => $opayo_transaction_id, 'orderId' => ($order != null ? $order->id() : '')]);

                // Redirect to the checkout flow
                if ($order != null) {
                  $url = Url::fromRoute('commerce_checkout.form', ['commerce_order' => $order->id()])->toString();
                  $payment_gateway_plugin->logVerboseDebug('OpayoController::Process3DSecure - redirect customer browser checkout URL for transaction id {transactionId}, cart ID: {orderId}, url: {url}', ['url' => $url, 'transactionId' => $opayo_transaction_id, 'orderId' => ($order != null ? $order->id() : '')]);
                  $response = new RedirectResponse($url);
                  $response->send();
                  // Redirect response has been sent
                  $redirected = true;
                  }
              }
            }
          }
          catch (PaymentGatewayException $pge) {
            // (note: includes DeclineException subclass) Details should have already been logged in $payment_gateway_plugin->process3DSAuthenticationResult
            $this->logger->error(
              'OpayoController::Process3DSecure: payment gateway exception for transaction {transactionId}, message: {message}',
              ['transactionId' => $opayo_transaction_id, 'clientIp' => $request->getClientIp(), 'message' => $pge->getMessage()]
            );
          }
          catch (Exception $pe) {
            try {
              $exception_info = $pe->getMessage() . ' ' . $pe->getTraceAsString();
              $this->logger->error('OpayoController::Process3DSecure: internal error processing transaction {transactionId}, (base64: {base64TransId}), internal exception: {exception}',
                ['transactionId' => $opayo_transaction_id, 'base64TransId' => $threeds_session_data_base64, 'clientIp' => $request->getClientIp(), 'exception' => $exception_info]);
            } catch (Exception $le) {
            }
          }
        }
        else {
          try {
            $this->logger->error('OpayoController::Process3DSecure: internal error processing transaction {transactionId}, (base64: {base64TransId})', ['transactionId' => $opayo_transaction_id, 'base64TransId' => $threeds_session_data_base64, 'clientIp' => $request->getClientIp()]);
          } catch (Exception $le) {
          }
        }
      }
      catch (Exception $e) {
        $this->logger->error('OpayoController::Process3DSecure: exception processing transaction {transactionId}, (base64: {base64TransId}), exception: {exc}', ['exc' => ($e->getMessage() . ' ' . $e->getTraceAsString()), 'transactionId' => $opayo_transaction_id, 'base64TransId' => $threeds_session_data_base64, 'clientIp' => $request->getClientIp()]);
      }
    }
    else {
      // Could not match response to an 'in-progress' Opayo transaction

      $this->logger->error('OpayoController::Process3DSecure - cannot match 3D response for cart ID: {orderId} from {clientIp}', ['orderId' => ($order != null ? $order->id() : ''), 'clientIp' => $request->getClientIp()]);

      if ($opayo_transaction == null) {
        // No matching transaction found
        try {
          $this->logger->warning('OpayoController::Process3DSecure: failed to match Opayo transaction for cart: {orderId} from {clientIp}', ['orderId' => ($order != null ? $order->id() : ''), 'clientIp' => $request->getClientIp()]);
        } catch (Exception $le) {
        }
      }
      else {
        try {
          $this->logger->warning(
            'OpayoController::Process3DSecure: transaction {transactionId} already received a challenge response, client IP: {clientIp}, ignoring subsequent response {cRes}',
            ['transactionId' => $opayo_transaction->getTransactionId(), 'clientIp' => $request->getClientIp(), 'cRes' => $challenge_response]
          );

          /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
          $payment_gateway_storage = $this->entityTypeManager->getStorage('commerce_payment_gateway');
          $gateway_id = $opayo_transaction->get('gateway')->value;
          /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
          $payment_gateway = $payment_gateway_storage->load($gateway_id);
          /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $payment_gateway_plugin */
          $payment_gateway_plugin = $payment_gateway->getPlugin();
          $configuration = $payment_gateway_plugin->getConfiguration();
        } catch (Exception $le) {
        }
        // Redirect back to the checkout, if there's an order and we're not using iFrames
        if ($order != null && $configuration != null && (!$configuration['3d_iframe'] == true)) {
          $url = Url::fromRoute('commerce_checkout.form', ['commerce_order' => $order->id()])->toString();
          $this->logger->warning('OpayoController::Process3DSecure - cannot process 3D response, redirect customer browser checkout URL for transaction id {transactionId}, cart ID: {orderId}, url: {url}', ['url' => $url, 'transactionId' => $opayo_transaction_id, 'orderId' => ($order != null ? $order->id() : '')]);
          $response = new RedirectResponse($url);
          $response->send();
        }
      }
    }

    if ($configuration == null) {
      // There's been an issue and we've not been able to match an on-going request to the response
      // Without a matching Opayo transaction we don't know the gateway that has been used and therefore we don't know whether to respond based on 'iFrame' or 'non-iFrame' situation
      $this->logger->info('OpayoController::Process3DSecure - no transaction match, payment gateway config unknown, cannot process any futher, client IP {clientIp} ', ['clientIp' => $request->getClientIp()]);
      // return an empty render array
      return array();
    }

    $configuration = $payment_gateway_plugin->getConfiguration();
    if ($configuration['3d_iframe'] == true) {
      // Return a page (within the iFrame) that sends a failure message to the iFrame's parent

      // User feedback needs to be passed from within the iFrame to the parent
      $computed_settings = [
        'message' => '3DS authentication failure. You need to re-enter your card details and retry.',
        'logLevel' => $configuration['gateway_log_level'],
      ];

      $payment_gateway_plugin->logVerboseDebug('OpayoController::Process3DSecure - returning \'commerce-opayo-3d-failure\' element to customer iFrame for transaction id {transactionId}, cart ID: {orderId}', ['transactionId' => $opayo_transaction_id, 'orderId' => ($order != null ? $order->id() : '')]);

      return [
        '#markup' => '<p>3D Secure failure</p>',
        '#prefix' => '<div id="commerce-opayo-3d-failure">',
        '#suffix' => '</div>',
        '#attached' => [
          'library' => ['commerce_opayo_pi/opayo_pi_3dsecure_iframe'],
          'drupalSettings' => ['commerceopayopi' => ['opayo_3dsecure' => $computed_settings]],
        ],
      ];
    }
    else {
      if (!$redirected) {
      // There was an error - redirect back to the checkout

        $this->messenger->addMessage('3DS authentication failure. You need to re-enter your card details and retry.', \Drupal\Core\Messenger\MessengerInterface::TYPE_WARNING, TRUE);

        if ($order != null) {
          $url = Url::fromRoute('commerce_checkout.form', ['commerce_order' => $order->id()])->toString();
          $response = new RedirectResponse($url);
          $payment_gateway_plugin->logVerboseDebug('OpayoController::Process3DSecure - redirect customer browser checkout URL for transaction id {transactionId}, cart ID: {orderId}, url: {url}', ['url' => $url, 'transactionId' => $opayo_transaction_id, 'orderId' => ($order != null ? $order->id() : '')]);
          $response->send();
        }
        else {
          // No order ID, redirect back to the cart
          $url = Url::fromRoute('commerce_cart.page')->toString();
          $response = new RedirectResponse($url);
          $payment_gateway_plugin->logVerboseDebug('OpayoController::Process3DSecure - redirect customer browser checkout URL for transaction id {transactionId}, url: {url}', ['url' => $url, 'transactionId' => $opayo_transaction_id]);
          $response->send();
        }
      }
    }

    $this->logger->info('OpayoController::Process3DSecure - completed from {clientIp} ', ['clientIp' => $request->getClientIp()]);

    // Not handled anywhere else, return an empty render array
    return array();
  }


  /**
   * Returns the status/result of 3DS authentication in an AJAX javascript response for a given order and opayo transaction ID
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   */
  public function Get3DSecureResult(RouteMatchInterface $route_match) {

    /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $opayo_transaction */
    $opayo_transaction = $route_match->getParameter('opayo_transaction_id');

    /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
    $payment_gateway_storage = $this->entityTypeManager->getStorage('commerce_payment_gateway');
    $gateway_id = $opayo_transaction->get('gateway')->value;
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $payment_gateway_storage->load($gateway_id);
    /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment_gateway->getPlugin();

    $result = 'transaction_not_found';

    if ($opayo_transaction != null)
    {
      $status_code = $opayo_transaction->getStatusCode();
      $transaction_type = $opayo_transaction->getTransactionType();
      $status = $opayo_transaction->getStatus();
      $threed_response_received_time = $opayo_transaction->get('3d_response_received')->value;
      $payment_gateway_plugin->logVerboseDebug('OpayoController::Get3DSecureResult - Transaction id {transactionId}, transaction type: {transactionType}, status: {status}, status code: {statusCode} 3D response received TS: {threeDrespRec}',
          ['transactionId' => $opayo_transaction->getTransactionId(), 'transactionType' => $transaction_type, 'status' => $status, 'statusCode' => $status_code, 'threeDrespRec' => $threed_response_received_time]);

      if ($status_code == '0000' && $status == 'Ok') {
        $result = 'success';
      }
      elseif ($status == "3DAuth" || $status_code == "2021") {
        // 3D Secure authentication started - still 'in progress'
        // check expiry
        $received_time = $opayo_transaction->getReceived();
        $seconds_since_received = (new DateTime('now'))->getTimestamp() - $received_time;
        if ($seconds_since_received > 1200)
          $result = 'expired';
        else
          $result = 'inprogress';
      }
      else if ($threed_response_received_time > 0) {
        // A 3D response has been received and submitted for authentication
        if ($status_code == '2000') {
          $result = 'declined';
        } else {
          $result = 'failed';
        }
      }
    }

    $response = new AjaxResponse();

    $computed_settings = [
      'transactionId' => $opayo_transaction->getTransactionId(),
      'result' => $result,
    ];
    if ($result == 'declined' || $result == 'failed')
    {
      $computed_settings['message'] = '3DS authentication failure. You need to re-enter your card details and retry.';
    }
    elseif ($result == 'expired')
    {
      $computed_settings['message'] = '3DS authentication not completed within 20 minutes. You need to re-enter your card details and retry.';
    }

    // Add an AJAX javascript command to process a response to the 3D secure authentication in the front-end javascript
    $response->addCommand(new Opayo3DSecureResult($computed_settings));

    $payment_gateway_plugin->logVerboseDebug('OpayoController::Get3DSecureResult - 3DS result {result} for transaction id {transactionId}', ['transactionId' => $opayo_transaction->getTransactionId(), 'result' => $result]);

    return $response;


  }


  /**
   * Requests and returns a new merchant session key in an AJAX response.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return array|Drupal\Core\Ajax\AjaxResponse
   *   The Ajax response commands
   */
  public function MerchantSessionKey(RouteMatchInterface $route_match) {

    $order = $route_match->getParameter('commerce_order');

    // The request may have an 'instruction' that gets returned with the Ajax response alongside the new merchant session key, to be
    // acted upon by the custom 'opayoMerchantSessionKey' Ajax command
    $instruction = $route_match->getParameter('instruction');

    // Get the payment gateway plugin via the order ID or the payment gateway ID
    $payment_gateway = null;
    /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
    $payment_gateway_storage = $this->entityTypeManager->getStorage('commerce_payment_gateway');

    // Find the gateway via the order or direct
    if ($order != null) {
      $payment_gateways = $payment_gateway_storage->loadMultipleForOrder($order);
      foreach($payment_gateways as $pg_id => $pg) {
        $payment_gateway_plugin = $pg->getPlugin();
        if (is_a($payment_gateway_plugin, 'Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGateway')) {
          $payment_gateway = $pg;
        }
      }
    }
    if ($payment_gateway == null) {
      // No order available to find the payment gateway - use the gateway_id supplied to the route
      $payment_gateway_id = $route_match->getParameter('gw_id');
      /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
      $payment_gateway = $payment_gateway_storage->load($payment_gateway_id);
    }

    /** @var \Drupal\commerce_opayo_pi\Plugin\Commerce\PaymentGateway\OpayoPiPaymentGatewayInterface $payment_gateway_plugin  */
    $payment_gateway_plugin = $payment_gateway->getPlugin();

    $response = new AjaxResponse();

    $payment_gateway_plugin->logVerboseDebug('OpayoController::MerchantSessionKey requesting new merchant session key');

    /** @var StdClass $newMerchantSessionKey  */
    $newMerchantSessionKey = $payment_gateway_plugin->getNewMerchantSessionKey($order != null ? $order->id() : null);
    $expiryinsecs = (new DateTime($newMerchantSessionKey->expiry))->getTimestamp() - (new DateTime('now'))->getTimestamp();
    $computed_settings = [
      'merchantSessionKey' => $newMerchantSessionKey->merchantSessionKey,
      'merchantSessionKeyExpirySecs' => $expiryinsecs,
      'merchantSessionKeyExpiry' => $newMerchantSessionKey->expiry,
      'paymentGateway' => $payment_gateway->id(),
    ];
    if (property_exists($newMerchantSessionKey, 'merchantSessionKeySequenceNr'))
      $computed_settings['merchantSessionKeySequenceNr'] = $newMerchantSessionKey->sequenceNr;
    if ($order != null) {
      $computed_settings['orderId'] = $order->id();
    }

    if (isset($instruction))
      // Pass the instruction back to the javascript
      $computed_settings['instruction'] = $instruction;

    $payment_gateway_plugin->logVerboseDebug('OpayoController::MerchantSessionKey returned merchant session key settings: ' . json_encode($computed_settings));

    // Add our custom 'opayoMerchantSessionKey' AJAX javascript command to process an new merchant session key in the front-end javascript
    $response->addCommand(new OpayoMerchantSessionKey($computed_settings));

    return $response;

  }

}
