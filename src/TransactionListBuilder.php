<?php

namespace Drupal\commerce_opayo_pi;

use Drupal\commerce_price\Plugin\Field\FieldType\PriceItem;
use Drupal\commerce_price\Price;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines the list builder for Opayo transactions.
 */
class TransactionListBuilder extends EntityListBuilder {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new OrderListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_type, $entity_type_manager->getStorage($entity_type->id()));

    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager'),
      $container->get('date.formatter')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Transaction ID');
    $header['type'] = $this->t('Transaction Type');
    $header['status'] = $this->t('Status');
    $header['status_detail'] = $this->t('Status Detail');
    $header['retrieval_reference'] = $this->t('Retrieval Reference');
    $header['amount'] = $this->t('Amount');
    $header['order'] = $this->t('Order');
    $header['received'] = $this->t('Received');
    $header['batch_id'] = $this->t('Batch ID');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\commerce_opayo_pi\Entity\OpayoTransaction $entity */

    $opayo_transaction_link = Link::fromTextAndUrl(
      $entity->get('transaction_id')->value,
      new Url(
        'entity.opayo_transaction.canonical',
        [
          'opayo_transaction' => $entity->get('transaction_id')->value,
        ]
      )
    );
    $row['id'] = $opayo_transaction_link;

    $row['type'] = $entity->get('transaction_type')->value;
    $row['status'] = $entity->get('status')->value;
    $row['status_detail'] = $entity->get('status_detail')->value;
    $row['retrieval_reference'] = $entity->get('retrieval_reference')->value;
    /** @var Price $amount */
    $amt_field = $entity->get('amount');
    $amt_val = '';
    if ($amt_field != null && $amt_field->first() != null)
    {
      if ($amt_field->first()->getValue() != null && array_key_exists('number', $amt_field->first()->getValue()))
      {
        $amt_val = $amt_field->first()->getValue()['number'];
      }
    }
    $row['amount'] = $amt_val;


    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $entity->get('order')->entity;
    if ($order != null)
    {
      $order_id = $order->id();
      $order_link = Link::fromTextAndUrl(
        $order_id,
        new Url(
          'entity.commerce_order.canonical',
          [
            'commerce_order' => $order_id,
          ]
        )
      );
      $row['order'] = $order_link;
    }
    else {
      $row['order'] = '';
    }

    foreach (['received'] as $field) {
      if ($entity->get($field)->isEmpty()) {
        $row[$field] = '';
        continue;
      }
      $row[$field] = $this->dateFormatter->format($entity->get($field)->value, 'short');
    }

    $row['batch_id'] = $entity->get('batch_id')->value;


    return $row + parent::buildRow($entity);
  }


  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->sort('received', 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

}
