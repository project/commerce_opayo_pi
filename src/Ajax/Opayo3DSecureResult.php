<?php
namespace Drupal\commerce_opayo_pi\Ajax;

use Drupal\Core\Ajax\CommandInterface;

class Opayo3DSecureResult implements CommandInterface
{
    /**
     * An array of key/value pairs of JavaScript settings.
     *
     * This will be used for all commands after this if they do not include their
     * own settings array.
     *
     * @var array
     */
    protected $settings;

    /**
     * Constructs a Opayo3DSecureResult object.
     *
     * @param array $settings
     *   An array of key/value pairs of JavaScript settings.
     */
    public function __construct(array $settings) {
        $this->settings = $settings;
    }

    /**
     * Implements Drupal\Core\Ajax\CommandInterface:render().
     */
    public function render(){
        return [
            'command' => 'opayo3DSecureResult',
            'settings' => $this->settings,
        ];
    }

}


