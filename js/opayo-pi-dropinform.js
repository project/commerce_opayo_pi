/**
 * @file
 * Defines behaviors to support Opayo Pi checkout using the 'drop-in form'
 *
 */
(function (Drupal, once) {

  'use strict';

  /**
   * Attaches the OpayoCheckout behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the OpayoCheckout behavior.
   */
  Drupal.behaviors.OpayoMerchantSessionKeyTimeout = {
    attach: function (context, settings) {
      once('OpayoPiDropinForm', '#sp-container', context).forEach((ele) => {

        // Call the function that will create the Opayo iframe with the credit card fields
        processingOpayoCheckout(context, settings);

        // Define an Ajax command that can be called via a PHP response 'Command' so that we can process a new merchant session key and act on any included 'instruction'
        Drupal.AjaxCommands.prototype.opayoMerchantSessionKey = function (ajax, response, status) {
          // Update the merchant session key that we need to use to tokenise the card details
          settings.commerceopayopi.opayoCheckout.merchantSessionKey = response.settings.merchantSessionKey;
          settings.commerceopayopi.opayoCheckout.merchantSessionKeySequenceNr = response.settings.merchantSessionKeySequenceNr;
          settings.commerceopayopi.opayoCheckout.merchantSessionKeyExpiry = response.settings.merchantSessionKeyExpiry;

          if (response.settings.hasOwnProperty("instruction") && response.settings.instruction == "rebuild_dropin_form") {
            // Opayo dropin from no longer 'valid' because its embedded 'merchant session key' has expired
            // Destroy and rebuild it irrespective of whether the customer had already filled the fields
            // Refresh the credit card fields and deal with the timers
            processingOpayoCheckout(context, settings, response.settings, true);
          }
        };

        if (settings.commerceopayopi.opayoCheckout.hasOwnProperty('telephoneSource') && settings.commerceopayopi.opayoCheckout.telephoneSource == 'phone_international') {
          // Phone international javascript libraries wait for a 'windowLoaded' flag
          if (window.hasOwnProperty('intlTelInputGlobals') && (!window.intlTelInputGlobals.hasOwnProperty('windowLoaded'))) {
            window.intlTelInputGlobals.windowLoaded = true;
          }
        }

        // Tidy up on submit
        document.querySelector('[type=submit]').addEventListener('submit', function (e) {
          try {
            if (settings.commerceopayopi.opayoCheckout.expiryTimeFeedback) {
              clearInterval(settings.commerceopayopi.opayoCheckout.expiryTimeFeedback);
            }
            if (settings.commerceopayopi.opayoCheckout.mskTimeout) {
              clearTimeout(settings.commerceopayopi.opayoCheckout.mskTimeout);
            }
          }
          catch { }
        }, false);
      }, context);
    }
  }

  function processingOpayoCheckout(context, settings, newSettings = null, refresh = false) {

    if (refresh && settings.commerceopayopi.opayoCheckout.spCheckoutRef) {
      settings.commerceopayopi.opayoCheckout.spCheckoutRef.destroy();
    }

    // Call Opayo's 'sagepayCheckout' function to trigger the building of the iframe
    const merchantSessionKey = newSettings == null ? settings.commerceopayopi.opayoCheckout.merchantSessionKey : newSettings.merchantSessionKey;
    var spCheckoutRef = sagepayCheckout({ merchantSessionKey: merchantSessionKey });
    spCheckoutRef.form();
    settings.commerceopayopi.opayoCheckout.spCheckoutRef = spCheckoutRef;

    if (newSettings != null) {
      // Record the merchant session key info in the form fields so that they get submitted with the tokenised card identifier
      document.querySelector('#merchant-session-key').value = newSettings.merchantSessionKey;
      document.querySelector('#merchant-session-key-sequence-nr').value = newSettings.merchantSessionKeySequenceNr;
      document.querySelector('#merchant-session-key-expiry').value = newSettings.merchantSessionKeyExpiry;
    }

    if (refresh) {
      if (settings.commerceopayopi.opayoCheckout.expiryTimeFeedback) {
        clearInterval(settings.commerceopayopi.opayoCheckout.expiryTimeFeedback);
      }
      if (settings.commerceopayopi.opayoCheckout.mskTimeout) {
        clearTimeout(settings.commerceopayopi.opayoCheckout.mskTimeout);
      }
    }

    // Time remaining on merchant session key
    const skExpiry = Date.parse(newSettings == null ? settings.commerceopayopi.opayoCheckout.merchantSessionKeyExpiry : newSettings.merchantSessionKeyExpiry);
    const dateNow = new Date().getTime();
    if (skExpiry > dateNow) {
      var timeoutInMs = skExpiry - dateNow;
      // Start a timeout based on the merchant session key's validity
      var timeoutRef = startMerchantSessionKeyTimeout(timeoutInMs, merchantSessionKey, settings.commerceopayopi.opayoCheckout.orderId);
      var interval = expiryTimeFeedback(context, skExpiry);
      settings.commerceopayopi.opayoCheckout.expiryTimeFeedback = interval;
      settings.commerceopayopi.opayoCheckout.mskTimeout = timeoutRef;
    }
    else {
      // Request new merchant session key
      var ajaxObject = Drupal.ajax({
        url: '/commerce_opayo_pi/merchantsessionkey/' + settings.commerceopayopi.opayoCheckout.orderId + '/' + settings.commerceopayopi.opayoCheckout.merchantSessionKey,
        base: false,
        element: false,
        progress: false
      });
      ajaxObject.execute();

    }
  }

  function startMerchantSessionKeyTimeout(msTimeout, merchantSessionKey, orderId) {
    // Start a timeout based on the merchant session key's validity
    var timeoutRef = setTimeout((merchantSessionKey, orderId) => {
      const spContainer = document.getElementById('sp-container');
      if (spContainer) {
        // Still displaying the checkout form, call the URL to refresh the merchant session key and return Ajax commands
        var ajaxObject = Drupal.ajax({
          url: '/commerce_opayo_pi/merchantsessionkey/' + orderId + '/rebuild_dropin_form',
          base: false,
          element: false,
          progress: false
        });
        ajaxObject.execute();
      }
    }, msTimeout, merchantSessionKey, orderId);
    return timeoutRef;
  }

  function expiryTimeFeedback(context, expiry) {
    var expiryMsgElement = document.getElementById('expiry-msg');
    var interval = setInterval((expiryMsgElement) => {
      const dateNow = new Date().getTime();
      var nrSecs = Math.round((expiry - dateNow) / 1000);
      if (nrSecs > 0 && nrSecs < 120) {
        let minutes = Math.floor(nrSecs / 60);
        let expiryMsg = '<i>Notice: In ' + (minutes > 0 ? '' + minutes + ' minutes ' : '') + (nrSecs % 60) + ' seconds the Name, Card, Expiry and CVC fields below will be automatically cleared. If you have card details entered you will have to type them in again.</i>';
        expiryMsgElement.innerHTML = expiryMsg;
      }
      else {
        expiryMsgElement.innerHTML = '';
      }
      nrSecs--;
      if (nrSecs < 0) {
        clearInterval(interval);
      }
    }, 1000, expiryMsgElement);
    return interval;
  }

})(Drupal, once);
