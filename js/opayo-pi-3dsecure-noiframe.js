/**
 * @file
 * Defines behaviors to support Opayo Pi 3D Secure authentication
 *
 */
(function (Drupal, once) {

  'use strict';

  /**
   * Attaches the Opayo3DSecure behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the OpayoCheckout behavior.
   */
  Drupal.behaviors.Opayo3DSecure = {
    attach: function (context, settings) {
      once('Opayo3DSecure', '#commerce-checkout-flow-opayo-checkout-flow', context).forEach((ele) => {

        // Redirect commerce checkout form and button to acquiring bank's 3D secure challenge
        let buttonSelector = settings.commerceopayopi.opayo_3dsecure_noiframe.hasOwnProperty('proceedButtonSelector') ? settings.commerceopayopi.opayo_3dsecure_noiframe.proceedButtonSelector : '[data-drupal-selector="edit-actions-next"]';
        let proceedButton = document.querySelector(buttonSelector);
        proceedButton.value = "Proceed to 3D Secure";
        let checkoutFormSelector = settings.commerceopayopi.opayo_3dsecure_noiframe.hasOwnProperty('formQuerySelector') ? settings.commerceopayopi.opayo_3dsecure_noiframe.formQuerySelector : 'form[id^="commerce-checkout"]';
        let checkoutForm = document.querySelector(checkoutFormSelector);
        checkoutForm.action = settings.commerceopayopi.opayo_3dsecure_noiframe.acsUrl;

      }, context);
    }
  }

})(Drupal, once);
