/**
 * @file
 * Javascript needed inside the Opayo 3D secure iframe
 *
 */
(function (Drupal, once) {

  'use strict';

  /**
   * Attaches the Opayo3DSecure behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the OpayoCheckout behavior.
   */
  Drupal.behaviors.Opayo3DSecure = {
    attach: function (context, settings) {

      once('Opayo3DSecureSuccess', '#commerce-opayo-3d-success', context).forEach((ele) => {
        // This is called from within the 3D Secure iFrame when the challenge has completed succesfully
        const message = JSON.stringify({
            result: 'success',
            date: Date.now(),
        });
        logVerboseDebugMessage(settings, 'OpayoPi3DSecure-iFrame - post success message to iFrame parent');
        window.parent.postMessage(message, '*');
      }, context);

      once('Opayo3DSecureFailure', '#commerce-opayo-3d-failure', context).forEach((ele) => {
        // This is called from within the 3D Secure iFrame when the challenge has failed
        const message = JSON.stringify({
            result: 'failed',
            messageforuser: settings.commerceopayopi.opayo_3dsecure.message,
            date: Date.now(),
        });
        logVerboseDebugMessage(settings, 'OpayoPi3DSecure-iFrame - post failed message to iFrame parent');
        window.parent.postMessage(message, '*');
      }, context);
    }
  }

  function logVerboseDebugMessage(settings, logMessage, error = null) {
    if (settings.commerceopayopi.opayo_3dsecure.hasOwnProperty('logLevel') && settings.commerceopayopi.opayo_3dsecure.logLevel == 10) {
      if (error != null) {
        console.error(error);
      }
      console.log(logMessage);
    }
  }

})(Drupal, once);
