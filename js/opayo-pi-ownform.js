/**
 * @file
 * Defines behaviors to support Opayo Pi checkout using the 'drop-in form'
 *
 */
(function (Drupal, once) {

  'use strict';

  /**
   * Attaches the OpayoCheckout behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the OpayoCheckout behavior.
   */
  Drupal.behaviors.OpayoPiOwnForm = {
    attach: function (context, settings) {
      once('OpayoPiOwnForm', '#card-identifier', context).forEach((ele) => {

        if (!Drupal.AjaxCommands.prototype.hasOwnProperty('opayoMerchantSessionKey')) {
          // Define an Ajax command that can be called via a PHP response 'Command' so that we can process a new merchant session key and act on any included 'instruction'
          Drupal.AjaxCommands.prototype.opayoMerchantSessionKey = function (ajax, response, status) {
            // Update the merchant session key that we need to use to tokenise the card details
            settings.commerceopayopi.opayoCheckout.merchantSessionKey = response.settings.merchantSessionKey;
            settings.commerceopayopi.opayoCheckout.merchantSessionKeySequenceNr = response.settings.merchantSessionKeySequenceNr;
            settings.commerceopayopi.opayoCheckout.merchantSessionKeyExpiry = response.settings.merchantSessionKeyExpiry;

            let formBuildId = '';
            try {
              let checkoutFormSelector = settings.commerceopayopi.opayoCheckout.hasOwnProperty('formQuerySelector') ? settings.commerceopayopi.opayoCheckout.formQuerySelector : 'form[id^="commerce-checkout"]';
              let checkoutForm = document.querySelector(checkoutFormSelector);
              formBuildId = checkoutForm.querySelector('[name="form_build_id"]').value;
            } catch { }
            logVerboseDebugMessage(settings, 'OpayoPiOwnForm - Ajax command opayoMerchantSessionKey: returned merchant session key: ' + response.settings.merchantSessionKey + ', expiry: ' + response.settings.merchantSessionKeyExpiry + ', instruction: ' + response.settings.instruction + ', form build id: ' + formBuildId);

            if (response.settings.hasOwnProperty("instruction") && response.settings.instruction == "tokenize_and_submit") {
              // New merchant session key needs to be used to tokenize the card details and submit the checkout form
              callCardDetailTokenisation(context, settings);
            }
          };
        }

        // Copy from first name and surname to cardholder unless the cardholder has been manually entered
        // Will need to keep track whether the 'cardholder name' has been modified by user input (in which case the event handler will leave it alone)
        document.querySelector('[data-card-details="cardholder-first-name"]').addEventListener('change', function (e) { copyCardholderName(e); }, false);
        document.querySelector('[data-card-details="cardholder-last-name"]').addEventListener('change', function (e) { copyCardholderName(e); }, false);
        document.querySelector('[data-card-details="cardholder-name"]').addEventListener('change', function (e) { copyCardholderName(e); }, false);

        let checkoutFormSelector = settings.commerceopayopi.opayoCheckout.hasOwnProperty('formQuerySelector') ? settings.commerceopayopi.opayoCheckout.formQuerySelector : 'form[id^="commerce-checkout"]';
        let checkoutForm = document.querySelector(checkoutFormSelector);
        if (checkoutForm != null && (checkoutForm.querySelector('input[data-card-details="cardholder-name-manual-entry"]') == null)) {
          let cardHolderNameHidden = document.createElement("input");
          cardHolderNameHidden.setAttribute("type", "hidden");
          cardHolderNameHidden.setAttribute("data-card-details", "cardholder-name-manual-entry");
          checkoutForm.appendChild(cardHolderNameHidden);
        }

        let paymentMethodWrapperSelector = settings.commerceopayopi.opayoCheckout.hasOwnProperty('paymentMethodWrapper') ? settings.commerceopayopi.opayoCheckout.paymentMethodWrapper : 'fieldset[data-drupal-selector="edit-payment-information-payment-method"]';
        let paymentMethodWrapper = document.querySelector(paymentMethodWrapperSelector);
        if (paymentMethodWrapper != null) {
          if (!settings.commerceopayopi.opayoCheckout.hasOwnProperty('gatewayChoiceOption')) {
            // There is a choice of payment methods - only one is Opayo Pi (where we intercept the form submit)
            // Intercepting the form submit button is only appropriate if the Opayo Pi payment gateway is selected
            let opayoPiPaymentGatewayChoiceOptionSelector = settings.commerceopayopi.opayoCheckout.hasOwnProperty('opayoPiPaymentGatewayChoiceSelector') ? settings.commerceopayopi.opayoCheckout.opayoPiPaymentGatewayChoiceSelector : 'input.form-radio[name="payment_information[payment_method]"][value^="new--credit_card_opayo"]';
            let opayoPiPaymentGatewayChoiceOption = document.querySelector(opayoPiPaymentGatewayChoiceOptionSelector);
            settings.commerceopayopi.opayoCheckout.gatewayChoiceOption = opayoPiPaymentGatewayChoiceOption;
            if (opayoPiPaymentGatewayChoiceOption == null) {
              logVerboseDebugMessage(settings, 'OpayoPiOwnForm - no element found representing choice of Opayo Pi gateway');
            }
            else {
              logVerboseDebugMessage(settings, 'OpayoPiOwnForm - element representing choice of Opayo Pi gateway checked: ' + opayoPiPaymentGatewayChoiceOption.checked);
            }
          }

          if (settings.commerceopayopi.opayoCheckout.gatewayChoiceOption != null && settings.commerceopayopi.opayoCheckout.gatewayChoiceOption.checked) {
            addSubmitEventListener(settings);
          }
        }
        else {
          // No choice of payment method, assume only one: 'Opayo Pi' - intercept the form submit
          addSubmitEventListener(settings);
          logVerboseDebugMessage(settings, 'OpayoPiOwnForm - no payment method choice detected, assume single Opayo Pi payment method');
        }

        if (settings.commerceopayopi.opayoCheckout.hasOwnProperty('telephoneSource') && settings.commerceopayopi.opayoCheckout.telephoneSource == 'phone_international') {
          // Phone international javascript libraries wait for a 'windowLoaded' flag
          if (window.hasOwnProperty('intlTelInputGlobals') && (!window.intlTelInputGlobals.hasOwnProperty('windowLoaded'))) {
            window.intlTelInputGlobals.windowLoaded = true;
          }
        }

       }, context);
    }
  }

  function addSubmitEventListener(settings) {
    logVerboseDebugMessage(settings, 'OpayoPiOwnForm.addSubmitEventListener called');
    // Add an event listener for the form submit button that will first contact Opayo to tokenise the card details, add the new 'card identifier' to the form and submit the form back to our server
    let buttonSelector = settings.commerceopayopi.opayoCheckout.hasOwnProperty('submitButtonSelector') ? settings.commerceopayopi.opayoCheckout.submitButtonSelector : 'input[data-drupal-selector="edit-actions-next"]';
    let checkoutFormButton = document.querySelector(buttonSelector);
    settings.commerceopayopi.opayoCheckout.submitButton = checkoutFormButton;
    if (checkoutFormButton != null) {
      try {
        if (settings.commerceopayopi.opayoCheckout.hasOwnProperty('clickEventBound')) {
          // Avoid duplication of event listener
          checkoutFormButton.removeEventListener('click', settings.commerceopayopi.opayoCheckout.clickEventBound);
          delete settings.commerceopayopi.opayoCheckout.clickEventBound;
        }
      } catch {}
      const once = { once: true };
      let clickEventBound = checkoutFormButtonClickEvent.bind(settings);
      settings.commerceopayopi.opayoCheckout.clickEventBound = clickEventBound;
      checkoutFormButton.addEventListener('click', clickEventBound, once);
      logVerboseDebugMessage(settings, 'OpayoPiOwnForm.addSubmitEventListener checkoutFormButtonClickEvent listener added');
    }
    logVerboseDebugMessage(settings, 'OpayoPiOwnForm.addSubmitEventListener completed');
  }

  function checkoutFormButtonClickEvent(e) {
    // Drupal 'settings' has been bound to 'this'
    if ((!this.commerceopayopi.opayoCheckout.hasOwnProperty('gatewayChoiceOption')) || (this.commerceopayopi.opayoCheckout.gatewayChoiceOption != null && this.commerceopayopi.opayoCheckout.gatewayChoiceOption.checked)) {
      // The 'Opayo Pi' gateway is selected - 'intercept' form submit to insert start of the process of fetching of merchant session key and cart tokenisation
      logVerboseDebugMessage(this, 'OpayoPiOwnForm.checkoutFormButtonClickEvent form submit clicked with Opayo Pi payment method');

      e.preventDefault(); // to prevent form submission

      let formBuildId = '';
      try { formBuildId = e.currentTarget.form.querySelector('[name="form_build_id"]').value; } catch { }
      logVerboseDebugMessage(this, 'OpayoPiOwnForm form submit button clicked, form build id: ' + formBuildId);

      // Fetch a merchant session key first by requesting the back-end to fetch and return one using AJAX
      // Result should contain the custom 'opayoMerchantSessionKey' Ajax command
      let url = '';
      if (this.commerceopayopi.opayoCheckout.hasOwnProperty('orderId')) {
        url = '/commerce_opayo_pi/merchantsessionkey/' + this.commerceopayopi.opayoCheckout.orderId + '/tokenize_and_submit';
      }
      else {
        // No order ID - payment gateway plugin ID needs to be set explicitly
        url = '/commerce_opayo_pi/merchantsessionkeyforgateway/' + this.commerceopayopi.opayoCheckout.paymentGateway + '/tokenize_and_submit';
      }
      logVerboseDebugMessage(this, 'OpayoPiOwnForm request merchant session key using URL: ' + url + ', form build id: ' + formBuildId);
      let ajaxObject = Drupal.ajax({
        url: url,
        base: false,
        element: false,
        progress: false
      });
      ajaxObject.execute();
      logVerboseDebugMessage(this, 'OpayoPiOwnForm request merchant session key sent (URL: ' + url + ')' + ', form build id: ' + formBuildId);
    }
    else {
      // 'else' if the payment method is different from a 'new card with Opayo Pi gateway' then event listener does nothing
      logVerboseDebugMessage(this, 'OpayoPiOwnForm.checkoutFormButtonClickEvent form submit clicked, non-OpayoPI payment method');
    }
  }

  function copyCardholderName(e) {
    const element = e.currentTarget;
    let dataCardDetails = element.getAttribute("data-card-details");
    if (dataCardDetails == "cardholder-first-name" || dataCardDetails == "cardholder-last-name") {
      if (!(document.querySelector('[data-card-details="cardholder-name-manual-entry"]').value))
      {
        // User has not typed into the cardholder name field, copy from first name and surname fields
        let firstName = document.querySelector('[data-card-details="cardholder-first-name"]').value;
        let lastName = document.querySelector('[data-card-details="cardholder-last-name"]').value;
        let cardholderName = firstName + (firstName.length > 0 && lastName.length > 0 ? ' ' : '') + lastName;
        document.querySelector('[data-card-details="cardholder-name"]').value = cardholderName;
      }
    }
    else if (dataCardDetails == "cardholder-name" && e.isTrusted) {
      // User modified cardholder name field
      document.querySelector('[data-card-details="cardholder-name-manual-entry"]').value = element.value;
    }
  }

  function getCardType (number) {
  const re = {
    electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
    maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
    dankort: /^(5019)\d+$/,
    interpayment: /^(636)\d+$/,
    unionpay: /^(62|88)\d+$/,
    visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
    mastercard: /^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/,
    amex: /^3[47][0-9]{13}$/,
    diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
    discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
    jcb: /^(?:2131|1800|35\d{3})\d{11}$/
  }
  for (var key in re) {
    if (re[key].test(number)) {
      return key
    }
  }
  return 'unknown';
}

  function processMerchantSessionKey(context, settings, newSettings) {
    // Update the merchant session key that we need to use to tokenise the card details
    settings.commerceopayopi.opayoCheckout.merchantSessionKey = newSettings.merchantSessionKey;
    settings.commerceopayopi.opayoCheckout.merchantSessionKeySequenceNr = newSettings.merchantSessionKeySequenceNr;
    settings.commerceopayopi.opayoCheckout.merchantSessionKeyExpiry = newSettings.merchantSessionKeyExpiry;
  }


  function maskCreditCard(cardNumber) {
    const n = cardNumber.length;
    const lastFour = cardNumber.slice(-4);
    const remaining = cardNumber.slice(0, n - 4);
    const masked = "*".repeat(remaining.length) + lastFour;
    return masked;
  }

  function callCardDetailTokenisation(context, settings) {

    let cardNumber = Drupal.checkPlain(context.querySelector('[data-card-details="card-number"]').value);
    settings.commerceopayopi.opayoCheckout.cardType = getCardType(cardNumber);
    settings.commerceopayopi.opayoCheckout.cardLastFourDigits = cardNumber.slice(-4);
    let cardDetails = {
      cardholderName: Drupal.checkPlain(context.querySelector('[data-card-details="cardholder-name"]').value),
      cardNumber: cardNumber,
      expiryDate: Drupal.checkPlain(context.querySelector('[data-card-details="expiry-date-month"]').value) + Drupal.checkPlain(context.querySelector('[data-card-details="expiry-date-year"]').value).substring(2),
      securityCode: context.querySelector('[data-card-details="security-code"]').value,
    };
    const maskedCardNumber = maskCreditCard(cardNumber);

    let formBuildId = '';
    try {
      let checkoutFormSelector = settings.commerceopayopi.opayoCheckout.hasOwnProperty('formQuerySelector') ? settings.commerceopayopi.opayoCheckout.formQuerySelector : 'form[id^="commerce-checkout"]';
      let checkoutForm = document.querySelector(checkoutFormSelector);
      formBuildId = checkoutForm.querySelector('[name="form_build_id"]').value;
    } catch { }
    let mode = settings.commerceopayopi.opayoCheckout.hasOwnProperty('mode') ? settings.commerceopayopi.opayoCheckout.mode : 'unknown';
    logVerboseDebugMessage(settings, 'OpayoPiOwnForm.callCardDetailTokenisation: about to call sagepayOwnForm.tokeniseCardDetails (mode: ' + mode + ') with merchant session key: ' + settings.commerceopayopi.opayoCheckout.merchantSessionKey + ' for card: ' + maskedCardNumber + ', form build id: ' + formBuildId);

    sagepayOwnForm({ merchantSessionKey: settings.commerceopayopi.opayoCheckout.merchantSessionKey }).tokeniseCardDetails({
      cardDetails: cardDetails,
      onTokenised: function (result) { cardDetailsTokenised(result, settings, context); }
    });

    logVerboseDebugMessage(settings, 'OpayoPiOwnForm.callCardDetailTokenisation: called sagepayOwnForm.tokeniseCardDetails with merchant session key: ' + settings.commerceopayopi.opayoCheckout.merchantSessionKey + ', form build id: ' + formBuildId);
  }


  function cardDetailsTokenised(result, settings, context) {

    let checkoutForm = null;
    let ds = settings;
    if (ds == null || (!ds.hasOwnProperty('commerceopayopi')) || (!ds.commerceopayopi.hasOwnProperty('opayoCheckout'))) {
      // Opayo settings not returned with the closure - get the global one
      ds = drupalSettings;
      if (ds == null || (!ds.hasOwnProperty('commerceopayopi')) || (!ds.commerceopayopi.hasOwnProperty('opayoCheckout'))) {
        // No access to the Drupal settings
        console.log('OpayoPiOwnForm.cardDetailsTokenised - no access to Drupal settings');
        return;
      }
    }

    let formBuildId = '';
    try {
      let checkoutFormSelector = ds.commerceopayopi.opayoCheckout.hasOwnProperty('formQuerySelector') ? ds.commerceopayopi.opayoCheckout.formQuerySelector : 'form[id^="commerce-checkout"]';
      checkoutForm = document.querySelector(checkoutFormSelector);
      formBuildId = checkoutForm.querySelector('[name="form_build_id"]').value;
    } catch { }

    logVerboseDebugMessage(ds, 'OpayoPiOwnForm.cardDetailsTokenised result: ' + result.success + ', merchant session key used: ' + ds.commerceopayopi.opayoCheckout.merchantSessionKey + ', form build id: ' + formBuildId);

    if (result.hasOwnProperty('success')) {
      try {
        // Required field for form submit
        document.querySelector('#card-identifier-result').value = result.success;
        logVerboseDebugMessage(ds, 'OpayoPiOwnForm.cardDetailsTokenised set card identifier result to: ' + result.success);
      }
      catch (error) {
        logVerboseDebugMessage(ds, 'OpayoPiOwnForm.cardDetailsTokenised could not set card identifier result', error);
      }
    }
    if (result.hasOwnProperty('success') && result.success) {
      try {
        document.querySelector('#card-identifier').value = result.cardIdentifier;
        // Return the merchant session key that was used for the tokenisation
        document.querySelector('#merchant-session-key').value = ds.commerceopayopi.opayoCheckout.merchantSessionKey;
        document.querySelector('#merchant-session-key-sequence-nr').value = ds.commerceopayopi.opayoCheckout.merchantSessionKeySequenceNr;
        document.querySelector('#merchant-session-key-expiry').value = ds.commerceopayopi.opayoCheckout.merchantSessionKeyExpiry;
      }
      catch (error) {
        logVerboseDebugMessage(ds, 'OpayoPiOwnForm.cardDetailsTokenised could not set card tokenisation result fields', error);
      }

      try {
        // Post the card identifier expiry back as well
        // Documentation states card identifier valid for 400 seconds
        // The exact value is not passed back by the Opayo javascript so assume 5 seconds for processing
        let cardIdExpTime = new Date();
        cardIdExpTime = new Date(cardIdExpTime.getTime() + (1000 * 395));
        let cardIdExpTimeField = document.querySelector('#card-identifier-expiry');
        cardIdExpTimeField.value = cardIdExpTime.toISOString();

        // Post back browser settings to be used with the (required) 'strong customer authentication' fields during the transaction request
        let colorDepth = window.screen.colorDepth;
        let screenHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        let screenWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        let timeZoneOffset = (new Date()).getTimezoneOffset();
        let language = navigator.language || navigator.userLanguage;
        // Create fields dynamically, haven't set up any preallocated hidden fields
        let input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "browser-color-depth");
        input.setAttribute("value", colorDepth);
        checkoutForm.appendChild(input);
        input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "browser-screen-height");
        input.setAttribute("value", screenHeight);
        checkoutForm.appendChild(input);
        input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "browser-screen-width");
        input.setAttribute("value", screenWidth);
        checkoutForm.appendChild(input);
        input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "browser-timezone-offset");
        input.setAttribute("value", timeZoneOffset);
        checkoutForm.appendChild(input);
        input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "browser-language");
        input.setAttribute("value", language);
        checkoutForm.appendChild(input);
        // Card Type and last four digits
        input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "card-type");
        input.setAttribute("value", ds.commerceopayopi.opayoCheckout.cardType);
        checkoutForm.appendChild(input);
        input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "card-last-four-digits");
        input.setAttribute("value", ds.commerceopayopi.opayoCheckout.cardLastFourDigits);
        checkoutForm.appendChild(input);

        if (ds.commerceopayopi.opayoCheckout.hasOwnProperty('useTriggeringElement')) {
          // Set the triggering element to the submit button
          input = document.createElement("input");
          input.setAttribute("type", "hidden");
          input.setAttribute("name", "_triggering_element_name");
          input.setAttribute("value", ds.commerceopayopi.opayoCheckout.submitButton.getAttribute("name"));
          checkoutForm.appendChild(input);
          input = document.createElement("input");
          input.setAttribute("type", "hidden");
          input.setAttribute("name", "_triggering_element_value");
          input.setAttribute("value", ds.commerceopayopi.opayoCheckout.submitButton.value);
          checkoutForm.appendChild(input);
        }
      }
      catch (error) {
        logVerboseDebugMessage(ds, 'OpayoPiOwnForm.cardDetailsTokenised could not set all context form fields', error);
      }

      logVerboseDebugMessage(ds, 'OpayoPiOwnForm.cardDetailsTokenised success response processed - about to submit form, card identifier: ' + result.cardIdentifier + ', merchant session key: ' + ds.commerceopayopi.opayoCheckout.merchantSessionKey);
      if (checkoutForm.requestSubmit) {
        checkoutForm.requestSubmit();
      } else {
        checkoutForm.submit();
      }
      logVerboseDebugMessage(ds, 'OpayoPiOwnForm.cardDetailsTokenised success response processed - form submitted');
    }
    else {
      // Failed tokenisation - we still submit the form to get the back-end to 'translate' Opayo to Drupal Commerce error messages
      let httpErrorCode = 0;
      let firstErrorCode = 0;
      let firstErrorMessage = '';
      try {
        document.querySelector('#card-identifier').value = 'failed';
        if (result.hasOwnProperty('httpErrorCode')) {
          document.querySelector('#card-identifier-error-httpcode').value = result.httpErrorCode;
          httpErrorCode = result.httpErrorCode;
        }
        try {
          // If there are multiple errors, add the errors to the form as hidden fields
          for (let i = 0; i < result.errors.length; i++) {
            let errorDetail = result.errors[i];
            let input = document.createElement("input");
            input.setAttribute("type", "hidden");
            input.setAttribute("name", "card-identifier-error-" + i + "-code");
            input.setAttribute("value", errorDetail.code);
            checkoutForm.appendChild(input);
            input = document.createElement("input");
            input.setAttribute("type", "hidden");
            input.setAttribute("name", "card-identifier-error-" + i + "-message");
            input.setAttribute("value", errorDetail.message);
            checkoutForm.appendChild(input);
            if (i == 0) {
              firstErrorCode = errorDetail.code;
              firstErrorMessage = errorDetail.message;
            }
            else {
              logVerboseDebugMessage(ds, 'OpayoPiOwnForm.cardDetailsTokenised error message ' + i + ' from Opayo - code: ' + errorDetail.code + ', message: ' + errorDetail.message);
             }
          }
        }
        catch (error) {
          logVerboseDebugMessage(ds, 'OpayoPiOwnForm.cardDetailsTokenised error processing Opayo result errors', error);
        }
      }
      catch (error) {
       logVerboseDebugMessage(ds, 'OpayoPiOwnForm.cardDetailsTokenised error processing Opayo tokenisation failure result', error);
      }

      logVerboseDebugMessage(ds, 'OpayoPiOwnForm.cardDetailsTokenised failed response processed - about to submit form, HTTP error code: ' + httpErrorCode + ', first error (code: ' + firstErrorCode + '): ' + firstErrorMessage);
      checkoutForm.submit();
      logVerboseDebugMessage(ds, 'OpayoPiOwnForm.cardDetailsTokenised failed response processed - form submitted');
    }
  }

  function logVerboseDebugMessage(settings, logMessage, error = null) {
    if (settings.commerceopayopi.opayoCheckout.hasOwnProperty('logLevel') && settings.commerceopayopi.opayoCheckout.logLevel == 10) {
      if (error != null) {
        console.error(error);
      }
      console.log(logMessage);
    }
  }

})(Drupal, once);
