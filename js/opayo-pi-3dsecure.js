/**
 * @file
 * Defines behaviors to support Opayo Pi 3D Secure authentication
 *
 */
(function (Drupal, once) {

  'use strict';

  /**
   * Attaches the Opayo3DSecure behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the OpayoCheckout behavior.
   */
  Drupal.behaviors.Opayo3DSecure = {
    attach: function (context, settings) {
      once('Opayo3DSecure', '#commerce-opayo-3d-iframe', context).forEach((ele) => {

        let formBuildId = '';
        try {
          let checkoutFormSelector = settings.commerceopayopi.opayo_3dsecure.hasOwnProperty('formQuerySelector') ? settings.commerceopayopi.opayo_3dsecure.formQuerySelector : 'form[id^="commerce-checkout"]';
          let checkoutForm = document.querySelector(checkoutFormSelector);
          formBuildId = checkoutForm.querySelector('[name="form_build_id"]').value;
        } catch { }
        logVerboseDebugMessage(settings, 'OpayoPi3DSecure setup' + ', form build id: ' + formBuildId);

        settings.commerceopayopi.opayo_3dsecure['status-' + formBuildId] = 'inprogress';

        // Cannot proceed at this stage until 3D secure authentication has completed
        let buttonSelector = settings.commerceopayopi.opayo_3dsecure.hasOwnProperty('proceedButtonSelector') ? settings.commerceopayopi.opayo_3dsecure.proceedButtonSelector : '[data-drupal-selector="edit-actions-next"]';
        let proceedButton = document.querySelector(buttonSelector);
        proceedButton.disabled = true;

        // Define an Ajax command that can be called via a PHP response 'Command' so that we can process the response to the 3D secure progress request and act on any included 'instruction'
        Drupal.AjaxCommands.prototype.opayo3DSecureResult = function (ajax, response, status) {
          if (response.settings.hasOwnProperty("result")) {
            logVerboseDebugMessage(settings, 'OpayoPi3DSecure AJAX command opayo3DSecureResult result: ' + response.settings.result);
            if (response.settings.result != "inprogress")
            {
              // There is a resolution
              let message = '';
              if (response.settings.hasOwnProperty("message")) {
                message = response.settings.message;
              }
              // Process the result
              process3DSecureResult(response.settings.result, message, settings);
            }
            // Do nothing if the authentication is still 'in progress'
          }
          else {
            logVerboseDebugMessage(settings, 'OpayoPi3DSecure AJAX command opayo3DSecureResult, response is missing result');
          }
        };

        // Timer to check for 3D Authentication results
        let transactionId = document.querySelector('#commerce-opayo-transaction-id').value;
        if (!settings.hasOwnProperty('commerceopayopi'))
          settings.commerceopayopi = new Object();
        if (!settings.commerceopayopi.hasOwnProperty('opayo_3dsecure'))
          settings.commerceopayopi.opayo_3dsecure = new Object();
        settings.commerceopayopi.opayo_3dsecure.timerRef = start3DResultTimer(10000, transactionId, settings);

        // Add event listener for messages from the 3D Secure iFrame
        window.addEventListener('message', function (e) {
          const message = JSON.parse(e.data);
          // Result feedback for the back-end checkout pane
          document.querySelector('#commerce-opayo-3d-result').value = message.message;

          // Process the result
          let messageForUser = '';
          if (message.hasOwnProperty('messageforuser') && message.messageforuser != '') {
            messageForUser = message.messageforuser;
          }
          process3DSecureResult(message.result, messageForUser, settings);
        });

      }, context);
    }
  }

  function process3DSecureResult(result, message, settings) {

    let formBuildId = '';
    try {
      let checkoutFormSelector = settings.commerceopayopi.opayo_3dsecure.hasOwnProperty('formQuerySelector') ? settings.commerceopayopi.opayo_3dsecure.formQuerySelector : 'form[id^="commerce-checkout"]';
      let checkoutForm = document.querySelector(checkoutFormSelector);
      formBuildId = checkoutForm.querySelector('[name="form_build_id"]').value;
    } catch { }

    logVerboseDebugMessage(settings, 'OpayoPi3DSecure.process3DSecureResult called, form build id: ' + formBuildId);

    if (settings.commerceopayopi.opayo_3dsecure.hasOwnProperty('status-' + formBuildId) && settings.commerceopayopi.opayo_3dsecure['status-' + formBuildId] == 'inprogress') {

      if (settings.commerceopayopi.opayo_3dsecure.hasOwnProperty('timerRef') && settings.commerceopayopi.opayo_3dsecure.timerRef != null) {
        // Cancel the timer that keeps checking for results
        clearInterval(settings.commerceopayopi.opayo_3dsecure.timerRef);
        logVerboseDebugMessage(settings, 'OpayoPi3DSecure.process3DSecureResult - timer cleared');
      }
      // Get the checkout form and proceed button
      let checkoutFormSelector = settings.commerceopayopi.opayo_3dsecure.hasOwnProperty('formQuerySelector') ? settings.commerceopayopi.opayo_3dsecure.formQuerySelector : 'form[id^="commerce-checkout"]';
      let checkoutForm = document.querySelector(checkoutFormSelector);
      let buttonSelector = settings.commerceopayopi.opayo_3dsecure.hasOwnProperty('proceedButtonSelector') ? settings.commerceopayopi.opayo_3dsecure.proceedButtonSelector : '[data-drupal-selector="edit-actions-next"]';
      let proceedButton = document.querySelector(buttonSelector);

      // 3D Secure authentication completed, submit the form associated with the 'threedsecure' checkout flow state to continue to the next appropriate step
      document.querySelector('#commerce-opayo-3d-result').value = result;
      if (result == 'success') {
        // Proceed to the next page of checkout (should be the 'complete' stage)
        logVerboseDebugMessage(settings, 'OpayoPi3DSecure.process3DSecureResult 3D secure Auth result success, submit form, form build id: ' + formBuildId);
        proceedButton.disabled = false;
        checkoutForm.submit();
      }
      else {
        // Check if there is a message to display to the user
        if (message != '') {
          const drupalMessage = new Drupal.Message();
          drupalMessage.add(message, {type: 'warning'});
          // Submit feedback message with the form so that it can be displayed if there's a redirect
          document.querySelector('#commerce-opayo-3d-user-message').value = message;
        }
        logVerboseDebugMessage(settings, 'OpayoPi3DSecure.process3DSecureResult 3D secure Auth failed, message: ' + message + ', submit form, form build id: ' + formBuildId);
        proceedButton.disabled = false;
        checkoutForm.submit();
      }
      settings.commerceopayopi.opayo_3dsecure['status-' + formBuildId] = 'submitted';
    }
    else {
      logVerboseDebugMessage(settings, 'OpayoPi3DSecure.process3DSecureResult do nothing (assume already submitted), form build id: ' + formBuildId);
    }
  }

  function start3DResultTimer(msInterval, transactionId, settings) {
    logVerboseDebugMessage(settings, 'OpayoPi3DSecure.start3DResultTimer - start interval to check 3D results, interval: ' + (msInterval / 1000) + 's');
    // Start an interval to check for 3D results - as a backup for the messaging with the iFrame
    let timerRef = setInterval((transactionId) => {
      const threeDSecureIframe = document.getElementById('commerce-opayo-3d-iframe');
      if (threeDSecureIframe) {
        logVerboseDebugMessage(settings, 'OpayoPi3DSecure.3DResultTimer - check for 3D Auth result by querying ' + '/commerce_opayo_pi/3dSecure_result/' + transactionId);
        // Still displaying the 3D secure iframe
        let ajaxObject = Drupal.ajax({
          url: '/commerce_opayo_pi/3dSecure_result/' + transactionId,
          base: false,
          element: false,
          progress: false
        });
        ajaxObject.execute();
      }
    }, msInterval, transactionId);
    return timerRef;
  }

  function logVerboseDebugMessage(settings, logMessage, error = null) {
    if (settings.commerceopayopi.opayo_3dsecure.hasOwnProperty('logLevel') && settings.commerceopayopi.opayo_3dsecure.logLevel == 10) {
      if (error != null) {
        console.error(error);
      }
      console.log(logMessage);
    }
  }

})(Drupal, once);
